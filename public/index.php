<?php

error_reporting(E_ALL);

try {

	/**
	 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
	 */
	$di = new \Phalcon\DI\FactoryDefault();

	/**
	 * Registering a router
	 */
	$di['router'] = function() {

		$router = new \Phalcon\Mvc\Router(false);

		/* Start of CENTER ROUTING */
		// Router for center
		// Ex: http://bnb.loc/glendale-arizona

		$router->add('/:params', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view',
			'params' => 1,
		));

		$router->add('/oakpark', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view',
			'params' => 'oak-park',
		));

		$router->add('/glenellyn', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view',
			'params' => 'glen-ellyn',
		));

		$router->add('/peachtreebuckhead', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view',
			'params' => 'peachtree-buckhead',
		));

		$router->add('/washingtondc', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view',
			'params' => 'washington-dc',
		));

		$router->add('/what-is/:params', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view',
			'params' => 1,
		));

		$router->add('/what-is', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view',
			'pageslugs' => 'about-body-and-brain',
		));

		$router->add('/classes/:params', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view',
			'params' => 1,
		));

		$router->add('/classes/videos/:params', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view',
			'params' => 1,
		));

		$router->add('/classes', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view',
			'pageslugs' => 'classes-overview',
		));

		//dati work-shops
		$router->add('/workshops/:params', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view',
			'params' => 1
		));

		// $router->add('/workshops', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'workshop',
		// 	'action' => 'overview',
		// 	'params' => 'workshops',
		// ));

		$router->add('/responsibility/:params', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view',
			'params' => 1,
		));

		$router->add('/responsibility', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view',
			'pageslugs' => 'responsibility',
		));

		$router->add('/wellness-at-work/:params', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view',
			'params' => 1,
		));

		$router->add('/wellness-at-work', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view',
			'pageslugs' => 'wellness-at-work',
		));

		$router->add('/personal-services/:params', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view',
			'params' => 1,
		));

		$router->add('/personal-services', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view',
			'params' => 'overview-of-personal-services',
		));

		$router->add('/franchise/:params', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view',
			'params' => 1,
		));

		$router->add('/franchise', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 'view',
			'params' => 'franchise',
		));



		// Router for center -> news
		// Ex: http://bnb.loc/glendale-arizona/news/1
		$router->add('/{centerslugs}/blog/{no}', array(
			'module' => 'frontend',
			'controller' => 'center',
			'action' => 'news'
		));

		// Router for center -> newsview
		// Ex: http://bnb.loc/glendale-arizona/news/news-slugs
		$router->add('/{centerslugs}/newspost/{newsslugs}', array(
			'module' => 'frontend',
			'controller' => 'center',
			'action' => 'newsview'
		));

		$router->add('/centernews/newspreview', array(
			'module' => 'frontend',
			'controller' => 'center',
			'action' => 'newspreview'
		));

		// Router for center -> calendar
		// Ex: http://bnb.loc/glendale-arizona/calendar/1
		$router->add('/{centerslugs}/calendar/{no}', array(
			'module' => 'frontend',
			'controller' => 'center',
			'action' => 'calendar'
		));

		// Router for center -> viewstory
		// Ex: http://bnb.loc/glendale-arizona/story/story-slugs
		$router->add('/{centerslugs}/story/{ssid}/{storyslugs}', array(
			'module' => 'frontend',
			'controller' => 'center',
			'action' => 'viewstory'
		));

		// Router for center -> pricing
		// Ex: http://bnb.loc/glendale-arizona/pricing
		$router->add('/{centerslugs}/pricing', array(
			'module' => 'frontend',
			'controller' => 'center',
			'action' => 'pricing'
		));

		// Router for center -> classschedule
		// Ex: http://bnb.loc/glendale-arizona/classschedule
		$router->add('/{centerslugs}/class-schedule', array(
			'module' => 'frontend',
			'controller' => 'center',
			'action' => 'classschedule'
		));

		// Router for center -> info
		// Ex: http://bnb.loc/glendale-arizona/info
		$router->add('/{centerslugs}/info', array(
			'module' => 'frontend',
			'controller' => 'center',
			'action' => 'info'
		));

		// Router for center -> success stories
		// Ex: http://bnb.loc/glendale-arizona/successstories
		$router->add('/{centerslugs}/success-stories/{no}', array(
			'module' => 'frontend',
			'controller' => 'center',
			'action' => 'successstories'
		));

		$router->add('/center/viewoffer', array(
			'module' => 'frontend',
			'controller' => 'center',
			'action' => 'viewoffer'
		));

		/* End of CENTER ROUTING */


		// Router for beneplace
		$router->add('/beneplace/trial', array(
			'module' => 'frontend',
			'controller' => 'beneplace',
			'action' => 'index'
		));
		// $router->add('/beneplace/:action', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'beneplace',
		// 	'action' => 1,
		// ));

		// Router for contact us
		$router->add('/contact-us', array(
			'module' => 'frontend',
			'controller' => 'contactus',
			'action' => 'index'
		));

		// Router for locations
		$router->add('/locations', array(
			'module' => 'frontend',
			'controller' => 'locations',
			'action' => 'index'
		));

		$router->add('/locations/{state}/{zip}', array(
			'module' => 'frontend',
			'controller' => 'locations',
			'action' => 'index'
		));

		// Router for feed
		$router->add('/feed', array(
			'module' => 'frontend',
			'controller' => 'feed',
			'action' => 'index'
		));

		// Router for getstarted
		$router->add('/getstarted', array(
			'module' => 'frontend',
			'controller' => 'getstarted',
			'action' => 'index',
		));
		$router->add('/getstarted/:action/:params', array(
			'module' => 'frontend',
			'controller' => 'getstarted',
			'action' => 1,
			'params' => 2
		));

		// Router for newsletter
		$router->add('/newsletter/:action/:params', array(
			'module' => 'frontend',
			'controller' => 'newsletter',
			'action' => 1,
			'params' => 2
		));

		// Router for page
		$router->add('/page/:action/:params', array(
			'module' => 'frontend',
			'controller' => 'page',
			'action' => 1,
			'params' => 2,
		));

		// Router for registration
		$router->add('/registration', array(
			'module' => 'frontend',
			'controller' => 'registration',
			'action' => 'index'
		));
		$router->add('/registration/:action/:params', array(
			'module' => 'frontend',
			'controller' => 'registration',
			'action' => 1,
			'params' => 2,
		));

		// Router for success stories
		$router->add('/successstories', array(
			'module' => 'frontend',
			'controller' => 'successstories',
			'action' => 'index'
		));

		$router->add('/successstories/:action/:params', array(
			'module' => 'frontend',
			'controller' => 'successstories',
			'action' => 1,
			'params' => 2,
		));

		// Router for users
		$router->add('/users/:action', array(
			'module' => 'frontend',
			'controller' => 'users',
			'action' => 1
		));

		// Router for workshop
		$router->add('/workshops', array(
			'module' => 'frontend',
			'controller' => 'workshop',
			'action' => 'overview'
		));

		// Router for workshop
		$router->add('/workshops/view/detail/{workshopid}', array(
			'module' => 'frontend',
			'controller' => 'workshop',
			'action' => 'detail',
		));

		// Router for workshop
		$router->add('/workshops/:action/{titleslugs}', array(
			'module' => 'frontend',
			'controller' => 'workshop',
			'action' => 1,
		));

		// Router for yogalife
		$router->add('/bnb-buzz', array(
			'module' => 'frontend',
			'controller' => 'yogalife',
			'action' => 'index'
		));

		$router->add('/bnb-buzz/preview', array(
			'module' => 'frontend',
			'controller' => 'yogalife',
			'action' => 'preview',
			'params' => 1
		));

		$router->add('/bnb-buzz/:action/:params', array(
			'module' => 'frontend',
			'controller' => 'yogalife',
			'action' => 1,
			'params' => 2,
		));

		$router->add('/conversations', array(
			'module' => 'frontend',
			'controller' => 'conversations',
			'action' => 'index',
			'params' => 1,
		));

		$router->add('/conversations/my-experiece', array(
			'module' => 'frontend',
			'controller' => 'conversations',
			'action' => 'myexperience',
			'params' => 1,
		));

		$router->add('/conversations/community', array(
			'module' => 'frontend',
			'controller' => 'conversations',
			'action' => 'community',
			'params' => 1,
		));


		$router->add('/maintenance', array(
			'module' => 'frontend',
			'controller' => 'maintenance',
			'action' => 'index',
			));

		$router->add('/success-stories/:params', array(
			'module' => 'frontend',
			'controller' => 'successstories',
			'action' => 'index',
			'params' => 1,
		));

		$router->add('/success-stories/success-write/{state}/{center}', array(
			'module' => 'frontend',
			'controller' => 'successstories',
			'action' => 'centerstory'
		));

		$router->add('/success-stories/{tag}/{offset}', array(
			'module' => 'frontend',
			'controller' => 'successstories',
			'action' => 'tag'
		));

		//dito maglagay

		$router->add('/', array(
			'module' => 'frontend',
			'controller' => 'index',
			'action' => 'index'
		));

		$router->add('/route404', array(
			'module' => 'frontend',
			'controller' => 'index',
			'action' => 'route404'
		));

		$router->add('/bnbadmin', array(
			'module' => 'backend',
			'controller' => 'index',
			'action' => 'index'
		));

		$router->add('/bnbadmin/:controller', array(
			'module'=> 'backend',
			'controller' => 1
		));

		$router->add('/bnbadmin/:controller/:action/:params', array(
			'module'=> 'backend',
			'controller' => 1,
			'action' => 2,
			'params' => 3,
		));


		$router->notFound(array(
			"controller" => "index",
			"action"	=> "route404"
			));

		// $router->add(
		//     "/:params",
		//     array(
		//     	"module" => 'frontend',
		//         "controller" => "center",
		//         "action"     => "page",
		//         "params" => 1
		//     )
		// );



		$router->removeExtraSlashes(true);

		return $router;
	};

	/**
	 * The URL component is used to generate all kind of urls in the application
	 */
	$di->set('url', function() {
		$url = new \Phalcon\Mvc\Url();
		$url->setBaseUri('/');
		return $url;
	});

	/**
	 * Start the session the first time some component request the session service
	 */
	$di->set('session', function() {
		$session = new \Phalcon\Session\Adapter\Files();
		$session->start();
		return $session;
	});

	/**
	 * If the configuration specify the use of metadata adapter use it or use memory otherwise
	 */
	$di->set('modelsMetadata', function () {
	    return new MetaDataAdapter();
	});

    /*
    ModelsManager
    */
	$di->set('modelsManager', function() {
	      return new Phalcon\Mvc\Model\Manager();
	});
    $config = include "../app/config/config.php";
    // Store it in the Di container
    $di->set('config', function () use ($config) {
        return $config;
    });
	/**
	 * Handle the request
	 */
	$application = new \Phalcon\Mvc\Application();

	$application->setDI($di);

	/**
	 * Register application modules
	 */
	$application->registerModules(array(
		'frontend' => array(
			'className' => 'Modules\Frontend\Module',
			'path' => '../app/frontend/Module.php'
		),
		'backend' => array(
			'className' => 'Modules\Backend\Module',
			'path' => '../app/backend/Module.php'
		)
	));

	$config = include __DIR__ . "/disqusapi/disqusapi.php";

	echo $application->handle()->getContent();

} catch (Phalcon\Exception $e) {
	echo $e->getMessage();
} catch (PDOException $e){
	echo $e->getMessage();
}
