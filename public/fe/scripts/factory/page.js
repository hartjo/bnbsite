app.factory("PageFactory", function ($http, Config){
	return {
		initialawakening: function (callback) {
			$http({
				url: Config.ApiURL + "/page/initialawakening",
				method: "GET"
			}).success(function (data, status, headers, config) {
				callback(data);
			})
		},
		related: function (pageslugs, callback) {
			$http({
				url: Config.ApiURL + "/page/related/"+pageslugs,
				method: "GET"
			}).success(function (data, status, headers, config) {
				callback(data);
			})
		}
	}
})