app.factory("SidebarFactory", function($http, Config) {
	return {
		contents: function(callback) {
			$http({
				url: Config.ApiURL + "/sidebar/contents",
				method: "GET"
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			})
		}
	}
})