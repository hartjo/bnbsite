app.factory('SuccessFactory',function($http, Config){
	return {
		storydetails: function(storyid, callback) {
			$http({
				url: Config.ApiURL + "/story/details/"+storyid,
				method: "GET",
				headers: { "Content-Type" : "application/x-www-form-urlencoded" }
			}).success(function (data, status, headers, config){
				callback(data);
			})
		}

	}
})