app.factory('locationFactory', function($http, $q, Config){
    return {
         searchlocation: function(search,callback){
            $http({
                url: Config.ApiURL + "/fe/location/searchcenterlocation",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(search)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
         }
    }
})
