app.factory('BeneplaceFactory', function($http, $q, Config){
    return {
         searchlocation: function(search,callback){
            $http({
                url: Config.ApiURL + "/fe/getstarted/searchlocation",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(search)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
         },
         authorize: function(card,callback){
            $http({
                url: Config.ApiURL + "/fe/getstarted/authorize",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(card)
            }).success(function (data, status, headers, config) {
                callback(data);
                console.log(data)
            }).error(function (data, status, headers, config) {
                callback(data);
            });
         },
          loadschedule: function(centerid,callback){
            $http({
                url: Config.ApiURL + "/center/schedule/loadschedule/"+ centerid,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
         },
         loadprice: function(callback){
            $http({
                url: Config.ApiURL + "/price/loadbeneplaceprice",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
         },
         loadcenterdetails: function(centerid,callback){
            $http({
                url: Config.ApiURL + "/fe/getstarted/loadcenter/"+centerid,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
         },
         viewreferrallink: function(callback){
            $http({
                url: Config.ApiURL +"/be/beneplace/viewreferrallink",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

         
    }  
})