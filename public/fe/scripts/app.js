'use strict';

var app = angular.module('app', [
    'ngFileUpload',
    'uiGmapgoogle-maps',
    'ng.deviceDetector',
    'ui.bootstrap',
    'hitcounter',
    'angular-storage',
    'angularMoment',
    'ngStorage',
    'ngSanitize'
    ])
  .config(function ($interpolateProvider){

     $interpolateProvider.startSymbol('{[{');
     $interpolateProvider.endSymbol('}]}');


   });
