'use strict';

/* Controllers */

app.controller('locationCtrl', function($scope, $http, Config, uiGmapGoogleMapApi, locationFactory,anchorSmoothScroll,$location,deviceDetector){



  var centerid = '';
  var centertitle = '';
  var centeraddress = '';
  var centerzip = '';
  var centeremail = '';
  var centerphone = '';
  var latitude = '';
  var longitude = '';
  var myloclat = '';
  var myloclon = '';

  navigator.geolocation.getCurrentPosition(function(pos) {
    myloclat = pos.coords.latitude;
    myloclon = pos.coords.longitude;
  });


var url = window.location.href;

 var auth3 = url.match(/\/locations\/(.*)+/);
  if(auth3 != null){
    var state = auth3[1].match(/(.*)+\//);
    var zip = auth3[1].match(/\/(.*)+/);

    $scope.search = {};

    angular.element('.chrono').removeClass('hidden');
        if(auth3 != null && auth3[1] != ""){

          if(state[1] != 'none'){
             $scope.search.state = state[1];
            var search = {
              state: state[1]
            }
          }

          if(zip[1] != 'none'){
            $scope.search.zip = zip[1];
            var search = {
              zip: zip[1]
            }
          }

          locationFactory.searchlocation(search, function(data){

              if(data != 'null') {

                if(data.length == 0){
                  angular.element('.chrono').addClass('hidden');
                  $scope.locationresults = false;
                  $scope.noresults = true;
                }
                else{
                  $scope.locationresults = true;
                  $scope.noresults = false;

                  var markers = [];
                  $scope.randomMarkers = [];
                  $scope.map = {center: {latitude: data[0].lat, longitude: data[0].lon }, zoom: 7 };
                  $scope.options = {scrollwheel: true};

                  markers.push({id: 'Location',latitude: myloclat, longitude: myloclon, title: 'My Current Location ',icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|69b9de'})
                  var newcenterlist = []
                  var items = 0

                  var distance =  function(lat1, lon1, lat2, lon2, unit) {
                    var radlat1 = Math.PI * lat1/180
                    var radlat2 = Math.PI * lat2/180
                    var radlon1 = Math.PI * lon1/180
                    var radlon2 = Math.PI * lon2/180
                    var theta = lon1-lon2
                    var radtheta = Math.PI * theta/180
                    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
                    dist = Math.acos(dist)
                    dist = dist * 180/Math.PI
                    dist = dist * 60 * 1.1515
                    return parseInt(dist, 10)
                  }


                  angular.element('.chrono').addClass('hidden');
                  data.forEach(function(list){
                      newcenterlist.push({
                        authorizeid: list.authorizeid,
                        authorizekey: list.authorizekey,
                        centeraddress: list.centeraddress,
                        centercity: list.centercity,
                        centerdistrict: list.centercity,
                        centerid: list.centerid,
                        centermanager: list.centermanager,
                        centerregion: list.centerregion,
                        centerslugs: list.centerslugs,
                        centerstate: list.centerstate,
                        centertelnumber: list.centertelnumber,
                        centertitle: list.centertitle,
                        centertype: list.centertype,
                        centerzip: list.centerzip,
                        datecreated: list.datecreated,
                        dateedited: list.dateedited,
                        email: list.email,
                        lat: list.lat,
                        locationid: list.locationid,
                        lon: list.lon,
                        metadesc: list.metadesc,
                        metatitle: list.metatitle,
                        paypalid: list.paypalid,
                        phonenumber: list.phonenumber,
                        status: list.status,
                        fdistance: list.fdistance,
                        'distance':distance(myloclat,myloclon,list.lat,list.lon,'M')
                      })

                      items = items+1;
                  })

                   $scope.locationlist = newcenterlist;

                  data.forEach(function(entry) {
                    markers.push({id: entry.centerid,latitude: entry.lat, longitude: entry.lon, title: 'Address ' + entry.centertitle + ': ' +  entry.centeraddress + ', ' + entry.centerzip})
                  });



                  $scope.randomMarkers = markers;
                  angular.element('.chrono').addClass('hidden');

                }
              }
              else{
               angular.element('.chrono').addClass('hidden');
                  $scope.locationresults = false;
                  $scope.noresults = true;
              }
            }); //end of fucktory

        }
        else{


          angular.element('.chrono').addClass('hidden');


        }

  }
  else{



      $scope.searchcenter = function(search){
        angular.element('.chrono').removeClass('hidden');
        if(search != undefined){
          locationFactory.searchlocation(search, function(data){
            console.log(data);
             if(data != 'null'){
               if(data.length == 0){
                $scope.locationresults = false;
                $scope.noresults = true;
                angular.element('.chrono').addClass('hidden');
              }
              else{
                $scope.locationresults = true;
                $scope.noresults = false;

                var markers = [];
                $scope.randomMarkers = [];
                $scope.map = {center: {latitude: data[0].lat, longitude: data[0].lon }, zoom: 7 };
                $scope.options = {scrollwheel: true};

                markers.push({id: 'Location',latitude: myloclat, longitude: myloclon, title: 'My Current Location ',icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|69b9de'})
                var newcenterlist = []
                var items = 0

                var distance =  function(lat1, lon1, lat2, lon2, unit) {
                  var radlat1 = Math.PI * lat1/180
                  var radlat2 = Math.PI * lat2/180
                  var radlon1 = Math.PI * lon1/180
                  var radlon2 = Math.PI * lon2/180
                  var theta = lon1-lon2
                  var radtheta = Math.PI * theta/180
                  var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
                  dist = Math.acos(dist)
                  dist = dist * 180/Math.PI
                  dist = dist * 60 * 1.1515
                  return parseInt(dist, 10)
                }



                data.forEach(function(list){
                    newcenterlist.push({
                      authorizeid: list.authorizeid,
                      authorizekey: list.authorizekey,
                      centeraddress: list.centeraddress,
                      centercity: list.centercity,
                      centerdistrict: list.centercity,
                      centerid: list.centerid,
                      centermanager: list.centermanager,
                      centerregion: list.centerregion,
                      centerslugs: list.centerslugs,
                      centerstate: list.centerstate,
                      centertelnumber: list.centertelnumber,
                      centertitle: list.centertitle,
                      centertype: list.centertype,
                      centerzip: list.centerzip,
                      datecreated: list.datecreated,
                      dateedited: list.dateedited,
                      email: list.email,
                      lat: list.lat,
                      locationid: list.locationid,
                      lon: list.lon,
                      metadesc: list.metadesc,
                      metatitle: list.metatitle,
                      paypalid: list.paypalid,
                      phonenumber: list.phonenumber,
                      status: list.status,
                      fdistance: list.fdistance,
                      'distance':distance(myloclat,myloclon,list.lat,list.lon,'M')
                    })

                    items = items+1;
                })

                 $scope.locationlist = newcenterlist;

                data.forEach(function(entry) {
                  markers.push({id: entry.centerid,latitude: entry.lat, longitude: entry.lon, title: 'Address ' + entry.centertitle + ': ' +  entry.centeraddress + ', ' + entry.centerzip})
                });



                $scope.randomMarkers = markers;
                angular.element('.chrono').addClass('hidden');

            }
          }
            else{
               angular.element('.chrono').addClass('hidden');
                  $scope.locationresults = false;
                  $scope.noresults = true;
              }
            }); //end of fucktory

        }
        else{
          $scope.locationresults = false;
          $scope.noresults = true;
          angular.element('.chrono').addClass('hidden');
        }

      }


 }

 $scope.searchcenter = function(search){
        angular.element('.chrono').removeClass('hidden');
        if(search != undefined){
          locationFactory.searchlocation(search, function(data){
            if(data != 'null'){
            if(data.length == 0){
              $scope.locationresults = false;
              $scope.noresults = true;
              angular.element('.chrono').addClass('hidden');
            }
            else{
              angular.element('.chrono').addClass('hidden');
              $scope.locationresults = true;
              $scope.noresults = false;

              var markers = [];
              $scope.randomMarkers = [];
              $scope.map = {center: {latitude: data[0].lat, longitude: data[0].lon }, zoom: 7 };
              $scope.options = {scrollwheel: true};

              markers.push({id: 'Location',latitude: myloclat, longitude: myloclon, title: 'My Current Location ',icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|69b9de'})
              var newcenterlist = []
              var items = 0

              var distance =  function(lat1, lon1, lat2, lon2, unit) {
                var radlat1 = Math.PI * lat1/180
                var radlat2 = Math.PI * lat2/180
                var radlon1 = Math.PI * lon1/180
                var radlon2 = Math.PI * lon2/180
                var theta = lon1-lon2
                var radtheta = Math.PI * theta/180
                var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
                dist = Math.acos(dist)
                dist = dist * 180/Math.PI
                dist = dist * 60 * 1.1515
                return parseInt(dist, 10)
              }



              data.forEach(function(list){
                  newcenterlist.push({
                    authorizeid: list.authorizeid,
                    authorizekey: list.authorizekey,
                    centeraddress: list.centeraddress,
                    centercity: list.centercity,
                    centerdistrict: list.centercity,
                    centerid: list.centerid,
                    centermanager: list.centermanager,
                    centerregion: list.centerregion,
                    centerslugs: list.centerslugs,
                    centerstate: list.centerstate,
                    centertelnumber: list.centertelnumber,
                    centertitle: list.centertitle,
                    centertype: list.centertype,
                    centerzip: list.centerzip,
                    datecreated: list.datecreated,
                    dateedited: list.dateedited,
                    email: list.email,
                    lat: list.lat,
                    locationid: list.locationid,
                    lon: list.lon,
                    metadesc: list.metadesc,
                    metatitle: list.metatitle,
                    paypalid: list.paypalid,
                    phonenumber: list.phonenumber,
                    status: list.status,
                    fdistance: list.fdistance,
                    'distance':distance(myloclat,myloclon,list.lat,list.lon,'M')
                  })

                  items = items+1;
              })

               $scope.locationlist = newcenterlist;

              data.forEach(function(entry) {
                markers.push({id: entry.centerid,latitude: entry.lat, longitude: entry.lon, title: 'Address ' + entry.centertitle + ': ' +  entry.centeraddress + ', ' + entry.centerzip})
              });



              $scope.randomMarkers = markers;
              angular.element('.chrono').addClass('hidden');

            }
             }
            else{
               angular.element('.chrono').addClass('hidden');
                  $scope.locationresults = false;
                  $scope.noresults = true;
              }
            }); //end of fucktory

        }
        else{
          $scope.locationresults = false;
          $scope.noresults = true;
          angular.element('.chrono').addClass('hidden');
        }

      }


})
