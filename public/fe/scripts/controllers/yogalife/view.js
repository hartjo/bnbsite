app.controller("yogalifeviewCtrl", function ($scope) {
	$scope.mostpopular = true;
	$scope.latest = false;

	$scope.tab_mostpop = function() {
		$scope.mostpopular = true;
		$scope.latest = false;
		angular.element('.mostpop').removeClass('dev_text');
		angular.element('.latest').addClass('dev_text');
		angular.element('.latest').removeClass('bg_whitish');
		angular.element('.mostpop').addClass('bg_whitish');
	}
	$scope.tab_latest = function() {
		$scope.mostpopular = false;
		$scope.latest = true;
		angular.element('.latest').removeClass('dev_text');
		angular.element('.mostpop').addClass('dev_text');
		angular.element('.mostpop').removeClass('bg_whitish');
		angular.element('.latest').addClass('bg_whitish');
	}
})