'use strict';

/* Controllers */

app.controller('WriteCtrl', function ($scope, $http, Config, CenterFactory, Upload,store) {
	// Story details counter
	$scope.contentcounter = 0;
    $scope.detailscounter = function (details) {
    	if(details == undefined || details == null ||  details == '') {
    		$scope.contentcounter = 0;
    	} else {
			$scope.contentcounter = details.length;
		}
    };
    // Story details counter ends here

  	var oriTestimony = angular.copy($scope.testimony);

  	var centerslugs = store.get('centerslugs');

  	$scope.state_code = function (state_code) {
  		CenterFactory.loadCenterViaState(state_code, function(data) {
  			$scope.centers = data;
  		});
  		CenterFactory.pagedata(centerslugs, function(data) {
  			$scope.testimony.center = data.centerid;
  		})
  	}

  // LOAD THE CENTER LIST from FACTORY
	CenterFactory.FEloadstate(function(data) {
		$scope.states = data;
	});

	$scope.centerViaState = function(state_code) {
		CenterFactory.loadCenterViaState(state_code, function(data) {
			$scope.centers = data;
		});
	}

	$scope.detailserror = false;
	$scope.write = function(testimony) {
			if(testimony.details.length < 200) {
				$scope.detailserror = true;
			} else {
				$scope.detailserror = false;
				var captcha = document.getElementById("txtCaptcha").value;
				if(captcha == $scope.str2){
				angular.element('.chrono').removeClass('hidden');
				var files = $scope.testimony.photo;
				var filename;
				var filecount = 0;
				if (files && files.length)
				{
					$scope.imageloader=true;
					$scope.imagecontent=false;

					for (var i = 0; i < files.length; i++)
					{
						var file = files[i];

						if (file.size >= 2000000)
						{
							$scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
							filecount = filecount + 1;

							if(filecount == files.length)
							{
								$scope.imageloader=false;
								$scope.imagecontent=true;
							}
						}
						else
						{
							var promises;
							promises = Upload.upload({
		              url: Config.amazonlink, //S3 upload url including bucket name
		              method: 'POST',
		              transformRequest: function (data, headersGetter) {
		              //Headers change here
		              var headers = headersGetter();
		              delete headers['Authorization'];
		              return data;
		            },
		            fields : {
		                key: 'uploads/testimonialimages/' + file.name, // the key to store the file on S3, could be file name or customized
		                AWSAccessKeyId: Config.AWSAccessKeyId,
		                acl: 'private', // sets the access to the uploaded file in the bucket: private or public
		                policy: Config.policy, // base64-encoded json policy (see article below)
		                signature: Config.signature, // base64-encoded signature based on policy string (see article below)
		                "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
		              },
		              file: file
		            })

								promises.then(function(data){
									filecount = filecount + 1;
									filename = data.config.file.name;

									testimony['photo'] = filename;
										$http({
											url: Config.ApiURL + "/success-stories/publish",
											method: "POST",
											headers: {'Content-Type': 'application/x-www-form-urlencoded'},
											data: $.param(testimony)
										}).success(function (data, status, headers, config) {
											$scope.isSaving = true;
											$scope.testimony = angular.copy(oriTestimony);
											$scope.str2 = "";
											$scope.str1 = "";
											$scope.writeform.$setPristine(true);
											$scope.isSaving = false;
											angular.element("#writeModal").removeClass('hidden');
											angular.element('#photopreview').hide();
											$scope.testimony.photo = "";
										})

								});
							}
						}
					}
				}else{
					angular.element("#writeModal").addClass('hidden');
					angular.element('.chrono').addClass('hidden');
					Materialize.toast('Captcha is Wrong!', 4000);
				}
			}
	};


});
