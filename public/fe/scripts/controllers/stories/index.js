'use strict';

app.controller('successCtrl', function ($scope, $http, Config) {
	var slide = 1;
	var pos =0;

	$scope.back = function() {
		var totalSpotlights = $scope.spot - 1;
		var rolls = Math.round(totalSpotlights / 2);

		if(slide != rolls) {
			pos -= 100;
			$scope.myStyle= { marginLeft: pos+'%' }
			slide++;
		}
		
	}

	$scope.next= function() {
		var totalSpotlights = $scope.spot - 1;
		var rolls = Math.round(totalSpotlights / 2);

		if(slide != 1) { 
			pos += 100;
			$scope.myStyle= { marginLeft: pos+'%' }
			slide--;
		}
		
	}
})