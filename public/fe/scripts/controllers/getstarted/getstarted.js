'use strict';

/* Controllers */

app.controller('getstartedCtrl', function($scope, $http, Config, uiGmapGoogleMapApi, getstartedFactory,anchorSmoothScroll,$location,deviceDetector){
  $scope.invalidpaypal = true;
  $scope.firststep = true;
  var sessionclass = '';
  var sessiontype = '';
  var centerid = '';
  var centertitle = '';
  var centeraddress = '';
  var centerzip = '';
  var centeremail = '';
  var centerphone = '';
  var itemprice = 0;
  var totalprice = 0;
  var fullname = '';
  var email = '';
  var phone1 = '';
  var phone2 = '';
  var phone3 = '';
  var classday = '';
  var classhour = '';
  var comment = '';
  var qty = 1;
  var sumdate = '';
  var latitude = '';
  var longitude = '';
  var device = '';
  var deviceos = '';
  var devicebrowser = '';
  var paypalid = '';
  var authorizeid = '';
  var authorizekey = '';
  var introofflinerate = '';
  var introonlinerate = '';
  var groupofflinerate = '';
  var grouponlinerate = '';
  var finalonlinerate = '';
  var myloclat = '';
  var myloclon = '';

    $scope.itemprice = 0;
    $scope.itemquantity = 0;
    $scope.totalprice = 0;

    $scope.introprice = 0;
    $scope.groupprice = 0;

  // navigator.geolocation.getCurrentPosition(function(pos) {
  //   myloclat = pos.coords.latitude;
  //   myloclon = pos.coords.longitude;
  // });

  var loadprices = function(){
      getstartedFactory.loadprice(function(data){

        introofflinerate = data.introofflinerate;
        introonlinerate = data.introonlinerate;
        groupofflinerate = data.groupofflinerate;
        grouponlinerate = data.grouponlinerate;

        $scope.introofflinerate = introofflinerate;
        $scope.introonlinerate = introonlinerate;
        $scope.groupofflinerate = groupofflinerate;
        $scope.grouponlinerate = grouponlinerate;
        //I dont know really if this fixes the issue but it works
        $scope.groupprice = data.grouponlinerate;
        $scope.introprice = data.introonlinerate;
      })
  }
  
  loadprices();

  if(deviceDetector.isDesktop() == true){
    device = 'desktop';
  }
  else{
    device = deviceDetector.device;
  }
  
  deviceos = deviceDetector.os;
  devicebrowser = deviceDetector.browser;

  var validatepaypalbutton = function(){
    if(fullname == '' || centerid == '' || email == '' ||  phone1 == '' || phone2 == ''|| phone3 == '' || classday == '' || classhour == '' || qty == '' ){

      $scope.validpaypal = false;
      $scope.invalidpaypal = true;
      $scope.device = device;
      $scope.deviceos = deviceos;
      $scope.devicebrowser = devicebrowser;
      $scope.lon = longitude;
      $scope.lat = latitude;
      $scope.sessionclass = sessionclass;
      $scope.sessiontype = sessiontype;
      $scope.finalonlinerate = finalonlinerate;
    }
    else
    {
      $scope.validpaypal = true;
      $scope.invalidpaypal = false;
      $scope.device = device;
      $scope.deviceos = deviceos;
      $scope.devicebrowser = devicebrowser;
      $scope.lon = longitude;
      $scope.lat = latitude;
      $scope.sessionclass = sessionclass;
      $scope.sessiontype = sessiontype;
      $scope.finalonlinerate = finalonlinerate;


    }
  }


  var url = window.location.href;
  var geturlcenterid = url.match(/\/getstarted\/starterspackage\/(.*)+/);
  var urlcenterid = '';

  if(geturlcenterid != null){
     urlcenterid = geturlcenterid[1];
  }
  else{
    urlcenterid = 'dummy';
  }

  getstartedFactory.loadcenterdetails(urlcenterid, function(data){
  
    if(data == 'false' || data.length == 0){
      withoutcenterid();
    }
    else {
      centerid = urlcenterid;
      centertitle = data.centertitle;
      centeraddress = data.centeraddress;
      centerzip = data.centerzip;
      centerphone = data.phonenumber;
      latitude = data.lat;
      longitude = data.lon;
      centeremail = data.email;
      paypalid = data.paypalid;
      authorizeid = data.authorizeid;
      authorizekey = data.authorizekey;

      $scope.paypalid = paypalid;
      $scope.authorizeid = authorizeid;
      $scope.authorizekey = authorizekey;
      $scope.centeremail = centeremail;
      $scope.centertitle = centertitle;
      $scope.centerid = centerid;
      $scope.centeraddress = centeraddress;
      $scope.centerzip = centerzip;
      $scope.centerphone = centerphone;
      $scope.centerlon = longitude;
      $scope.centerlat = latitude;
      loadschedule(centerid);
      withcenterid();

    }

  })

var withcenterid = function(){
  $scope.selectsession = function(session){

    if(session == 'intro'){
      sessionclass = '1-on-1 Intro Session';
      $scope.classhour = true;
      sessiontype = 0;
      finalonlinerate = introonlinerate;
      $scope.finalonlinerate = finalonlinerate;
      //Added this - efrem
      $scope.itemprice = $scope.introprice;

      $scope.sessionclass = sessionclass;
      $scope.totalprice = qty * finalonlinerate;
      totalprice = qty * finalonlinerate;
     
    }
    else{
      sessionclass = '1 Group Class + 1-on-1 Intro Session';
      $scope.classhour = false;
      sessiontype = 1;
      finalonlinerate = grouponlinerate;
      $scope.finalonlinerate = finalonlinerate;
      $scope.sessionclass = sessionclass;
      //Added this - efrem
      $scope.itemprice = $scope.groupprice;
      $scope.totalprice = qty * finalonlinerate;
      totalprice = qty * finalonlinerate;
    }
    
    $scope.firststep = false;
    $scope.thirdstep = true;
    anchorSmoothScroll.scrollTo('gototop');
  }

}

var withoutcenterid = function(){

   $scope.backtosecondstep = function(){
    sessionclass = '';
    $scope.firststep = false;
    $scope.secondstep = true;
    $scope.thirdstep = false;
    $scope.locationresults = false;
    $scope.paymentcheck = false;
    anchorSmoothScroll.scrollTo('gototop');
    $scope.itemprice = '';
    $scope.itemquantity = '';
    $scope.totalprice = '';
  }

  $scope.selectsession = function(session){

    if(session == 'intro'){
      sessionclass = '1-on-1 Intro Session';
      $scope.classhour = true;
      sessiontype = 0;
      finalonlinerate = introonlinerate;
      $scope.itemprice = finalonlinerate;
     
      $scope.totalprice = qty * finalonlinerate;
      totalprice = qty * finalonlinerate;
     
    }
    else{
      sessionclass = '1 Group Class + 1-on-1 Intro Session';
      $scope.classhour = false;
      sessiontype = 1;
      
      finalonlinerate = grouponlinerate;
      $scope.itemprice = finalonlinerate;
      $scope.totalprice = qty * finalonlinerate;
      totalprice = qty * finalonlinerate;
      
    }
    
    $scope.firststep = false;
    $scope.secondstep = true;
    anchorSmoothScroll.scrollTo('gototop');
  }

}

  $scope.backtofirststep = function(){
    sessionclass = '';
    $scope.firststep = true;
    $scope.secondstep = false;
    $scope.thirdstep = false;
    $scope.locationresults = false;
    $scope.paymentcheck = false;
    $scope.itemprice = '';
    $scope.totalprice = '';
    $scope.itemquantity = '';
    anchorSmoothScroll.scrollTo('gototop');
  }

 

  

  $scope.putname = function(name){
    fullname = name;
    $scope.submitfullname = fullname;
    $scope.submitcenterid = centerid;
    validatepaypalbutton();
  }

  $scope.putemail = function(txtemail){
    email = txtemail;
    $scope.submitemail = email;
    $scope.submitcenterid = centerid;
    validatepaypalbutton();
  }

  $scope.putphone1 = function(txtphone1){
    phone1 = txtphone1;
    $scope.submitphonenumber  = phone1+'-'+phone2+'-'+phone3;
    validatepaypalbutton();
  }

  $scope.putphone2 = function(txtphone2){
    phone2 = txtphone2;
    $scope.submitphonenumber  = phone1+'-'+phone2+'-'+phone3;
    validatepaypalbutton();
  }

  $scope.putphone3 = function(txtphone3){
    phone3 = txtphone3;
    $scope.submitphonenumber  = phone1+'-'+phone2+'-'+phone3;
    validatepaypalbutton();
  }

  $scope.puttime = function(txttime){
  
    classhour = txttime;
    $scope.submittime = classhour;
    validatepaypalbutton();
  }

  $scope.putcomment = function(txtcomment){
    comment = txtcomment;
    $scope.submitcomment = comment;

  }

  $scope.centerpick = function(centeridpick,centertitlepick,centeraddresspick,centerzippick,centerphonepick,centerlat,centerlon,pickcenteremail,pickpaypalid,pickauthorizeid,pickauthorizekey){
 
    centerid = centeridpick;
    centertitle = centertitlepick;
    centeraddress = centeraddresspick;
    centerzip = centerzippick;
    centerphone = centerphonepick;
    latitude = centerlat;
    longitude = centerlon;
    centeremail = pickcenteremail;
    paypalid = pickpaypalid;
    authorizeid = pickauthorizeid;
    authorizekey = pickauthorizekey;

    $scope.paypalid = paypalid;
    $scope.authorizeid = authorizeid;
    $scope.authorizekey = authorizekey;
    $scope.centeremail = centeremail;
    $scope.centertitle = centertitle;
    $scope.centerid = centerid;
    $scope.centeraddress = centeraddress;
    $scope.centerzip = centerzip;
    $scope.centerphone = centerphone;
    $scope.centerlon = longitude;
    $scope.centerlat = latitude;
    $scope.secondstep = false;
    $scope.thirdstep = true;
    anchorSmoothScroll.scrollTo('gototop');
    loadschedule(centerid);
    validatepaypalbutton();
  }

  $scope.centerradiopick = function(centeridpick,centertitlepick,centeraddresspick,centerzippick,centerphonepick,centerlat,centerlon,pickcenteremail,pickpaypalid,pickauthorizeid,pickauthorizekey){

    centerid = centeridpick;
    centertitle = centertitlepick;
    centeraddress = centeraddresspick;
    centerzip = centerzippick;
    centerphone = centerphonepick;
    latitude = centerlat;
    longitude = centerlon;
    centeremail = pickcenteremail;
    paypalid = pickpaypalid;
    authorizeid = pickauthorizeid;
    authorizekey = pickauthorizekey;

    $scope.paypalid = paypalid;
    $scope.authorizeid = authorizeid;
    $scope.authorizekey = authorizekey;
    $scope.centeremail = centeremail;
    $scope.centerlon = longitude;
    $scope.centerlat = latitude;
    $scope.btnpickcenter = true;
    $scope.centertitle = centertitle;
    $scope.centerid = centerid;
    $scope.centeraddress = centeraddress;
    $scope.centerzip = centerzip;
    $scope.centerphone = centerphone;
    loadschedule(centerid);
    validatepaypalbutton();
  }

  var loadschedule = function(centerid){
    getstartedFactory.loadschedule(centerid, function(data){
      $scope.schedlelist = data;
  
    });
  }
 

  $scope.gotoschedule = function(){
    if(centertitle != ''){
      $scope.secondstep = false;
      $scope.thirdstep = true;
      anchorSmoothScroll.scrollTo('gototop');
    }
    else{
      $scope.pickcentermsg = 'Please Select Center First!';
    }
  }

  function isPastDate(value) {
    var now    = new Date;
    var target = new Date(value);

    if (now.getFullYear() > target.getFullYear()) {
      return true;
    }
    else if (now.getMonth() > target.getMonth()) {
      return true;
    }
    else if (now.getDate() > target. getDate()) {
      return true;
    }

    return false;
  }


  $scope.checkdate = function(scheduledate){
    var inputDate = new Date(scheduledate);
    var todaysDate = new Date();
    if(inputDate.setHours(0,0,0,0) < todaysDate.setHours(0,0,0,0))
    {
       $scope.dateerror = true;
     $scope.dateerrormsg = 'Please choose a date after ' + finaldatetoday;
     $scope.validpaypal = false;
     $scope.invalidpaypal = true;
     $scope.scheduleform.txtDate.$setValidity("maxLength",false);
    }
    else{
     $scope.dateerror = false;
     classday = scheduledate;

     $scope.submitschedday = classday;
     $scope.scheduleform.txtDate.$setValidity("maxLength",true);
     validatepaypalbutton();

     var date = new Date(scheduledate);
     var finaldate = scheduledate;
     var datetoday = new Date();
     var finaldatetoday = scheduledate;
     sumdate = finaldate;

    if(date.getDay() == 0){
      $scope.today = 'Sunday';
    }
    else if(date.getDay() == 1){
      $scope.today = 'Monday';
    }
    else if(date.getDay() == 2){
      $scope.today = 'Tuesday';
    }
    else if(date.getDay() == 3){
      $scope.today = 'Wednesday';
    }
    else if(date.getDay() == 4){
      $scope.today = 'Thursday';
    }
    else if(date.getDay() == 5){
      $scope.today = 'Friday';
    }
    else if(date.getDay() == 6){
      $scope.today = 'Saturday';
    }

    loadschedule(centerid);
    }


   
   //  if(isPastDate(scheduledate) == true){
    
   // }
   // else{
   
    
  // }

}

$scope.calculateprice = function(itemqty){
  qty = itemqty;
  $scope.itemprice = finalonlinerate;
  $scope.totalprice = itemqty * finalonlinerate;
  totalprice = itemqty * finalonlinerate;
  $scope.itemquantity = qty;
  validatepaypalbutton();
}

$scope.searchcenter = function(search){
  angular.element('.chrono').removeClass('hidden');
  if(search != undefined){
    getstartedFactory.searchlocation(search, function(data){
 
      if(data.length == 0){
        $scope.locationresults = false;
        $scope.noresults = true;
        angular.element('.chrono').addClass('hidden');
      }
      else{
        $scope.locationresults = true;
        $scope.noresults = false;
       
        var markers = [];
        $scope.randomMarkers = [];
        $scope.map = {center: {latitude: data[0].lat, longitude: data[0].lon }, zoom: 7 };
        $scope.options = {scrollwheel: true};

        markers.push({id: 'Location',latitude: myloclat, longitude: myloclon, title: 'My Current Location ',icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|69b9de'})
        var newcenterlist = []
        var items = 0

        var distance =  function(lat1, lon1, lat2, lon2, unit) {
          var radlat1 = Math.PI * lat1/180
          var radlat2 = Math.PI * lat2/180
          var radlon1 = Math.PI * lon1/180
          var radlon2 = Math.PI * lon2/180
          var theta = lon1-lon2
          var radtheta = Math.PI * theta/180
          var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
          dist = Math.acos(dist)
          dist = dist * 180/Math.PI
          dist = dist * 60 * 1.1515
          return parseInt(dist, 10)
        }



        data.forEach(function(list){
            newcenterlist.push({
              authorizeid: list.authorizeid,
              authorizekey: list.authorizekey,
              centeraddress: list.centeraddress,
              centercity: list.centercity,
              centerdistrict: list.centercity,
              centerid: list.centerid,
              centermanager: list.centermanager,
              centerregion: list.centerregion,
              centerslugs: list.centerslugs,
              centerstate: list.centerstate,
              centertelnumber: list.centertelnumber,
              centertitle: list.centertitle,
              centertype: list.centertype,
              centerzip: list.centerzip,
              datecreated: list.datecreated,
              dateedited: list.dateedited,
              email: list.email,
              lat: list.lat,
              locationid: list.locationid,
              lon: list.lon,
              metadesc: list.metadesc,
              metatitle: list.metatitle,
              paypalid: list.paypalid,
              phonenumber: list.phonenumber,
              status: list.status,
              // 'distance':distance(myloclat,myloclon,list.lat,list.lon,'M')
              distance: list.fdistance
            })

            items = items+1;
        })

         $scope.locationlist = newcenterlist;

        data.forEach(function(entry) {
          markers.push({id: entry.centerid,latitude: entry.lat, longitude: entry.lon, title: 'Address ' + entry.centertitle + ': ' +  entry.centeraddress + ', ' + entry.centerzip})
        });

        

        $scope.randomMarkers = markers;
        angular.element('.chrono').addClass('hidden');

      }

      }); //end of fucktory

  }
  else{
    $scope.locationresults = false;
    $scope.noresults = true;
    angular.element('.chrono').addClass('hidden');
  }

}

$scope.paymentsubmit = function(){
  if(classhour != ''){
    $scope.thirdstep = false;
    $scope.paymentcheck = true;
    $scope.card.billfullname = fullname;
    $scope.card.billphone = phone1+'-'+phone2+'-'+phone3;

    $scope.sumsession = sessionclass;
    $scope.sumname = fullname;
    $scope.sumcentername = centertitle;
    $scope.sumlocation = centeraddress;
    $scope.sumzip = centerzip;
    $scope.sumdate = sumdate;
    $scope.sumtime = classhour;
    $scope.sumtotal = totalprice;
    anchorSmoothScroll.scrollTo('gototop');
   }
  else{
    anchorSmoothScroll.scrollTo('gotooffer_sche');
    $scope.classhourwarning = true;
  }
}


$scope.submitcheckout = function(card){
 
  if($scope.acceptterm == false){
    $scope.checkthis = true;
    anchorSmoothScroll.scrollTo('gototerm');
  }
  else{
    $scope.checkthis = false;
 
      angular.element('.chrono').removeClass('hidden');

      card['centerid'] = centerid;
      card['centertitle'] = centertitle;
      card['centeraddress'] = centeraddress;
      card['centerphone'] = centerphone;
      card['fullname'] = fullname;
      card['email'] = email;
      card['phone'] = phone1+'-'+phone2+'-'+phone3;
      card['classday'] = classday;
      card['classhour'] = classhour;
      card['comment'] = comment;
      card['qty'] = qty;
      card['totalprice'] = totalprice;
      card['itemprice'] = itemprice;
      card['sessionclass'] = sessionclass;
      card['device'] = device;
      card['deviceos'] = deviceos;
      card['devicebrowser'] = devicebrowser;
      card['sessiontype'] = sessiontype;
      card['authorizeid'] = authorizeid;
      card['authorizekey'] = authorizekey;
      card['moduletype'] = 'starter';

      getstartedFactory.authorize(card,function(data){

        if(data[0] != '1' && data[2] == '6'){
          $scope.cardmsg = data[3];
          angular.element('.chrono').addClass('hidden');
          anchorSmoothScroll.scrollTo('gototop');
        }
        else if(data[0] != '1' && data[2] == '8'){
          $scope.cardmsg = data[3];
          angular.element('.chrono').addClass('hidden');
          anchorSmoothScroll.scrollTo('gototop');
        }
        else if(data[0] == '1'){
          angular.element('.chrono').addClass('hidden');
          anchorSmoothScroll.scrollTo('gototop');
          $scope.paymentcheck = false;
          $scope.finalstep = true;
          $scope.fullname = fullname;
          $scope.centerphone = centerphone;
          $scope.centertitle = centertitle;
          $scope.centeraddress = centeraddress;
          $scope.centerzip = centerzip;
          $scope.centeremail = centeremail;
          $scope.scheddate = new Date(sumdate);
          $scope.schedhour = classhour;
          $scope.totalprice = totalprice;
          $scope.confirmnumber = data[6];
          $scope.paymenttype = data[51];
          $scope.sessionclass = sessionclass;

          var markers = [];
          $scope.randomMarkers = [];
          $scope.map = {center: {latitude: latitude, longitude: longitude }, zoom: 18 };
          $scope.options = {scrollwheel: true};
          
          markers.push({id: centerid,latitude: latitude, longitude: longitude, title: 'Address ' + centertitle + ': ' +  centeraddress + ', ' + centerzip})

          $scope.randomMarkers = markers;


          
        }
        else{
          $scope.cardmsg = data[3];
          angular.element('.chrono').addClass('hidden');
          anchorSmoothScroll.scrollTo('gototop');
        }

      });


    }

}


})