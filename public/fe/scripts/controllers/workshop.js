'use strict';

app.controller('workshopCtrl', function ($scope, $http, Config, $window, WorkshopFactory, detailsFactory, anchorSmoothScroll, deviceDetector) {
   var device = '';
   var deviceos = '';
   var devicebrowser = '';
   var authorizeid = '';
   var authorizekey = '';
   if(deviceDetector.isDesktop()){
    device = 'desktop';
    }
    else{
        device = deviceDetector.device;
    }

    deviceos = deviceDetector.os;
    devicebrowser = deviceDetector.browser;

    var url = window.location.href;
    var schedulepage = url.match(/\/workshops\/workshopschedules\/(.*)+/);
    var registrationpage = url.match(/\/workshops\/registration\/(.*)+/);

	WorkshopFactory.lists(function(data){
		$scope.titles = data.titles;
        $scope.states = data.states;
        $scope.date_choices = data.date_choices;
    });

    var cities = function() {
        WorkshopFactory.lists(function(data){
            $scope.cities = data.cities;
        });
    }
    cities();

    $scope.statechange = function(statecode)
    {
    	if(statecode == '') { statecode = 'All' }
        WorkshopFactory.loadcity(statecode, function(data){
           	$scope.cities = data.cities;
        });
    }
    $scope.titlechange = function(title) {
    }

    var num = 10;
    var off = 1;
    var filter = '';

    var schedules = function(titleslugs, off, filter) {
        WorkshopFactory.schedulelist(titleslugs, off, filter, function(data){
            // $scope.date_choices = data.date_choices;
            $scope.workshops = data.workshops;
            $scope.dates_of_schedule = data.dates_of_schedule
            $scope.maxSize = 5;
            $scope.lastnumber = data.lastnumber;
            $scope.TotalItems = data.total_items;
            $scope.itemfinaloffset = data.counter + 1;
            $scope.CurrentPage = data.index;

        });
    }

    $scope.numpages = function (off, filter) {
        schedules(schedulepage[1], off, filter);
    };
    $scope.setPage = function (pageNo) {
        schedules(schedulepage[1], pageNo, filter);
        off = pageNo;
    };
    $scope.search= function (workshopfilter) {
        var off = 1;
        schedules(schedulepage[1], off, workshopfilter);
        filter = workshopfilter;
    }

    if(schedulepage != null) {
        schedules(schedulepage[1], off, filter);
        $scope.inittitle = schedulepage[1];
    }

    $scope.matchemail =function(email, confirmemail){
        if(email != confirmemail){
            $scope.notmatch = true;
            $scope.wsregform.email.$invalid = true;
        }else{
            $scope.notmatch = false;
        }
    };

    $scope.register = function(wsreg) {
        $scope.invalid = false;
        angular.element(".warning").addClass("hidden");
        angular.element(".chrono1").removeClass("hidden");
        wsreg['workshopid'] = $scope.workshop.workshopid;
        wsreg['titlename'] = $scope.workshop.titlename;
        wsreg['device'] = device;
        wsreg['deviceos'] = deviceos;
        wsreg['devicebrowser'] = devicebrowser;
        wsreg['authorizeid'] = authorizeid;
        wsreg['authorizekey'] = authorizekey;
        WorkshopFactory.registrant(wsreg, function (data){
            console.log('=======================data====================');
            console.log(data);
            angular.element(".chrono1").addClass("hidden");
            angular.element(".warning").addClass("hidden");
            if(data.status != 1) { 
                angular.element(".warning").removeClass("hidden");
                $scope.invalid = true;
            } else if (data.status == 1) {
                $scope.invalid = false;
                anchorSmoothScroll.scrollTo('confirmation');
                angular.element(".wsregform").addClass("hidden");
                angular.element("#confirmation").removeClass("hidden");
                $scope.paymentinfo = data.paymentinfo;
                $scope.registrantinfo = data.registrantinfo;
                $scope.workshopinfo = data.workshopinfo;
                $scope.thisCenter = data.thisCenter;
                $scope.person = $scope.workshop.firstname;
                var datalat = "";
                var datalon = "";
                    if(data.workshopinfo.latitude == ''){
                        datalat = '39.8422862102958';
                        datalon = '-104.95239295312501';
                        $scope.map = {center: {latitude: datalat, longitude: datalon }, zoom: 12 };
                        $scope.options = {scrollwheel: true};
                        $scope.coordsUpdates = 0;
                        $scope.dynamicMoveCtr = 0;
                        $scope.marker = {
                            id: 0,
                            coords: { latitude: datalat, longitude: datalon },
                            options: { draggable: false },
                        }
                    } else {

                        datalat = data.workshopinfo.latitude;
                        datalon = data.workshopinfo.longtitude;

                        $scope.map = {center: {latitude: datalat, longitude: datalon }, zoom: 12 };
                        $scope.options = {scrollwheel: true};
                        $scope.coordsUpdates = 0;
                        $scope.dynamicMoveCtr = 0;
                        $scope.marker = {
                            id: 0,
                            coords: { latitude: datalat, longitude: datalon },
                            options: { draggable: false }
                        }
                    } //end of else
            }

        })
    }

    // REGISTRATION LIST

    if(registrationpage != null) {
        WorkshopFactory.registrationinfo(registrationpage[1], function (data){
            $scope.workshop = data.workshopprop;
            if(data.workshopprop.workshopvenueid == null || data.workshopprop.workshopvenueid == ''){
                authorizeid = data.workshopprop.authorizeid;
                authorizekey = data.workshopprop.authorizekey;
            }
            else{
                authorizeid = data.workshopprop.vauthorizeid;
                authorizekey = data.workshopprop.vauthorizekey;
            }
            
            console.log(authorizeid + '/' + authorizekey);
            if(data.workshopprop.sale == null || data.workshopprop.sale == 0) {
                $scope.discount = false;
            } else {
                $scope.discount = true;
            }
         });

        // $scope.creditcards = [
        //     {card:'Visa', slugs:'visa'},
        //     {card:'Master card', slugs:'master-card'},
        //     {card:'American express', slugs:'american-express'},
        //     {card:'Discover', slugs:'discover'}
        // ]

        $scope.months = [   {month:'01'}, {month:'02'}, {month:'03'},
                            {month:'04'}, {month:'05'}, {month:'06'},
                            {month:'07'}, {month:'08'}, {month:'09'},
                            {month:'10'}, {month:'11'}, {month:'12'}
            ];

        $scope.year = new Date().getFullYear();

        $scope.getTimes=function(n){
            return new Array(n);
        };
    }

    // =============================================== WORKSHOP OVERVIEW 

    WorkshopFactory.overview(function (data) {
        $scope.upcomingschedule = data.upcomingschedule;
        $scope.upcomingscheduledate = data.schedule; 
    });

    
})