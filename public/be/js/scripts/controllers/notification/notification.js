'use strict';

/* Controllers */

app.controller('notificationCtrl', function ($scope, $state, $q,$stateParams, Config, notificationFactory,$interval,ngAudio,$rootScope,toaster,$modal, allintrosessionFactory,allgroupclasssessionFactory,allbeneplaceFactory,$http){
	$scope.notiloader = false;
	// $scope.notiloadertext = true;
	var offset = 5;
    var filter = 'none';
	var globaluserid = '';
	var currentcount = 0;
	var countocompair = 0;
	var centerid = '';
	var uigo = '';
	angular.element('.nifiicon').removeClass('infinite');
	angular.element('.nifiicon').removeClass('bounce');
	angular.element('.nifiicon').removeClass('animated1');



	$scope.userid = function(userid) {
		globaluserid = userid;
		notificationFactory.listnotification(globaluserid, offset, filter, function(data){
			$scope.nofiticationlist = data.notification;
			// $scope.nofiticationlistcount = data.notificationcount.length;
            notificationcountbyuser();
			currentcount = data.notificationcount;
            if(data.notification < 5){
                $scope.notiloadertext = false;
            }
            else{
                $scope.notiloadertext = true;
            }

            if(data.notification < offset){
                $scope.notiloadertext = false;
            }
            else{
                $scope.notiloadertext = true;
            }
		});
	};

    var notificationcountbyuser = function(){
        notificationFactory.notificationcount(globaluserid, function(data){
            $scope.nofiticationlistcount = data.count;
        });
    }



	var loadthat = function(){

		notificationFactory.listnotification(globaluserid, offset, filter, function(data){
			$scope.nofiticationlist = data.notification;
			// $scope.nofiticationlistcount = data.notificationcount.length;
			currentcount = data.notificationcount;
			$scope.notiloader = false;
            if(data.notification < 5){
                $scope.notiloadertext = false;
            }
            else{
                $scope.notiloadertext = true;
            }

            if(data.notification < offset){
                $scope.notiloadertext = false;
            }
            else{
                $scope.notiloadertext = true;
            }
		});
	}
	

	var loadthis = function(){
		notificationFactory.listnotification(globaluserid, offset, filter, function(data){

			$scope.nofiticationlist = data.notification;
			// $scope.nofiticationlistcount = data.notificationcount.length;
            notificationcountbyuser();
			if(currentcount <  data.notificationcount){
				centerid = data.notification[0].centerid;
				uigo = data.notification[0][0].uigo;
				angular.element('.nifiicon').addClass('infinite');
				angular.element('.nifiicon').addClass('bounce');
				angular.element('.nifiicon').addClass('animated1');
				var audio = new Audio('/notifi.mp3');
				audio.play();
				currentcount = data.notificationcount;
				if(data.notification[0].type == 'introsession'){
					toaster.pop('info', "", "{template: 'myTemplateWithData.html', data: '"+data.notification[0].name+"', data2: 'Enrolled to 1-on-1 Intro Session'}", 6000, 'templateWithData', 
					function(toaster) {
						forintrosession(data.notification[0].itemid);
						changestatus(data.notification[0].notificationid);
						return true;
        			});
				}
				else if(data.notification[0].type == 'groupsession'){
					toaster.pop('info', "", "{template: 'myTemplateWithData.html', data: '"+data.notification[0].name+"', data2: 'Enrolled to 1 Group Class + 1-on-1 Intro Session'}", 6000, 'templateWithData',
					function(toaster) {
						forgroupsession(data.notification[0].itemid);
						changestatus(data.notification[0].notificationid);
						return true;
					});
				}
				else if(data.notification[0].type == 'beneplace'){
					toaster.pop('info', "", "{template: 'myTemplateWithData.html', data: '"+data.notification[0].name+"', data2: 'Enrolled to Beneplace'}", 6000, 'templateWithData',
						function(toaster) {
						forbeneplace(data.notification[0].itemid);
						changestatus(data.notification[0].notificationid);
						return true;
					});
				}
				else if(data.notification[0].type == 'message'){
					toaster.pop('info', "", "{template: 'myTemplateWithData.html', data: '"+data.notification[0].name+"', data2: 'Sent you a new message'}", 6000, 'templateWithData',
						function(toaster) {
						formessage(data.notification[0].itemid);
						changestatus(data.notification[0].notificationid);
						return true;
					});
				}
				else if(data.notification[0].type == 'workshop'){
					toaster.pop('info', "", "{template: 'myTemplateWithData.html', data: '"+data.notification[0].name+"', data2: 'Enrolled to Workshop'}", 6000, 'templateWithData',
						function(toaster) {
						forworkshop(data.notification[0].itemid);
						changestatus(data.notification[0].notificationid);
						return true;
					});
				}
                else if(data.notification[0].type == 'story'){
                    toaster.pop('info', "", "{template: 'myTemplateWithData.html', data: '"+data.notification[0].name+"', data2: 'Sent you Testimonies'}", 6000, 'templateWithData',
                        function(toaster) {
                        forstory(data.notification[0].itemid);
                        changestatus(data.notification[0].notificationid);
                        return true;
                    });
                }
				

			}
		});
	}
	
	
    $interval(function(){
        notificationcountbyuser();
    	loadthis();
    },10000);

    var limitStep = 5;
    $scope.incrementLimit = function() {
    	offset += limitStep;
    	$scope.notiloader = true;
    	$scope.notiloadertext = false;
    	loadthat();
    	$scope.limit = offset;
    };
    $scope.decrementLimit = function() {
    	$scope.limit -= limitStep;
    };

    $scope.removeinfi = function(){
    	angular.element('.nifiicon').removeClass('bounce');
    	angular.element('.nifiicon').removeClass('animated1');
    	angular.element('.nifiicon').removeClass('infinite');
        notificationFactory.notificationcountzero(globaluserid, function(data){
            $scope.nofiticationlistcount = data.count;
        });
    };

    $scope.viewnotification = function(notificationid,itemid,type,tempcenterid,tempuigo){
    	centerid = tempcenterid;
    	uigo = tempuigo;
    	if(type == 'introsession'){
    		forintrosession(itemid);
    	}
    	else if(type == 'groupsession'){
    		forgroupsession(itemid);
    	}
    	else if(type == 'beneplace'){
    		forbeneplace(itemid);
    	}
    	else if(type == 'message'){
    		formessage(itemid);
    	}
    	else if(type == 'workshop'){
    		forworkshop(itemid);
    	}
        else if(type == 'story'){
            forstory(itemid);
        }
        

    	changestatus(notificationid);
    	

    };

    var forintrosession = function(itemid){
    	var modalInstance = $modal.open({
    		templateUrl: 'sessionView.html',
    		controller: sessionViewCTRL,
    		resolve: {
    			itemid: function() {
    				return itemid
    			}
    		}
    	});

    	
    }; //end of forintrosession

    var sessionViewCTRL = function($scope, $modalInstance, itemid, $state) {
    	allintrosessionFactory.loadsession(itemid,globaluserid, function(data){
    		$scope.session = data;
    		if(uigo == 'admin'){
    			$state.go('allintrosession', {userid: globaluserid });
    		}
    		else{
    			$state.go('centerview', {centerid: centerid, uifrom: 'introsession'});
    		}
    		
    	})

    	$scope.cancel = function () {
    		$modalInstance.dismiss('cancel');
    	};
    };

    var forgroupsession = function(itemid){
    	var modalInstance = $modal.open({
            templateUrl: 'groupsessionView.html',
            controller: groupsessionViewCTRL,
            resolve: {
                itemid: function() {
                    return itemid
                }
            }
        });
    }; //end of forgroupsession

    var groupsessionViewCTRL = function($scope, $modalInstance, itemid, $state) {
    	allgroupclasssessionFactory.loadgroupsession(itemid, globaluserid, function(data){
    		$scope.session = data;
    		if(uigo == 'admin'){
    			$state.go('allgroupclasssession', {userid: globaluserid });
    		}
    		else{
    			$state.go('centerview', {centerid: centerid, uifrom: 'groupsession'});
    		}
    	})

    	$scope.cancel = function () {
    		$modalInstance.dismiss('cancel');
    	};
    };

    var forbeneplace = function(itemid){
    	var modalInstance = $modal.open({
            templateUrl: 'beneplaceView.html',
            controller: beneplaceViewCTRL,
            resolve: {
                itemid: function() {
                    return itemid
                }
            }
        });
    }; //end of forbeneplace

    var beneplaceViewCTRL = function($scope, $modalInstance, itemid, $state) {
        allbeneplaceFactory.loadgroupsession(itemid, globaluserid, function(data){
            $scope.session = data;
            if(uigo == 'admin'){
    			$state.go('allbeneplace', {userid: globaluserid });
    		}
    		else{
    			$state.go('centerview', {centerid: centerid, uifrom: 'beneplace'});
    		}
        })

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }

    var formessage = function(itemid){
    	$scope.process = false;
    	var modalInstance = $modal.open({
    		templateUrl: 'contactReview.html',
    		controller: reviewcontactCTRL,
    		resolve: {
    			itemid: function () {
    				return itemid;
    			}
    		}
    	});
    }; //end of formessage

     var reviewcontactCTRL = function($scope, $modalInstance, itemid, $sce) {
           $scope.process = false;
           $http({
            url: Config.ApiURL+"/managecontacts/listreplies/" + itemid,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
           $scope.datalist = data;
		   $state.go('managecontacts');

       })

        $scope.process = false;
        $http({
            url: Config.ApiURL+"/message/view/" + itemid,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
            $scope.rid = data.id;
            $scope.reply.rid = data.id;
            $scope.cname = data.name;
            $scope.email = data.email;
            $scope.reply.email = data.email;
            $scope.content = data.content;
            $scope.reply.content = data.content;
            $scope.datesubmitted = data.datesubmitted;
            $scope.status = data.status;
        })

        $scope.reply = function(itemid) {

        };

        $scope.clear = function() {
            $scope.reply.messages = [];
            $scope.formsubmit.$setPristine(true);
        };
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.send = function (reply){
            $http({
                url: Config.ApiURL + "/message/reply",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(reply)
            }).success(function (data, status, headers, config) {
                $modalInstance.dismiss('cancel');
            }).error(function (data, status, headers, config) {

            });
        };

    };

    var forworkshop = function(itemid){
    	$state.go('createworkshop', {tab: '4' });
    }; //end of forworkshop

    var forstory = function(itemid){
        if(uigo == 'admin'){
            $state.go('viewstory', {storyid: itemid });
        }
        else{
            $state.go('viewstory', {storyid: itemid });
        }
    }; //end of forstory

    var changestatus = function(notificationid){
    	notificationFactory.changestatus(notificationid, globaluserid, function(data){
    		// loadthis();
    		loadthat();
    	});
    };



    

})