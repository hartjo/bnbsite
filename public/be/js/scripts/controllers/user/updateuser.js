app.controller('UserUpdateCtrl', function ($http, $scope, $stateParams, $modal, $sce, Config, Usersfactory, Upload) {
     //{MODAL**
        var msgCTRL = function ($scope, $modalInstance, $state, message) {
            $scope.message = message;
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }
        var msgmodal = function(msg){
            var message ="neil";
            var modalInstance = $modal.open({
                templateUrl: 'success.html',
                controller: msgCTRL,
                resolve: {
                    message: function () {
                        return msg;
                    }
                }
            });
        }
                                //**MODAL}//

                                var loadUserInfo = function() {
                                  $http({
                                     url: Config.ApiURL + "/user/info/"+$stateParams.userid,
                                     method: "GET",
                                     headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                 }).success(function (data, status, headers, config) {
                                    console.log(data);
                                     if(data.bday == '' || data.bday == null || data.bday == undefined || data.bday == '0000-00-00'){
                                         $scope.user = data;
                                         $scope.user.region = data.roleid;
                                         // $scope.user.district = data.districtid;
                                         $scope.user.center = data.roleid;
                                         $scope.defaultuser = data.username;
                                         $scope.defaultemail = data.email;
                                         $scope.user.bday = ''
                                     }
                                     else{
                                         $scope.user = data;
                                         $scope.user.region = data.roleid;
                                         // $scope.user.district = data.districtid;
                                         $scope.user.center = data.roleid;
                                         $scope.defaultuser = data.username;
                                         $scope.defaultemail = data.email;
                                     }
                                     
                                 })
                             }

                             loadUserInfo();

                             $scope.chkusername =function() {
                                var username = $scope.user.username;
                                if($scope.defaultuser != username){
                                    $http({
                                        url:Config.ApiURL + "/validate/username/"+username,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            // data: $.param(dlt)
        }).success(function (data, status, headers, config) {
            $scope.usrname= data.exists;
            var result = $scope.usrname;
            if(result == true) {
             $scope.form.$invalid = true;
         }
     })}else{
            $scope.usrname= false;
        }
    };
    //VALIDATE EMAIL
    $scope.chkemail =function(){
        var email = $scope.user.email;
        if($scope.defaultemail != email){
            $http({
                url: Config.ApiURL + "/validate/email/"+email,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            // data: $.param(dlt)
        }).success(function (data, status, headers, config) {
            $scope.usremail= data.exists;
            if(data.exists == true){
                $scope.form.$invalid = true;
            }
        })}else{
            $scope.usremail= false;
        }
    };

    $scope.closeAlert = function(index, type) {
        if(type = 'image') {
            $scope.imagealert.splice(index, 1);
        }
    };

    $scope.updateData = function(user, file){
        if(file == undefined) {
            $scope.processing = true;
          if (user.bday == undefined) { user.bday = new Date(user.bday); } 
          else {  user.bday = new Date(user.bday); }

         $http({
            url: Config.ApiURL +"/user/update",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(user)
        }).success(function (data, status, headers, config) {
            $scope.processing = false;
            loadUserInfo();
            if(data.success == "TOOSHORT")
            {
                msgmodal("Password is too Short");
            }
            if(data.success == "OLDPASSWORDNOTMATCH")
            {
                msgmodal("Old Password not Match");
            }
            if(data.success == "CONFIRMPASSWORDNOTMATCH")
            {
                msgmodal("Confirm Password not Match");
            }
            if(data.success == "UPDATED")
            {
                msgmodal("User has been successfuly updated");
            }
            if(data == "Success")
            {
                msgmodal("User has been successfuly updated");
            }
            REGDISCEN();
        });

    } else {
      $scope.upload(user,$scope.file);
  }

};

$scope.upload = function (user,files) 
{
    $scope.processing = true;

    if (user.bday == undefined) { user.bday = new Date(user.bday); } 
    else {  user.bday = new Date(user.bday); }

    $scope.imagealert = [];
    var filename
    var filecount = 0;
    if (files && files.length) 
    {

        for (var i = 0; i < files.length; i++) 
        {
            var file = files[i];

            if (file.size >= 6000000) //6MB
            {
                $scope.imagealert.push({type: 'danger', msg: 'File ' + file.name + ' is too big. (Maximum of 6MB)'});
                $scope.processing = false;
            }
            else
            {

                var promises;

                promises = Upload.upload({

                                url: Config.amazonlink, //S3 upload url including bucket name
                                method: 'POST',
                                transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                            },
                            fields : {
                                  key: 'uploads/userimages/' + file.name, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                              },
                              file: file
                          })

promises.then(function(data){

    filecount = filecount + 1;
    filename = data.config.file.name;

    user['profile_pic_name'] = filename;

    $http({
      url: Config.ApiURL +"/user/update",
      method: "POST",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      data: $.param(user)
  }).success(function (data, status, headers, config) {
    $scope.processing = false;
      loadUserInfo();
      if(data.success == "TOOSHORT")
      {
        msgmodal("Password is too Short");
    }
    if(data.success == "OLDPASSWORDNOTMATCH")
    {
        msgmodal("Old Password not Match");
    }
    if(data.success == "CONFIRMPASSWORDNOTMATCH")
    {
        msgmodal("Confirm Password not Match");
    }
    if(data.success == "UPDATED")
    {
        msgmodal("User has been successfuly updated");
    }
    if(data == "Success")
    {
        msgmodal("User has been successfuly updated");
    }
    REGDISCEN();

});
})           
}
}
}    
};

var REGDISCEN = function() { 
   Usersfactory.availableREGDISCENlist($stateParams.userid, function(data) {	
        if(data.upd_regions != '' ) { 
           $scope.availableregion = data.upd_regions;
        } 
        if(data.upd_districts != '' ) {
           $scope.availabledistrict = data.upd_districts;
        } 
        if(data.upd_centers != '' ) {
        $scope.availablecenter = data.upd_centers;
        }
    });
}
REGDISCEN();

	//DATE PICKER
	$scope.today = function () {
        $scope.dt = new Date();
    };

    $scope.today();
    $scope.clear = function () {
        $scope.dt = null;

    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
});
app.controller('ProfileUpdateCtrl', function($http, $scope, $stateParams, $modal, $sce, Config, Usersfactory, Upload) {
     //{MODAL**

        var msgCTRL = function ($scope, $modalInstance, $state, message) {
            $scope.message = message;
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }
        var msgmodal = function(msg){
            var message ="neil";
            var modalInstance = $modal.open({
                templateUrl: 'success.html',
                controller: msgCTRL,
                resolve: {
                    message: function () {
                        return msg;
                    }
                }
            });
        }
                                //**MODAL}//

                                var loadUserInfo = function() {
                                    $http({
                                        url: Config.ApiURL + "/user/info/"+userid,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                    }).success(function (data, status, headers, config) {
                                        $scope.user = data;
                                        $scope.user.region = data.regionid;
                                        $scope.user.district = data.districtid;
                                        $scope.user.center = data.centerid;
                                        $scope.defaultuser = data.username;
                                        $scope.defaultemail = data.email;
                                    })
                                }

                                loadUserInfo();

                                $scope.chkusername =function() {
                                    var username = $scope.user.username;
                                    if($scope.defaultuser != username){
                                        $http({
                                            url:Config.ApiURL + "/validate/username/"+username,
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            // data: $.param(dlt)
        }).success(function (data, status, headers, config) {
            $scope.usrname= data.exists;
            var result = $scope.usrname;
            if(result == true) {
             $scope.form.$invalid = true;
         }
     })}else{
            $scope.usrname= false;
        }
    };
    //VALIDATE EMAIL
    $scope.chkemail =function(){
        var email = $scope.user.email;
        if($scope.defaultemail != email){
            $http({
                url: Config.ApiURL + "/validate/email/"+email,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            // data: $.param(dlt)
        }).success(function (data, status, headers, config) {
            $scope.usremail= data.exists;
            if(data.exists == true){
                $scope.form.$invalid = true;
            }
        })}else{
            $scope.usremail= false;
        }
    };

    $scope.closeAlert = function(index, type) {
        if (type=='image') {
            $scope.imagealert.splice(index, 1);
        }
    };

    $scope.updateData = function(user, file){
        if(file == undefined) {
            $scope.processing = true;
            if (user.bday == undefined) { user.bday = new Date(user.bday); } 
            else {  user.bday = new Date(user.bday); }

            $http({
                url: Config.ApiURL +"/user/update",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(user)
            }).success(function (data, status, headers, config) {
                $scope.processing = false;
                loadUserInfo();
                if(data.success == "TOOSHORT")
                {
                    msgmodal("Password is too Short");
                }
                if(data.success == "OLDPASSWORDNOTMATCH")
                {
                    msgmodal("Old Password not Match");
                }
                if(data.success == "CONFIRMPASSWORDNOTMATCH")
                {
                    msgmodal("Confirm Password not Match");
                }
                if(data.success == "UPDATED")
                {
                    msgmodal("User has been successfuly updated");
                }
                if(data == "Success")
                {
                    msgmodal("User has been successfuly updated");
                }
                    REGDISCEN();
            });

        } else {
          $scope.upload(user,$scope.file);
      }

  };

  $scope.upload = function (user,files) 
  {
     if(files === undefined || files === null || files === ''){

        $http({
                url: Config.ApiURL +"/user/update",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(user)
            }).success(function (data, status, headers, config) {
                $scope.processing = false;
                loadUserInfo();
                if(data.success == "TOOSHORT")
                    {
                        msgmodal("Password is too Short");
                    }
                    if(data.success == "OLDPASSWORDNOTMATCH")
                    {
                        msgmodal("Old Password not Match");
                    }
                    if(data.success == "CONFIRMPASSWORDNOTMATCH")
                    {
                        msgmodal("Confirm Password not Match");
                    }
                    if(data.success == "UPDATED")
                    {
                        msgmodal("User has been successfuly updated");
                    }
                    if(data == "Success")
                    {
                        msgmodal("User has been successfuly updated");
                    }
                REGDISCEN();
            });
     }
     else{

            $scope.processing = true;
           if (user.bday == undefined) { user.bday = new Date(user.bday); } 
           else {  user.bday = new Date(user.bday); }

            $scope.imagealert = [];
            var filename
            var filecount = 0;
            if (files && files.length) 
            {

                for (var i = 0; i < files.length; i++) 
                {
                    var file = files[i];

                    if (file.size >= 2000000) // 6MB
                    {
                        $scope.imagealert.push({type: 'danger', msg: 'File ' + file.name + ' is too big. (Maximum of 6MB)'});
                        $scope.processing = false;
                    }
                    else
                    {

                        var promises;

                        promises = Upload.upload({

                                        url: Config.amazonlink, //S3 upload url including bucket name
                                        method: 'POST',
                                        transformRequest: function (data, headersGetter) {
                                        //Headers change here
                                        var headers = headersGetter();
                                        delete headers['Authorization'];
                                        return data;
                                    },
                                    fields : {
                                          key: 'uploads/userimages/' + file.name, // the key to store the file on S3, could be file name or customized
                                          AWSAccessKeyId: Config.AWSAccessKeyId,
                                          acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
                                          policy: Config.policy, // base64-encoded json policy (see article below)
                                          signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                          "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                      },
                                      file: file
                                  })

        promises.then(function(data){

            filecount = filecount + 1;
            filename = data.config.file.name;

            user['profile_pic_name'] = filename;

            $http({
                url: Config.ApiURL +"/user/update",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(user)
            }).success(function (data, status, headers, config) {
                $scope.processing = false;
                loadUserInfo();
                if(data.success == "TOOSHORT")
                    {
                        msgmodal("Password is too Short");
                    }
                    if(data.success == "OLDPASSWORDNOTMATCH")
                    {
                        msgmodal("Old Password not Match");
                    }
                    if(data.success == "CONFIRMPASSWORDNOTMATCH")
                    {
                        msgmodal("Confirm Password not Match");
                    }
                    if(data.success == "UPDATED")
                    {
                        msgmodal("User has been successfuly updated");
                    }
                    if(data == "Success")
                    {
                        msgmodal("User has been successfuly updated");
                    }
                REGDISCEN();
            });
        })           
        }
        }
        } 

 }
};

    //DATE PICKER
    $scope.today = function () {
        $scope.dt = new Date();
    };

    $scope.today();
    $scope.clear = function () {
        $scope.dt = null;

    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
});
