'use strict';

/* Controllers */

app.controller('pricingCtrl', function($scope, $state ,$q, $http, Config, $stateParams , $modal, pricingFactory){

    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    pricingFactory.loadfee($stateParams.centerid,function(data){

        $scope.regfee = data.regfee.fee;
        $scope.regfeestatus = data.regfee.status;

        $scope.regclass = data.regclass.fee;
        $scope.regclassstatus = data.regclass.status;

        $scope.sessionfee = data.sessionfee.fee;
        $scope.sessionfeestatus = data.sessionfee.status;

    })

    $scope.updatepricing = function(regfee,rfstatus,regclass,rgstatus,sessionfee,sfstatus){
      

        var allfee = {'centerid':$stateParams.centerid, 'regfee':regfee, 'regclass':regclass, 'sessionfee':sessionfee,
                       'regfeestatus':rfstatus, 'regclassstatus':rgstatus, 'sessionfeestatus':sfstatus }

        pricingFactory.savefee(allfee,function(data){

            $scope.alerts[0]= {type: data.type, msg: data.msg};

        })

    }

    $scope.regfeeclick = function(status){

        if(status == 1){
            pricingFactory.statusregfee($stateParams.centerid, status,function(data){

               

            })
        }
        else{
            pricingFactory.statusregfee($stateParams.centerid, status,function(data){

                

            })
        }

    }

    $scope.regclassclick = function(status){

        if(status == 1){
            pricingFactory.statusregclass($stateParams.centerid, status,function(data){

               

            })
        }
        else{
            pricingFactory.statusregclass($stateParams.centerid, status,function(data){

               

            })
        }

    }

    $scope.sessionfeeclick = function(status){

        if(status == 1){
            pricingFactory.statussessionfee($stateParams.centerid, status,function(data){

            })
        }
        else{
            pricingFactory.statussessionfee($stateParams.centerid, status,function(data){

            })
        }

    }

    $scope.membershipalerts = [];

    $scope.closemembershipAlert = function (index) {
        $scope.membershipalerts.splice(index, 1);
    };

    var membershipalertsuccess = function(){
        $scope.membershipalerts[0]= {type: 'success', msg: 'Membership Successfully Added'};
    }

    $scope.addnewmembership = function(){
        var modalInstance = $modal.open({
            templateUrl: 'membershipAdd.html',
            controller: membershipAddCTRL,
        })
    }

    var membershipAddCTRL = function($scope, $modalInstance){

        $scope.ok = function(membership) {
          
            membership['centerid'] = $stateParams.centerid;
            // console.log(membership);
            pricingFactory.savemembership(membership,function(data){

                membershipalertsuccess();
                membershiploader();
                $modalInstance.dismiss('cancel');

            })

        }

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        }

    }

    var membershiploader = function(){

        pricingFactory.loadmembership($stateParams.centerid,function(data){
           $scope.membershipdata = data;
        })

    }

    membershiploader();

    $scope.editmembership = function(membershipid){
        var modalInstance = $modal.open({
            templateUrl: 'membershipEdit.html',
            controller: membershipEditCTRL,
            resolve: {
                membershipid: function() {
                    return membershipid
                }
            }
        });
    }

    var membershipEditCTRL = function($scope, $modalInstance, membershipid) {

        $http({
            url: Config.ApiURL + "/center/pricing/loadmembershipbyid/" + membershipid,
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function (data, status, headers, config) {
            $scope.membership = data;
        }).error(function (data, status, headers, config) {
            $modalInstance.dismiss('cancel');
        });

        $scope.ok = function(membership) {

           $http({
                url: Config.ApiURL + "/center/pricing/savemembershipbyid",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(membership)
            }).success(function (data, status, headers, config) {
                membershipeditAlertsuccess();
                membershiploader();
                $modalInstance.dismiss('cancel');
            }).error(function (data, status, headers, config) {
                $modalInstance.dismiss('cancel');
            });

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };


    }

    $scope.membershipalerts = [];

    $scope.closemembershipeditAlert = function (index) {
        $scope.membershipalerts.splice(index, 1);
    };

    var membershipeditAlertsuccess = function(){
        $scope.membershipalerts[0]= {type: 'success', msg: 'Membership Successfully Updated'};
    }

    $scope.deletemembership = function(membershipid){
        var modalInstance = $modal.open({
            templateUrl: 'membershipDelete.html',
            controller: membershipDeleteCTRL,
            resolve: {
                membershipid: function() {
                    return membershipid
                }
            }
        });
    }


    var membershipDeleteCTRL = function($scope, $modalInstance, membershipid) {

        $scope.ok = function(membership) {
            
           $http({
                url: Config.ApiURL + "/center/pricing/deletemembershipbyid/" + membershipid,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                membershipdeleteAlertsuccess();
                membershiploader();
                $modalInstance.dismiss('cancel');
            }).error(function (data, status, headers, config) {
                $modalInstance.dismiss('cancel');
            });

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };


    }

    $scope.membershipalerts = [];

    $scope.closemembershipeditAlert = function (index) {
        $scope.membershipalerts.splice(index, 1);
    };

    var membershipdeleteAlertsuccess = function(){
        $scope.membershipalerts[0]= {type: 'success', msg: 'Membership Successfully Deleted!'};
    }

    // ====================================>>> PRIVATE SESSION
    $scope.privatesession = {};
    pricingFactory.loadPrivateSession($stateParams.centerid, function(data){
        if(data.privatesession == null) {
            $scope.privatesession = {
                status : 0,
            }
            private_checkstatus(0);
        } else {
            $scope.privatesession = {
                status : data.privatesession.status,
                details : data.privatesession.details
            }
            private_checkstatus(data.privatesession.status);
        }
    });

    $scope.privatestatus = function(status) {
        private_checkstatus(status);
    }
    var private_checkstatus = function(status) {
        if(status == 1) {
            $scope.type = 'info';
            $scope.statusmsg = 'Active';
        } else {
            $scope.type = 'danger';
            $scope.statusmsg = 'Hidden';
        }
    }

    $scope.privatesessionAlert = [];
    $scope.saveprivatesession = function(asset) {
        asset['centerid'] = $stateParams.centerid;
        console.log(asset)
        pricingFactory.saveprivatesession(asset, function (data){
            $scope.privatesessionAlert[0] = {type: data.stat , msg: data.statusmsg };
        });
    }
    $scope.closeprivatesessionAlert = function (index) {
        $scope.privatesessionAlert.splice(index, 1);
    }

})