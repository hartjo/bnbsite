'use strict';

/* Controllers */

app.controller('beneplaceCtrl', function($scope, $state, Upload ,$q, $http, Config, $stateParams,beneplaceFactory, $modal){

     $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    var num = 10;
    var off = 1;
    var keyword = null;

    $scope.search = function (searchkeyword) {
        keyword = searchkeyword;
        beneplaceFactory.loadclasslist(num,off, keyword,$stateParams.centerid, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })

    }
    
    $scope.numpages = function (off, keyword) {
        beneplaceFactory.loadclasslist(num,off, keyword,$stateParams.centerid, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })
    }

    $scope.setPage = function (pageNo) {
        off = pageNo;
        beneplaceFactory.loadclasslist(num,pageNo, keyword,$stateParams.centerid, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })
    };
    
    var loadnews = function(){
        beneplaceFactory.loadclasslist(num,off, keyword,$stateParams.centerid, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })

    }

    loadnews();

    $scope.viewsession = function(sessionid){
        var modalInstance = $modal.open({
            templateUrl: 'beneplaceView.html',
            controller: beneplaceViewCTRL,
            resolve: {
                sessionid: function() {
                    return sessionid
                }
            }
        });
    }

    var beneplaceViewCTRL = function($scope, $modalInstance, sessionid, $state) {
        beneplaceFactory.loadgroupsession(sessionid, $stateParams.userid, function(data){
            $scope.session = data;
            loadnews();
        })

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }

})