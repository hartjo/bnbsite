'use strict';

/* Controllers */

app.controller('Createcenter', function($scope, $state, Upload ,$q, $http, Config, CreateCenterFactory, $location, anchorSmoothScroll, $timeout, $modal){

    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.center = {

        status:1,
        centertype:1
    };

    var oricenter = angular.copy($scope.center);

    $scope.saveCenter = function(center)
    {
        CreateCenterFactory.savecenter(center,function(data){

            $scope.alerts[0]= {type: data.type, msg: data.success};
            // clear fields
            $scope.center = angular.copy(oricenter);
            $scope.formcreatecenter.$setPristine();
            //scroll top
            $location.hash('gototop');
            anchorSmoothScroll.scrollTo('gototop');
            
        });
        
    }

    $scope.oncentertitle = function convertToSlug(Text)
    {

        if(Text == null)
        {
            $scope.center.slugs = '';
        }
        else
        {
            var text1 = Text.replace(/[^\w ]+/g,'');
            $scope.center.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
        }
        
    }

    CreateCenterFactory.loadregionmanager(function(data1){
         $scope.regionmanager = data1;
         // $scope.center.centerstate = data[0];

    });

    CreateCenterFactory.listregion(function (data){
        if(data != '') {
            $scope.regions = data;
            $scope.center.centerregion = data[0].regionid;
            $scope.hideregion = false;
        } else { $scope.hideregion = true; }

    });

    CreateCenterFactory.loadcentermanager(function(data3){
         $scope.centermanager = data3;
         // $scope.center.centerstate = data[0];

    });


    CreateCenterFactory.loadstate(function(data){
         $scope.centerstate = data;
         // $scope.center.centerstate = data[0];

    });


    $scope.statechange = function(statecode)
    {
        CreateCenterFactory.loadcity(statecode,function(data){
            
            $scope.centercity = data;
            $scope.center.centercity =data[0].city;
            
            CreateCenterFactory.loadzip(data[0].city,function(data){
                // console.log(data);
                $scope.getcenterzip = data;
                $scope.center.centerzip =data[0].zip;
            });

        });
    }

    $scope.citychange = function(cityname)
    {

        CreateCenterFactory.loadzip(cityname,function(data){
         // $scope.center.centerzip =data[0].zip;
         CreateCenterFactory.loadzip(data[0].city,function(data){
            // console.log(data);
            $scope.getcenterzip = data;
            $scope.center.centerzip =data[0].zip;
        });
        });
    }


    $scope.authorizehelp = function() {
        var modalInstance = $modal.open({
            templateUrl: 'authorizeHelp.html',
            controller: authorizeHelpCTRL
        });
    }
       
    var authorizeHelpCTRL = function($scope, $modalInstance) {
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    };

    $scope.paypalhelp = function() {
        var modalInstance = $modal.open({
            templateUrl: 'paypalHelp.html',
            controller: paypalhelpCTRL
        });
    }
       
    var paypalhelpCTRL = function($scope, $modalInstance) {
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    };

    $scope.percent = function(price){
        var getpercent = .25 * price;
        $scope.center.dis1monthprice = price - getpercent;
    }

    $scope.percent2 = function(price){
        var getpercent = .25 * price;
        $scope.center.dis3monthprice = price - getpercent;
    }
    
    //DATE PICKER
    $scope.today = function () {
    $scope.dt = new Date();
    };

    $scope.today();
    $scope.clear = function () {
    $scope.dt = null;
       
    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

})