'use strict';

/* Controllers */

app.controller('scheduleCtrl', function ($scope, $state, Config, $stateParams, scheduleFactory ,$modal){

	var loadschedule = function(){
		scheduleFactory.loadschedule($stateParams.centerid, function(data){
			$scope.schedlelist = data;
            console.log(data);
		});
	}
	loadschedule();

	$scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    }

	var addalert = function(type,message){
		 $scope.alerts[0]= {type: type, msg: message};
	}


    $scope.editsession = function(sessionid) {
        var modalInstance = $modal.open({
            templateUrl: 'scheduleedit.html',
            controller: scheduleEditCTRL,
            resolve: {
                sessionid: function() {
                    return sessionid
                }
            }
        });
    }

    var scheduleEditCTRL = function($scope, $modalInstance, sessionid, $state) {
    	$scope.hourlist = [
         {time:'00'},
         {time:'01'},
         {time:'02'},
         {time:'03'},
         {time:'04'},
         {time:'05'},
         {time:'06'},
         {time:'07'},
         {time:'08'},
         {time:'09'},
         {time:'10'},
         {time:'11'},
         {time:'12'}];

         $scope.minlist = [
         {time:'00'},
         {time:'05'},
         {time:'10'},
         {time:'15'},
         {time:'20'},
         {time:'25'},
         {time:'30'},
         {time:'35'},
         {time:'40'},
         {time:'45'},
         {time:'50'},
         {time:'55'}];

        $scope.classlist = [
         {classlist:'Basic Yoga'},
         {classlist:'Tai Chi'},
         {classlist:'Meridian Stretching'},
         {classlist:'Yoga and Qigong'},
         {classlist:'Advanced Yoga'},
         {classlist:'Core Strengthening'},
         {classlist:'Energy Meditation'},
         {classlist:'Energy Movement'},
         {classlist:'Yoga for Kids'},
         {classlist:'Yoga for Young Adults'},
         {classlist:'Successful Aging'},
         {classlist:'Yoga en Español'},
         {classlist:'Open Class'},
         {classlist:'Special Class'}];

         $scope.daylist = [
         {day:'Monday'},
         {day:'Tuesday'},
         {day:'Wednesday'},
         {day:'Thursday'},
         {day:'Friday'},
         {day:'Saturday'},
         {day:'Sunday'}];

         scheduleFactory.loadschedulebyid(sessionid, function(data){
            console.log(data);
            $scope.session = data;
            $scope.session.starthourtime = data.starttime.slice( 0, 2 );
            $scope.session.startmintime = data.starttime.slice( 3, 5 );
            $scope.session.starttimeformat = data.starttime.slice( 6, 8 );
            $scope.session.endhourtime = data.endtime.slice( 0, 2 );
            $scope.session.endmintime = data.endtime.slice( 3, 5 );
            $scope.session.endtimeformat = data.endtime.slice( 6, 8 );
         });


        $scope.ok = function(session) {
            scheduleFactory.updateschedule(session, function(data){
                addalert(data.type,data.msg);
                loadschedule();
            });

            $modalInstance.dismiss('cancel');
        };
        $scope.cancel = function () {

            $modalInstance.dismiss('cancel');
        };
    }






    $scope.addnewschedule = function() {
        var modalInstance = $modal.open({
            templateUrl: 'scheduleAdd.html',
            controller: scheduleAddCTRL
        });
    }

    var scheduleAddCTRL = function($scope, $modalInstance, $state, scheduleFactory) {
    	$scope.hourlist = [
         {time:'00'},
         {time:'01'},
         {time:'02'},
         {time:'03'},
         {time:'04'},
         {time:'05'},
         {time:'06'},
         {time:'07'},
         {time:'08'},
         {time:'09'},
         {time:'10'},
         {time:'11'},
         {time:'12'}];

         $scope.minlist = [
         {time:'00'},
         {time:'05'},
         {time:'10'},
         {time:'15'},
         {time:'20'},
         {time:'25'},
         {time:'30'},
         {time:'35'},
         {time:'40'},
         {time:'45'},
         {time:'50'},
         {time:'55'}];


        $scope.classlist = [
         {classlist:'Basic Yoga'},
         {classlist:'Tai Chi'},
         {classlist:'Meridian Stretching'},
         {classlist:'Yoga and Qigong'},
         {classlist:'Advanced Yoga'},
         {classlist:'Core Strengthening'},
         {classlist:'Energy Meditation'},
         {classlist:'Energy Movement'},
         {classlist:'Yoga for Kids'},
         {classlist:'Yoga for Young Adults'},
         {classlist:'Successful Aging'},
         {classlist:'Yoga en Español'},
         {classlist:'Open Class'},
         {classlist:'Special Class'}]

         $scope.daylist = [
         {day:'Monday'},
         {day:'Tuesday'},
         {day:'Wednesday'},
         {day:'Thursday'},
         {day:'Friday'},
         {day:'Saturday'},
         {day:'Sunday'}]


        $scope.ok = function(session) {
            console.log(session);
        	session['centerid'] = $stateParams.centerid;
        	scheduleFactory.saveschedule(session, function(data){
                console.log(data);
        		addalert(data.type,data.msg);
        		loadschedule();
        	})
        	$modalInstance.dismiss('cancel');
            
        };
        $scope.cancel = function () {

            $modalInstance.dismiss('cancel');
        };
    }

    $scope.delete = function(sessionid) {
        var modalInstance = $modal.open({
            templateUrl: 'scheduleDelete.html',
            controller: scheduleDeleteCTRL,
            resolve: {
                sessionid: function() {
                    return sessionid
                }
            }
        });
    };

    var scheduleDeleteCTRL = function($scope, $modalInstance, $state, sessionid) {

        $scope.ok = function() {
            scheduleFactory.deleteschedule(sessionid, function(data){
                addalert(data.type,data.msg);
                loadschedule();
            })
            $modalInstance.dismiss('cancel');
            
        };
        $scope.cancel = function () {

            $modalInstance.dismiss('cancel');
        };
    };



})