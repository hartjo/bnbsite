app.controller("centerincomefranchiseCtrl", function ($scope, Config, CenterincomeFactory){
	$scope.filter = {};
    $scope.filterperday = {};
    $scope.filterpermonth = {};

// ======================= 1ON1 INTRO SESSION TAB ====================
	var nofiltertab3 = function() {
      CenterincomeFactory.allfranchise(function (data){
            $scope.one_f_sessions = data.sessions;
            $scope.one_f_totalincome = data.totalincome;
            $scope.one_f_totalregistrant = data.totalregistrant;
            $scope.one_f_finalquantity = data.finalquantity;
            var allcenters = {centerid:12345, centertitle:"All"};
            var allstates = {state_code:"ALL", state:"All"}
            data.centers.splice(0, 0, allcenters);
            data.states.splice(0, 0, allstates);
            $scope.centers = data.centers;
            $scope.states = data.states;
        });
	}
	nofiltertab3();

	$scope.filtertab3 = function(filter) {
        CenterincomeFactory.filterallfranchise(filter, function (data) {
            $scope.one_f_sessions = data.sessions;
            $scope.one_f_totalincome = data.totalincome;
            $scope.one_f_totalregistrant = data.totalregistrant;
            $scope.one_f_finalquantity = data.finalquantity;
        });
	}

    var nofilter = function() {
        $scope.filter = {
            fromyear    : yearNow,
            toyear      : yearNow,
            frommonth   : monthNow,
            tomonth     : monthNow,
            center      : 12345,
            state       : "ALL",
        };
        $scope.slice = monthNow-1;
    }
    nofilter();

	$scope.nofiltertab3 = function() {
		nofiltertab3();
		nofilter();
	}

// =================================================================================== //
	//custom DATESAVER [..] prerequisite (MOMENT)
    var monthNow = moment().format('MM');
    var dayNow = moment().format('DD');
    var yearNow = new Date().getFullYear();

    $scope.yearNow = yearNow;
    $scope.monthNow = monthNow;
    $scope.dayNow = dayNow;

    var date = new Date();
    var date2 = new Date();
    var date3 = new Date();
    $scope.day10daysbefore = moment(date.setDate(date.getDate() - 9)).format('DD');
    $scope.month10daysbefore = moment(date2.setDate(date2.getDate() - 9)).format('MM');
    $scope.year10daysbefore = parseInt(moment(date3.setDate(date3.getDate() - 9)).format('YYYY'));

    $scope.slice = monthNow-1;
    $scope.dayslice = dayNow-1;
    $scope.initmonthto = monthNow;

    var datemonth = function() { 
    	$scope.datemonth = [
        {name: "Jan", value: "01"}, {name: "Feb", value: "02"},
        {name: "Mar", value: "03"}, {name: "Apr", value: "04"},
        {name: "May", value: "05"}, {name: "Jun", value: "06"},
        {name: "Jul", value: "07"}, {name: "Aug", value: "08"},
        {name: "Sep", value: "09"}, {name: "Oct", value: "10"},
        {name: "Nov", value: "11"}, {name: "Dec", value: "12"}
    	];
	}
	datemonth();

    $scope.dateday = ['01','02','03','04','05','06','07','08','09','10',
        '11','12','13','14','15','16','17','18','19','20',
        '21','22','23','24','25','26','27','28','29','30', '31'];

    var getDays = function(m, y) {
        if(m=='01' || m=='03' || m=='05' || m=='07' || m=='08' || m=='10' || m=='12') {
            $scope.dayOfMonth = 31;
        } else if (m=='02') {
            if(y % 4 === 0) { $scope.dayOfMonth = 29; }
            else { $scope.dayOfMonth = 28; }
        } else {
            $scope.dayOfMonth = 30;
        }
    }
    getDays(monthNow, yearNow);

    var getDaysTo = function(m, y) {
        if(m=='01' || m=='03' || m=='05' || m=='07' || m=='08' || m=='10' || m=='12') {
            $scope.dayOfMonthTo = 31;
        } else if (m=='02') {
            if(y % 4 === 0) { $scope.dayOfMonthTo = 29; }
            else { $scope.dayOfMonthTo = 28; }
        } else {
            $scope.dayOfMonthTo = 30;
        }
    }
    getDaysTo(monthNow, yearNow);

    $scope.selectedMonth = function(month, year) {
        getDays(month, year);
    }

    var uptoyear = yearNow - 2000; 
    var yearOptions = [];
    for(var x=0; x<=uptoyear; x++) {
        var year = 2000 + x;
        yearOptions.push(year); 
    }
    $scope.dateyear = yearOptions;

    $scope.selectedYear = function(month, year) {
        getDays(month, year);
    }

    $scope.selectedyearmonth = function(frommonth, fromyear, tomonth, toyear) {
        if(fromyear > toyear) {
            $scope.slice = frommonth-1;
            $scope.filter.fromyear = toyear;
        } else if (fromyear == toyear) {
            if(frommonth > tomonth || frommonth == tomonth) {
                $scope.filter.frommonth = tomonth;
            } else {
                $scope.slice = frommonth-1;
            }
        } else {
            $scope.slice = 0;
        }
    }

    // ==================== FOR DUAL DATE / COMPARING ====================
    var dualdatevalidationperday = function (monthfrom,yearfrom,dayfrom, monthto, yearto,dayto) {
        if(yearfrom > yearto) {
            $scope.filterperday.toyear = yearfrom;
            $scope.sliceperday = monthfrom-1;
            if(monthfrom > monthto) {
                $scope.filterperday.tomonth = monthfrom;
                if(dayfrom > dayto) {
                    $scope.filterperday.today = dayfrom;
                }
            }
        } else if ( yearfrom == yearto ) {
            $scope.sliceperday = monthfrom-1;
            if(monthfrom > monthto || monthfrom == monthto) {
                $scope.filterperday.tomonth = monthfrom;
                if(dayfrom > dayto) {
                    $scope.filterperday.today = dayfrom;
                }
            }
        } else {
            $scope.slice = 0;
        }
        getDays(monthfrom, yearfrom);
    }

    $scope.dualdatevalidationperday = function(monthfrom,yearfrom,dayfrom, monthto, yearto,dayto) {
        dualdatevalidationperday(monthfrom,yearfrom,dayfrom, monthto, yearto,dayto);
    }

    var dualdatevalidationpermonth = function (monthfrom,yearfrom, monthto,yearto) {
        if(yearfrom > yearto) {
            $scope.filterpermonth.toyear = yearfrom;
            $scope.slicepermonth = monthfrom-1;
            if(monthfrom > monthto) {
                $scope.filterpermonth.tomonth = monthfrom;
            }
        } else if ( yearfrom == yearto ) {
            $scope.slicepermonth = monthfrom-1;
            if(monthfrom > monthto || monthfrom == monthto) {
                $scope.filterpermonth.tomonth = monthfrom;
            }
        } else {
            $scope.slice = 0;
        }
    }

    $scope.dualdatevalidationpermonth = function(monthfrom,yearfrom, monthto,yearto) {
        dualdatevalidationpermonth(monthfrom,yearfrom, monthto,yearto);
    }

    //DATE PICKER
    $scope.today = function () {
    $scope.dt = new Date();
    };

    $scope.today();
    $scope.clear = function () {
    $scope.dt = null;
       
    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0]
})