app.controller("centerincomeCtrl", function ($scope, Config, CenterincomeFactory){
	$scope.filter = {
	};
    $scope.filterperday = {
    };
    $scope.filterpermonth = {
    };

// ======================= 1ON1 INTRO SESSION TAB ====================
    $scope.tab1true = function() {
        nofiltertab1();
        nofilter();
    }
	var nofiltertab1 = function() {
		CenterincomeFactory.oneonone(function (data){
			$scope.sessions = data.sessions;
			$scope.totalincome = data.totalincome;
			$scope.totalregistrant = data.totalregistrant;
            $scope.finalquantity = data.finalquantity;
			// $scope.count = data.sessions.length;
			var allcenters = {centerid:12345, centertitle:"All"};
			var allstates = {state_code:"ALL", state:"All"}
			data.centers.splice(0, 0, allcenters);
			data.states.splice(0, 0, allstates);
			$scope.centers = data.centers;
			$scope.states = data.states;
		});
	}
	nofiltertab1();

	$scope.filtertab1 = function(filter) {
		CenterincomeFactory.filteroneonone(filter, function (data) {
			$scope.sessions = data.sessions;
			$scope.totalincome = data.totalincome;
			$scope.totalregistrant = data.totalregistrant;
            $scope.finalquantity = data.finalquantity;
		});
	}

    var nofilter = function() {
        $scope.filter = {
            fromyear    : yearNow,
            toyear      : yearNow,
            frommonth   : monthNow,
            tomonth     : monthNow,
            center      : 12345,
            state       : "ALL",
        };
        $scope.slice = monthNow-1;
    }
    nofilter();

	$scope.nofiltertab1 = function() {
		nofiltertab1();
		nofilter();
	}
// ======================= GROUP INTRO SESSION TAB ====================
    $scope.tab2true = function() {
        nofiltertab2();
        nofilter();
    }
    var nofiltertab2 = function() {
        CenterincomeFactory.groupsession(function (data){
            $scope.group_sessions = data.sessions;
            $scope.group_totalincome = data.totalincome;
            $scope.group_totalregistrant = data.totalregistrant;
            $scope.group_finalquantity = data.finalquantity;
            // $scope.group_count = data.sessions.length;
            var allcenters = {centerid:12345, centertitle:"All"};
            var allstates = {state_code:"ALL", state:"All"}
            data.centers.splice(0, 0, allcenters);
            data.states.splice(0, 0, allstates);
            $scope.centers = data.centers;
            $scope.states = data.states;
        });
    }
    nofiltertab2();

    $scope.nofiltertab2 = function() {
        nofiltertab2();
        $scope.filter = {
            fromyear    : yearNow,
            toyear      : yearNow,
            frommonth   : monthNow,
            tomonth     : monthNow,
            center      : 12345,
            state       : "ALL",
        };
        $scope.slice = monthNow-1;
    }

    $scope.filtertab2 = function(filter) {
        CenterincomeFactory.filtergroupsession(filter, function (data) {
            $scope.group_sessions = data.sessions;
            $scope.group_totalincome = data.totalincome;
            $scope.group_totalregistrant = data.totalregistrant;
            $scope.group_finalquantity = data.finalquantity;
        });
    }

// ======================= 1ON1 INTRO SESSION TAB (franchise) ====================
    $scope.tab3true = function() {

        nofiltertab3();
        nofilter();
    }
    var nofiltertab3 = function() {
        CenterincomeFactory.oneononefranchise(function (data){
            $scope.one_f_sessions = data.sessions;
            $scope.one_f_totalincome = data.totalincome;
            $scope.one_f_totalregistrant = data.totalregistrant;
            $scope.one_f_finalquantity = data.finalquantity;
            // $scope.group_count = data.sessions.length;
            var allcenters = {centerid:12345, centertitle:"All"};
            var allstates = {state_code:"ALL", state:"All"}
            data.centers.splice(0, 0, allcenters);
            data.states.splice(0, 0, allstates);
            $scope.centers = data.centers;
            $scope.states = data.states;
        });
    }
    nofiltertab3();

    $scope.nofiltertab3 = function() {
        nofiltertab3();
        $scope.filter = {
            fromyear    : yearNow,
            toyear      : yearNow,
            frommonth   : monthNow,
            tomonth     : monthNow,
            center      : 12345,
            state       : "ALL",
        };
        $scope.slice = monthNow-1;
    }

    $scope.filtertab3 = function(filter) {
        CenterincomeFactory.filteroneononefranchise(filter, function (data) {
            $scope.one_f_sessions = data.sessions;
            $scope.one_f_totalincome = data.totalincome;
            $scope.one_f_totalregistrant = data.totalregistrant;
            $scope.one_f_finalquantity = data.finalquantity;
        });
    }

// ======================= GROUP + INTRO SESSION TAB (franchise) ====================
    $scope.tab4true = function() {
        nofiltertab4();
        nofilter();
    }
    var nofiltertab4 = function() {
        CenterincomeFactory.groupfranchise(function (data){
            $scope.group_f_sessions = data.sessions;
            $scope.group_f_totalincome = data.totalincome;
            $scope.group_f_totalregistrant = data.totalregistrant;
            $scope.group_f_finalquantity = data.finalquantity;
            // $scope.group_count = data.sessions.length;
            var allcenters = {centerid:12345, centertitle:"All"};
            var allstates = {state_code:"ALL", state:"All"}
            data.centers.splice(0, 0, allcenters);
            data.states.splice(0, 0, allstates);
            $scope.centers = data.centers;
            $scope.states = data.states;
        });
    }
    nofiltertab4();

    $scope.nofiltertab4 = function() {
        nofiltertab3();
        $scope.filter = {
            fromyear    : yearNow,
            toyear      : yearNow,
            frommonth   : monthNow,
            tomonth     : monthNow,
            center      : 12345,
            state       : "ALL",
        };
        $scope.slice = monthNow-1;
    }

    $scope.filtertab4 = function(filter) {
        CenterincomeFactory.filtergroupfranchise(filter, function (data) {
            $scope.group_f_sessions = data.sessions;
            $scope.group_f_totalincome = data.totalincome;
            $scope.group_f_totalregistrant = data.totalregistrant;
            $scope.group_f_finalquantity = data.finalquantity;
        });
    }
// ======================= CHART TAB ====================
    var chapterperday_default = function () {
        CenterincomeFactory.chartperday(function (data) { //CHART PER DAY FACTORY
            var days = []
            var one_series = [];
            var one_series_f = [];
            var group_series = [];
            var group_series_f = [];
            angular.forEach(data.days, function(valueX, keyX) { //OUTER
                days.push(valueX.categories);
                var one_pushed = false;
                var one_pushed_f = false;
                var group_pushed = false;
                var group_pushed_f = false;

                angular.forEach(data.oneonone, function(valueY, keyY) { //1ON1 INTERNAL
                    var oneonone_date = moment(valueY.datecreated).format("YYYY-MM-DD");
                    var one_registrant = parseInt(valueY.totalregistrant);
                    if(valueX.fulldate == oneonone_date) {
                        one_series.push(one_registrant);
                        one_pushed = true;
                    }
                });
                if(one_pushed != true) { one_series.push(0);
                } else { one_pushed = false; }

                angular.forEach(data.oneonone_f, function(valueY, keyY) { //1ON1 INTERNAL
                    var oneonone_date = moment(valueY.datecreated).format("YYYY-MM-DD");
                    var one_registrant = parseInt(valueY.totalregistrant);
                    if(valueX.fulldate == oneonone_date) {
                        one_series_f.push(one_registrant);
                        one_pushed_f = true;
                    }
                });
                if(one_pushed_f != true) { one_series_f.push(0);
                } else { one_pushed_f = false; }

                angular.forEach(data.groupclass, function(valueZ, keyZ) { //GROUP INTERNAL
                    var group_date = moment(valueZ.datecreated).format("YYYY-MM-DD");
                    var group_registrant = parseInt(valueZ.totalregistrant);
                    if(valueX.fulldate ==group_date ) {
                        group_series.push(group_registrant);
                        group_pushed = true;
                    }
                });
                if(group_pushed != true) { group_series.push(0);
                } else { group_pushed = false; }

                angular.forEach(data.groupclass_f, function(valueZ, keyZ) { //GROUP INTERNAL
                    var group_date = moment(valueZ.datecreated).format("YYYY-MM-DD");
                    var group_registrant = parseInt(valueZ.totalregistrant);
                    if(valueX.fulldate ==group_date ) {
                        group_series_f.push(group_registrant);
                        group_pushed_f = true;
                    }
                });
                if(group_pushed_f != true) { group_series_f.push(0);
                } else { group_pushed_f = false; }

            });

            $scope.chartConfig_perday = {
                options: {
                    chart: {
                        type: 'column'
                    }
                },
                series: [
                        {name: "1 on 1 Intro Session", color: "#7cb5ec", data: one_series},
                        {name: "Group Class +", color: "#d76a1a", data: group_series},
                        {name: "1 on 1 Intro Session (Franchise)", color: "#466289",  data: one_series_f},
                        {name: "Group Class + (Franchise)", color:"#FFB739", data: group_series_f},
                        ],
                title: {
                    text: 'Per Day'
                },
                xAxis: {
                    categories: days,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Quantity'
                    }
                },
                loading: false
            }
        })
    }
    chapterperday_default();
    $scope.nofilterchartperday = function() {
        chapterperday_default();
        var monthNow = moment().format('MM');
        var dayNow = moment().format('DD');
        var yearNow = new Date().getFullYear();

        var date = new Date();
        var date2 = new Date();
        var date3 = new Date();
        var day10daysbefore = moment(date.setDate(date.getDate() - 9)).format('DD');
        var month10daysbefore = moment(date2.setDate(date2.getDate() - 9)).format('MM');
        var year10daysbefore = parseInt(moment(date3.setDate(date3.getDate() - 9)).format('YYYY'));

        $scope.filterperday = {
            fromyear : year10daysbefore,
            frommonth : month10daysbefore,
            fromday : day10daysbefore,
            toyear : yearNow,
            tomonth : monthNow,
            today : dayNow,
        }
    }

    var chapterpermonth_default = function () {

        CenterincomeFactory.chartpermonth(function (data) { //CHART PER MONTH FACTORY

            var yearNow = new Date().getFullYear();
            var monthNow = moment().format('MM');

            $scope.slicepermonth = 'monthNow';

            $scope.filterpermonth.fromyear = parseInt(data.inityear);
            $scope.filterpermonth.frommonth = data.initmonth;
            $scope.filterpermonth.toyear = parseInt(yearNow);
            $scope.filterpermonth.tomonth = monthNow;

            var months = [];
            var one_series = [];
            var group_series = [];
            var one_series_f = [];
            var group_series_f = [];
            angular.forEach(data.months, function(valueX, keyX) { //OUTER
                months.push(valueX.categories);
                var yearmonth = moment(valueX.fulldate).format("YYYY-MM");
                var one_pushed = false;
                var group_pushed = false;
                var one_pushed_f = false;
                var group_pushed_f = false;

                angular.forEach(data.oneonone, function(valueY, keyY) { //1ON1 INTERNAL
                    var oneonone_date = moment(valueY.datecreated).format("YYYY-MM");
                    var one_registrant = parseInt(valueY.totalregistrant);
                    if(yearmonth == oneonone_date) {
                        one_series.push(one_registrant);
                        one_pushed = true;
                    }
                });
                if(one_pushed != true) { one_series.push(0);
                } else { one_pushed = false; }

                angular.forEach(data.oneonone_f, function(valueY_f, keyY_f) { //1ON1 INTERNAL franchise
                    var oneonone_date = moment(valueY_f.datecreated).format("YYYY-MM");
                    var one_registrant = parseInt(valueY_f.totalregistrant);
                    if(yearmonth == oneonone_date) {
                        one_series_f.push(one_registrant);
                        one_pushed_f = true;
                    }
                });
                if(one_pushed_f != true) { one_series_f.push(0);
                } else { one_pushed_f = false; }

                angular.forEach(data.groupclass, function(valueZ, keyZ) { //GROUP INTERNAL
                    var group_date = moment(valueZ.datecreated).format("YYYY-MM");
                    var group_registrant = parseInt(valueZ.totalregistrant);
                    if(yearmonth ==group_date ) {
                        group_series.push(group_registrant);
                        group_pushed = true;
                    }
                });
                if(group_pushed != true) { group_series.push(0);
                } else { group_pushed = false; }

                angular.forEach(data.groupclass_f, function(valueZ_f, keyZ_f) { //GROUP INTERNAL franchise
                    var group_date = moment(valueZ_f.datecreated).format("YYYY-MM");
                    var group_registrant = parseInt(valueZ_f.totalregistrant);
                    if(yearmonth == group_date ) {
                        group_series_f.push(group_registrant);
                        group_pushed_f = true;
                    }
                });
                if(group_pushed_f != true) { group_series_f.push(0);
                } else { group_pushed_f = false; }

            });

            $scope.chartConfig_permonth = {
                options: {
                    chart: {
                        type: 'column'
                    }
                },
                series: [
                        {name: "1 on 1 Intro Session", color: "#7cb5ec", data: one_series},
                        {name: "Group Class +", color: "#d76a1a", data: group_series},
                        {name: "1 on 1 Intro Session (Franchise)", color: "#466289",  data: one_series_f},
                        {name: "Group Class + (Franchise)", color:"#FFB739", data: group_series_f}
                        ],
                title: {
                    text: 'Per Month'
                },
                xAxis: {
                    categories: months,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Quantity'
                    }
                },
                loading: false
            }
        });
    }
    chapterpermonth_default();
    $scope.nofilterchartpermonth = function() {
        chapterpermonth_default();
    }

    $scope.filterchartperday = function (filter) {
        CenterincomeFactory.filterchartperday(filter, function (data) {
            var days = []
            var one_series = [];
            var group_series = [];
            var one_series_f = [];
            var group_series_f = [];
            angular.forEach(data.days, function(valueX, keyX) { //OUTER
                days.push(valueX.categories);
                var one_pushed = false;
                var group_pushed = false;
                var one_pushed_f = false;
                var group_pushed_f = false;

                angular.forEach(data.oneonone, function(valueY, keyY) { //1ON1 INTERNAL
                    var oneonone_date = moment(valueY.datecreated).format("YYYY-MM-DD");
                    var one_registrant = parseInt(valueY.totalregistrant);
                    if(valueX.fulldate == oneonone_date) {
                        one_series.push(one_registrant);
                        one_pushed = true;
                    }
                });
                if(one_pushed != true) { one_series.push(0);
                } else { one_pushed = false; }

                angular.forEach(data.oneonone_f, function(valueY, keyY) { //1ON1 INTERNAL
                    var oneonone_date = moment(valueY.datecreated).format("YYYY-MM-DD");
                    var one_registrant = parseInt(valueY.totalregistrant);
                    if(valueX.fulldate == oneonone_date) {
                        one_series_f.push(one_registrant);
                        one_pushed_f = true;
                    }
                });
                if(one_pushed_f != true) { one_series_f.push(0);
                } else { one_pushed_f = false; }

                angular.forEach(data.groupclass, function(valueZ, keyZ) { //GROUP INTERNAL
                    var group_date = moment(valueZ.datecreated).format("YYYY-MM-DD");
                    var group_registrant = parseInt(valueZ.totalregistrant);
                    if(valueX.fulldate ==group_date ) {
                        group_series.push(group_registrant);
                        group_pushed = true;
                    }
                });
                if(group_pushed != true) { group_series.push(0);
                } else { group_pushed = false; }

                angular.forEach(data.groupclass_f, function(valueZ, keyZ) { //GROUP INTERNAL
                    var group_date = moment(valueZ.datecreated).format("YYYY-MM-DD");
                    var group_registrant = parseInt(valueZ.totalregistrant);
                    if(valueX.fulldate ==group_date ) {
                        group_series_f.push(group_registrant);
                        group_pushed_f = true;
                    }
                });
                if(group_pushed_f != true) { group_series_f.push(0);
                } else { group_pushed_f = false; }

            });

            $scope.chartConfig_perday = {
                options: {
                    chart: {
                        type: 'column'
                    }
                },
                series: [
                        {name: "1 on 1 Intro Session", color: "#7cb5ec", data: one_series},
                        {name: "Group Class +", color: "#d76a1a", data: group_series},
                        {name: "1 on 1 Intro Session (Franchise)", color: "#466289",  data: one_series_f},
                        {name: "Group Class + (Franchise)", color:"#FFB739", data: group_series_f}
                        ],
                title: {
                    text: 'Per Day'
                },
                xAxis: {
                    categories: days,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Quantity'
                    }
                },
                loading: false
            }
        });
    }

    $scope.filterchartpermonth = function (filter) {
        CenterincomeFactory.filterchartpermonth(filter, function (data) {
            var months = []
            var one_series = [];
            var group_series = [];
            var one_series_f = [];
            var group_series_f = [];
            angular.forEach(data.months, function(valueX, keyX) { //OUTER
                months.push(valueX.categories);
                var one_pushed = false;
                var group_pushed = false;
                var one_pushed_f = false;
                var group_pushed_f = false;

                angular.forEach(data.oneonone, function(valueY, keyY) { //1ON1 INTERNAL
                    var oneonone_date = moment(valueY.datecreated).format("YYYY-MM");
                    var one_registrant = parseInt(valueY.totalregistrant);
                    if(valueX.fulldate == oneonone_date) {
                        one_series.push(one_registrant);
                        one_pushed = true;
                    }
                });
                if(one_pushed != true) { one_series.push(0);
                } else { one_pushed = false; }

                angular.forEach(data.oneonone_f, function(valueY, keyY) { //1ON1 INTERNAL
                    var oneonone_date = moment(valueY.datecreated).format("YYYY-MM");
                    var one_registrant = parseInt(valueY.totalregistrant);
                    if(valueX.fulldate == oneonone_date) {
                        one_series_f.push(one_registrant);
                        one_pushed_f = true;
                    }
                });
                if(one_pushed_f != true) { one_series_f.push(0);
                } else { one_pushed_f = false; }

                angular.forEach(data.groupclass, function(valueZ, keyZ) { //GROUP INTERNAL
                    var group_date = moment(valueZ.datecreated).format("YYYY-MM");
                    var group_registrant = parseInt(valueZ.totalregistrant);
                    if(valueX.fulldate ==group_date ) {
                        group_series.push(group_registrant);
                        group_pushed = true;
                    }
                });
                if(group_pushed != true) { group_series.push(0);
                } else { group_pushed = false; }

                angular.forEach(data.groupclass_f, function(valueZ, keyZ) { //GROUP INTERNAL
                    var group_date = moment(valueZ.datecreated).format("YYYY-MM");
                    var group_registrant = parseInt(valueZ.totalregistrant);
                    if(valueX.fulldate ==group_date ) {
                        group_series_f.push(group_registrant);
                        group_pushed_f = true;
                    }
                });
                if(group_pushed_f != true) { group_series_f.push(0);
                } else { group_pushed_f = false; }

            });


            $scope.chartConfig_permonth = {
                options: {
                    chart: {
                        type: 'column'
                    }
                },
                series: [
                        {name: "1 on 1 Intro Session", color: "#7cb5ec", data: one_series},
                        {name: "Group Class +", color: "#d76a1a", data: group_series},
                        {name: "1 on 1 Intro Session (Franchise)", color: "#466289",  data: one_series_f},
                        {name: "Group Class + (Franchise)", color:"#FFB739", data: group_series_f}
                        ],
                title: {
                    text: 'Per Month'
                },
                xAxis: {
                    categories: months,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Quantity'
                    }
                },
                loading: false
            }
        });
    }


// =================================================================================== //
	//custom DATESAVER [..] prerequisite (MOMENT)
    var monthNow = moment().format('MM');
    var dayNow = moment().format('DD');
    var yearNow = new Date().getFullYear();

    $scope.yearNow = yearNow;
    $scope.monthNow = monthNow;
    $scope.dayNow = dayNow;

    var date = new Date();
    var date2 = new Date();
    var date3 = new Date();
    $scope.day10daysbefore = moment(date.setDate(date.getDate() - 9)).format('DD');
    $scope.month10daysbefore = moment(date2.setDate(date2.getDate() - 9)).format('MM');
    $scope.year10daysbefore = parseInt(moment(date3.setDate(date3.getDate() - 9)).format('YYYY'));

    $scope.slice = monthNow-1;
    $scope.dayslice = dayNow-1;
    $scope.initmonthto = monthNow;

    var datemonth = function() {
    	$scope.datemonth = [
        {name: "Jan", value: "01"}, {name: "Feb", value: "02"},
        {name: "Mar", value: "03"}, {name: "Apr", value: "04"},
        {name: "May", value: "05"}, {name: "Jun", value: "06"},
        {name: "Jul", value: "07"}, {name: "Aug", value: "08"},
        {name: "Sep", value: "09"}, {name: "Oct", value: "10"},
        {name: "Nov", value: "11"}, {name: "Dec", value: "12"}
    	];
	}
	datemonth();

    $scope.dateday = ['01','02','03','04','05','06','07','08','09','10',
        '11','12','13','14','15','16','17','18','19','20',
        '21','22','23','24','25','26','27','28','29','30', '31'];

    var getDays = function(m, y) {
        if(m=='01' || m=='03' || m=='05' || m=='07' || m=='08' || m=='10' || m=='12') {
            $scope.dayOfMonth = 31;
        } else if (m=='02') {
            if(y % 4 === 0) { $scope.dayOfMonth = 29; }
            else { $scope.dayOfMonth = 28; }
        } else {
            $scope.dayOfMonth = 30;
        }
    }
    getDays(monthNow, yearNow);

    var getDaysTo = function(m, y) {
        if(m=='01' || m=='03' || m=='05' || m=='07' || m=='08' || m=='10' || m=='12') {
            $scope.dayOfMonthTo = 31;
        } else if (m=='02') {
            if(y % 4 === 0) { $scope.dayOfMonthTo = 29; }
            else { $scope.dayOfMonthTo = 28; }
        } else {
            $scope.dayOfMonthTo = 30;
        }
    }
    getDaysTo(monthNow, yearNow);

    $scope.selectedMonth = function(month, year) {
        getDays(month, year);
    }

    var uptoyear = yearNow - 2000;
    var yearOptions = [];
    for(var x=0; x<=uptoyear; x++) {
        var year = 2000 + x;
        yearOptions.push(year);
    }
    $scope.dateyear = yearOptions;

    $scope.selectedYear = function(month, year) {
        getDays(month, year);
    }

    $scope.selectedyearmonth = function(frommonth, fromyear, tomonth, toyear) {
        if(fromyear > toyear) {
            $scope.slice = frommonth-1;
            $scope.filter.fromyear = toyear;
        } else if (fromyear == toyear) {
            if(frommonth > tomonth || frommonth == tomonth) {
                $scope.filter.frommonth = tomonth;
            } else {
                $scope.slice = frommonth-1;
            }
        } else {
            $scope.slice = 0;
        }
    }

    // ==================== FOR DUAL DATE / COMPARING ====================
    var dualdatevalidationperday = function (monthfrom,yearfrom,dayfrom, monthto, yearto,dayto) {
        if(yearfrom > yearto) {
            $scope.filterperday.toyear = yearfrom;
            $scope.sliceperday = monthfrom-1;
            if(monthfrom > monthto) {
                $scope.filterperday.tomonth = monthfrom;
                if(dayfrom > dayto) {
                    $scope.filterperday.today = dayfrom;
                }
            }
        } else if ( yearfrom == yearto ) {
            $scope.sliceperday = monthfrom-1;
            if(monthfrom > monthto || monthfrom == monthto) {
                $scope.filterperday.tomonth = monthfrom;
                if(dayfrom > dayto) {
                    $scope.filterperday.today = dayfrom;
                }
            }
        } else {
            $scope.slice = 0;
        }
        getDays(monthfrom, yearfrom);
    }

    $scope.dualdatevalidationperday = function(monthfrom,yearfrom,dayfrom, monthto, yearto,dayto) {
        dualdatevalidationperday(monthfrom,yearfrom,dayfrom, monthto, yearto,dayto);
    }

    var dualdatevalidationpermonth = function (monthfrom,yearfrom, monthto,yearto) {
        if(yearfrom > yearto) {
            $scope.filterpermonth.toyear = yearfrom;
            $scope.slicepermonth = monthfrom-1;
            if(monthfrom > monthto) {
                $scope.filterpermonth.tomonth = monthfrom;
            }
        } else if ( yearfrom == yearto ) {
            $scope.slicepermonth = monthfrom-1;
            if(monthfrom > monthto || monthfrom == monthto) {
                $scope.filterpermonth.tomonth = monthfrom;
            }
        } else {
            $scope.slice = 0;
        }
    }

    $scope.dualdatevalidationpermonth = function(monthfrom,yearfrom, monthto,yearto) {
        dualdatevalidationpermonth(monthfrom,yearfrom, monthto,yearto);
    }

    //DATE PICKER
    $scope.today = function () {
    $scope.dt = new Date();
    };

    $scope.today();
    $scope.clear = function () {
    $scope.dt = null;

    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0]
})
