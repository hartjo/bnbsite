'use strict';

app.controller('Managenewsletter', function($scope,$http, $modal, $state, $stateParams, Config ,Managenewsletter){

	console.log("JS SUCCESSFULY LOADED")


	$scope.alerts = [];

	$scope.closeAlert = function (index) {
		$scope.alerts.splice(index, 1);
	};

	   // FACTORIZED PAGINATION
	   var num = 10;
	   var off = 1;
	   var keyword = null;
	   var loadlist = function(off,keyword){
	   	Managenewsletter.loadlist(num,off,keyword,function(data){
	   		$scope.data = data;
	   		$scope.maxSize = 5;
	   		$scope.bigTotalItems = data.total_items;
	   		$scope.bigCurrentPage = data.index;
	   	});
	   }
	   loadlist(off,keyword);


	   $scope.search = function (searchkeyword) {
	   	loadlist('1', searchkeyword);
	   	keyword = searchkeyword;
	   }

	   $scope.resetsearch = function() {
	   	var off = 1;
	   	loadlist(off,null);
	   	$scope.searchtext="";
	   }

	   $scope.numpages = function (off, keyword) {
	   	var off = 1;
	   	loadlist(off, keyword);
	   }

	   $scope.setPage = function (page) {
	   	loadlist(page, keyword);
	   };

 // ;FACTORIZED PAGINATION

 var successloadalert = function(){
 	$scope.alerts.splice(0, 1);
 	$scope.alerts.push({ type: 'success', msg: 'Newsletter successfully Deleted!' });

 }
 	var loadpage = function(){
	   	Managenewsletter.loadlist(num,$scope.bigCurrentPage, $scope.searchtext,function(data){
	   		$scope.data = data;
	   		$scope.maxSize = 5;
	   		$scope.bigTotalItems = data.total_items;
	   		$scope.bigCurrentPage = data.index;
	   	});
	   }
 	// DELETE
 	$scope.deletenews = function(id) {
 		var modalInstance = $modal.open({
 			templateUrl: 'deletenewsletter.html',
 			controller: deletenewsCTRL,
 			resolve: {
 				id: function() {
 					return id
 				}
 			}
 		});
 	}
 	var deletenewsCTRL = function($scope, $modalInstance, id) {
 		$scope.ok = function() {
 			Managenewsletter.deletenews(id, function(data){
 				$modalInstance.dismiss('cancel');
 				successloadalert();
 				loadpage();
 			});
 		};
 		$scope.cancel = function() {
 			$modalInstance.dismiss('cancel');
 		};
 	};
	// ;DELETE


	// DELETE PDF
 	$scope.deletepdf = function(id) {
 		var modalInstance = $modal.open({
 			templateUrl: 'deletenewsletter.html',
 			controller: deletepdfCTRL,
 			resolve: {
 				id: function() {
 					return id
 				}
 			}
 		});
 	}
 	var deletepdfCTRL = function($scope, $modalInstance, id) {
 		$scope.ok = function() {
 			Managenewsletter.deletenewspdf(id, function(data){
 				$modalInstance.dismiss('cancel');
 				successloadalert();
 				loadpage();
 			});
 		};
 		$scope.cancel = function() {
 			$modalInstance.dismiss('cancel');
 		};
 	};
	// ;DELETE PDF

	$scope.setstatus = function(status,id){
		$http({
			url:  Config.ApiURL+"/newsletter/changestatus/"+ status + '/' + id,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			loadlist($scope.bigCurrentPage, keyword);
		})
	}

	$scope.setstatus1 = function(status,id){
		$http({
			url:  Config.ApiURL+"/newsletterpdf/changestatus/"+ status + '/' + id,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			loadlist($scope.bigCurrentPage, keyword);
		})
	}

	var newsEditpdfCTRL = function($scope, $modalInstance, id, $state) {
		$scope.id = id;
        $scope.ok = function(ids) {
            $scope.id = id;
            $state.go('editnewspdf', {id: ids });
                $modalInstance.dismiss('cancel');
            };
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }

    $scope.editnewspdf = function(id) {
        var modalInstance = $modal.open({
            templateUrl: 'newsletterEdit.html',
            controller: newsEditpdfCTRL,
            resolve: {
                id: function() {
                    return id
                }
            }
        });
    }

    var newsletterEditCTRL = function($scope, $modalInstance, id, $state) {
    	$scope.id = id;
        $scope.ok = function(ids) {
            $state.go('editnewsletter', {id: ids });
                $modalInstance.dismiss('cancel');     
            };
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }

    $scope.editnewsletter = function(id) {
        var modalInstance = $modal.open({
            templateUrl: 'newsletterEdit.html',
            controller: newsletterEditCTRL,
            resolve: {
                id: function() {
                    return id
                }
            }
        });
    }


});