app.factory("CenterincomeFactory", function ($http, Config) {
	return {
		oneonone: function(callback) {
			$http({
				url: Config.ApiURL + "/centerincome/oneonone",
				method: "GET"
			}).success(function (data, status, headers, config){
				callback(data);
			}).error(function (data, status, headers, config){
				callback(data);
			})
		},
		filteroneonone: function(filter, callback) {
			$http({
				url: Config.ApiURL + "/centerincome/filteroneonone",
				method: "POST",
				headers: { "Content-Type" : "application/x-www-form-urlencoded" },
				data: $.param(filter)
			}).success(function (data, status, headers, config){
				callback(data);
			}).error(function (data, status, headers, config){
				callback(data);
			})
		},
		groupsession: function(callback) {
			$http({
				url: Config.ApiURL + "/centerincome/groupsession",
				method: "GET"
			}).success(function (data, status, headers, config){
				callback(data);
			}).error(function (data, status, headers, config){
				callback(data);
			})
		},
		filtergroupsession: function(filter, callback) {
			$http({
				url: Config.ApiURL + "/centerincome/filtergroupsession",
				method: "POST",
				headers: { "Content-Type" : "application/x-www-form-urlencoded" },
				data: $.param(filter)
			}).success(function (data, status, headers, config){
				callback(data);
			}).error(function (data, status, headers, config){
				callback(data);
			})
		},
		chartperday: function(callback) {
			$http({
				url: Config.ApiURL + "/centerincome/chartperday",
				method: "GET"
			}).success(function (data, status, headers, config){
				callback(data);
			}).error(function (data, status, headers, config){
				callback(data);
			})
		},
		chartpermonth: function(callback) {
			$http({
				url: Config.ApiURL + "/centerincome/chartpermonth",
				method: "GET"
			}).success(function (data, status, headers, config){
				callback(data);
			}).error(function (data, status, headers, config){
				callback(data);
			})
		},
		filterchartperday: function(filter, callback) {
			$http({
				url: Config.ApiURL + "/centerincome/filterchartperday",
				method: "POST",
				headers: { "Content-Type" : "application/x-www-form-urlencoded" },
				data: $.param(filter)
			}).success(function (data, status, headers, config){
				callback(data);
			}).error(function (data, status, headers, config){
				callback(data);
			})
		},
		filterchartpermonth: function(filter, callback) {
			$http({
				url: Config.ApiURL + "/centerincome/filterchartpermonth",
				method: "POST",
				headers: { "Content-Type" : "application/x-www-form-urlencoded" },
				data: $.param(filter)
			}).success(function (data, status, headers, config){
				callback(data);
			}).error(function (data, status, headers, config){
				callback(data);
			})
		},
		oneononefranchise: function(callback) {
			$http({
				url: Config.ApiURL + "/centerincome/oneononefranchise",
				method: "GET"
			}).success(function (data, status, headers, config){
				callback(data);
			}).error(function (data, status, headers, config){
				callback(data);
			})
		},
		filteroneononefranchise: function(filter, callback) {
			$http({
				url: Config.ApiURL + "/centerincome/filteroneononefranchise",
				method: "POST",
				headers: { "Content-Type" : "application/x-www-form-urlencoded" },
				data: $.param(filter)
			}).success(function (data, status, headers, config){
				callback(data);
			}).error(function (data, status, headers, config){
				callback(data);
			})
		},
		groupfranchise: function(callback) {
			$http({
				url: Config.ApiURL + "/centerincome/groupfranchise",
				method: "GET"
			}).success(function (data, status, headers, config){
				callback(data);
			}).error(function (data, status, headers, config){
				callback(data);
			})
		},
		filtergroupfranchise: function(filter, callback) {
			$http({
				url: Config.ApiURL + "/centerincome/filtergroupfranchise",
				method: "POST",
				headers: { "Content-Type" : "application/x-www-form-urlencoded" },
				data: $.param(filter)
			}).success(function (data, status, headers, config){
				callback(data);
			}).error(function (data, status, headers, config){
				callback(data);
			})
		},
		allfranchise: function(callback) {
			$http({
				url: Config.ApiURL + "/centerincome/allfranchise",
				method: "GET",
				headers: { "Content-Type" : "application/x-www-form-urlencoded" }
			}).success(function (data, status, headers, config){
				callback(data);
			}).error(function (data, status, headers, config){
				callback(data);
			})
		},
		filterallfranchise: function(filter, callback) {
			$http({
				url: Config.ApiURL + "/centerincome/filterallfranchise",
				method: "POST",
				headers: { "Content-Type" : "application/x-www-form-urlencoded" },
				data: $.param(filter)
			}).success(function (data, status, headers, config){
				callback(data);
			}).error(function (data, status, headers, config){
				callback(data);
			})
		}
		

	}
})