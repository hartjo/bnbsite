app.factory('manageauditlogFactory', function($http, $q, Config){
    return {
        auditloglist: function(num, off, keyword, callback){
            $http({
                url: Config.ApiURL +"/be/auditlogs/auditloglist/" + num + '/' + off + '/' + keyword,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        }

    }
})