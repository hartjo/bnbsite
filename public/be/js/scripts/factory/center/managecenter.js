app.factory('ManageCenterFactory', function($http, $q, Config){
  	return {
    	 data: {},
    	 centerlist: function(num, off, keyword ,userid, callback){
	    	 	$http({
	    	 		url: Config.ApiURL +"/center/managecenter/" + num + '/' + off + '/' + keyword + '/' + userid,
	    	 		method: "GET",
	    	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	    	 	}).success(function (data, status, headers, config) {
	    	 		callback(data);
	    	 		pagetotalitem = data.total_items;
	    	 		currentPage = data.index;
	    	 	}).error(function (data, status, headers, config) {
	    	 		callback(data);
	    	 	});
    	 },

    	 dropdowncenterlist: function(num, off, keyword ,userid, callback){
	    	 	$http({
	    	 		url: Config.ApiURL +"/center/managecenterdropdown/" + num + '/' + off + '/' + keyword + '/' + userid,
	    	 		method: "GET",
	    	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	    	 	}).success(function (data, status, headers, config) {
	    	 		callback(data);
	    	 		pagetotalitem = data.total_items;
	    	 		currentPage = data.index;
	    	 	}).error(function (data, status, headers, config) {
	    	 		callback(data);
	    	 	});
    	 }

    }
   
})