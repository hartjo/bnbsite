app.factory('managenewsFactory', function($http, $q, Config){
  	return {
    		newslist: function(num, off, keyword, centerid , callback){
	    	 	$http({
	    	 		url: Config.ApiURL +"/center/news/newslist/" + num + '/' + off + '/' + keyword + '/' + centerid,
	    	 		method: "GET",
	    	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	    	 	}).success(function (data, status, headers, config) {
	    	 		callback(data);
	    	 	}).error(function (data, status, headers, config) {
	    	 		callback(data);
	    	 	});
    	 	}

    }
   
})