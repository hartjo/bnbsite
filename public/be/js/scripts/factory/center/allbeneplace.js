app.factory('allbeneplaceFactory', function($http, $q, Config){
    return {
        loadclasslist: function(num, off, keyword , callback){
            $http({
                url: Config.ApiURL +"/be/allbeneplace/allbeneplacelist/" + num + '/' + off + '/' + keyword,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        loadgroupsession: function(sessionid , userid, callback){
            $http({
                url: Config.ApiURL +"/be/beneplace/loadbeneplacesession/"+ sessionid+"/"+userid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        viewreferrallink: function(callback){
            $http({
                url: Config.ApiURL +"/be/beneplace/viewreferrallink",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        savereferrallink: function(ref,callback){
            $http({
                url: Config.ApiURL +"/be/beneplace/savereferral",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(ref)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        }
       

    }  
})