app.factory('centerViewFactory', function($http, $q, Config){
    return {
         loadcenter: function(centerid,callback){
            $http({
                url: Config.ApiURL + "/center/centerview/loadcenter/"+ centerid,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
         },

        saveimagesforslider: function(fileout, callback) {
            $http({
                url: Config.ApiURL + "/center/saveimagesforslider",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param(fileout)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        loadCenterImages: function(centerid, callback) {
            $http({
                url: Config.ApiURL + "/be/center/loadcenterimages/" + centerid,
                method: "GET",
                headers: {'Content-Type' : 'application/x-www-form-urlencoded' }
            }).success(function (data, status, headers, config) {
                callback(data);

            })
        },

        saveordering: function(thispost, callback) {
            $http({
                url: Config.ApiURL + "/center/saveordering",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param(thispost)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
    }



   
})