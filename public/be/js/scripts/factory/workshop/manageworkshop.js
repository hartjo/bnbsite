app.factory('ManageWorkshopFactory', function($http, Config){
	return {
		createvenue: function(venue,callback) {
			$http({
				url: Config.ApiURL + "/BE/workshop/createvenue",
				method: "POST",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
				data: $.param(venue)
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
			}).error(function (data, status, headers, config) {
				data = data;
				callback(data);
			});
		},
		listofvenues: function(num, off, keyword, callback) {
			$http({
				url: Config.ApiURL + "/BE/workshop/listofvenues/"+num+"/"+off+"/"+keyword,
				method: "GET",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
			}).error(function (data, status, headers, config) {
				data = data;
				callback(data);
			});
		},
		checkvenuename: function(venuename, callback) {
			$http({
				url: Config.ApiURL + "/BE/workshop/checkvenuename/"+venuename,
				method: "GET",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
			}).error(function (data, status, headers, config) {
				data = data;
				callback(data);
			});
		},
		deleteworkshopvenue: function(workshopvenueid, callback) {
			$http({
				url: Config.ApiURL + "/BE/workshop/deleteworkshopvenue/"+workshopvenueid,
				method: "GET",
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
			}).error(function (data, status, headers, config) {
				data = data;
				callback(data);
			});
		},
		editworkshopvenue: function(workshopvenueid, callback) {
			$http({
				url: Config.ApiURL + "/BE/workshop/editworkshopvenue/"+workshopvenueid,
				method: "GET",
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
			}).error(function (data, status, headers, config) {
				data = data;
				callback(data);
			});
		},
		checkvenuenameWException: function(venuename, workshopvenueid, callback) {
			$http({
				url: Config.ApiURL + "/BE/workshop/checkvenuenamewexception/"+venuename+"/"+workshopvenueid,
				method: "GET",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
			}).error(function (data, status, headers, config) {
				data = data;
				callback(data);
			});
		},
		updatevenue: function(venue, workshopvenueid, callback) {
			$http({
				url: Config.ApiURL + "/BE/workshop/updatevenue/"+workshopvenueid,
				method: "POST",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
				data: $.param(venue)
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
			}).error(function (data, status, headers, config) {
				data = data;
				callback(data);
			});
		},
		createtitle: function(title, callback) {

			var titleslugs = title.replace(/[^\w ]+/g,'');
            titleslugs = angular.lowercase(titleslugs.replace(/ +/g,'-'));

            var thisPost = { 'title': title, 'titleslugs': titleslugs };

			$http({
				url: Config.ApiURL + "/BE/workshop/createtitle",
				method: "POST",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
				data: $.param(thisPost)
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
			}).error(function (data, status, headers, config) {
				data = data;
				callback(data);
			});
		},
		titlelist: function(num, off, keyword, callback)	{
			$http({
				url: Config.ApiURL + "/BE/workshop/listoftitles/"+num+"/"+off+"/"+keyword,
				method: "GET",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
			}).error(function (data, status, headers, config) {
				data = data;
				callback(data);
			});
		},
		removetitle: function(workshoptitleid, callback) {
			$http({
				url: Config.ApiURL + "/BE/workshop/removetitle/"+workshoptitleid,
				method: "GET",
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
			}).error(function (data, status, headers, config) {
				data = data;
				callback(data);
			});
		},
		checktitlename: function(titlename, callback) {

			var titleslugs = titlename.replace(/[^\w ]+/g,'');
            titleslugs = angular.lowercase(titleslugs.replace(/ +/g,'-'));
            // var thisPost = { 'title': titlename, 'titleslugs': titleslugs };

			$http({
				url: Config.ApiURL + "/BE/workshop/checktitlename/"+titleslugs,
				method: "GET",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
			}).error(function (data, status, headers, config) {
				data = data;
				callback(data);
			});
		},
		workshoplists: function(callback) {
			$http({
				url: Config.ApiURL + "/BE/workshop/lists",
				method: "GET",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
			}).error(function (data, status, headers, config) {
				data = data;
				callback(data);
			});
		},
		createworkshop: function(workshop, callback) {
			$http({
				url: Config.ApiURL + "/BE/workshop/createworkshop",
				method: "POST",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
				data: $.param(workshop)
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
			}).error(function (data, status, headers, config) {
				data = data;
				callback(data);
			});
		},
		workshoplist: function(num, off, keyword, callback)	{
			$http({
				url: Config.ApiURL + "/BE/workshop/listofworkshop/"+num+"/"+off+"/"+keyword,
				method: "GET",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
			}).error(function (data, status, headers, config) {
				data = data;
				callback(data);
			});
		},
		removeworkshop: function(workshopid, callback) {
			$http({
				url: Config.ApiURL + "/BE/workshop/removeworkshop/"+workshopid,
				method: "GET",
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
			}).error(function (data, status, headers, config) {
				data = data;
				callback(data);
			});
		},
		editworkshop: function(workshopid, callback) {
			$http({
				url: Config.ApiURL + "/BE/workshop/edit/"+workshopid,
				method: "GET",
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
			}).error(function (data, status, headers, config) {
				data = data;
				callback(data);
			});
		},
		updateworkshop: function(upworkshop, workshopid, callback) {
			$http({
				url: Config.ApiURL + "/BE/workshop/update/"+workshopid,
				method: "POST",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
				data: $.param(upworkshop)
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
			}).error(function (data, status, headers, config) {
				data = data;
				callback(data);
			});
		},
		updatetitle: function(titleid, title, callback) {
			var titleslugs = title.replace(/[^\w ]+/g,'');
            titleslugs = angular.lowercase(titleslugs.replace(/ +/g,'-'));

			var thisPost = { 'titleid': titleid, 'title': title, 'titleslugs': titleslugs };

			$http({
				url: Config.ApiURL + "/BE/workshop/updatetitle",
				method: "POST",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
				data: $.param(thisPost)
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
			}).error(function (data, status, headers, config) {
				data = data;
				callback(data);
			});
		},
		registrantslist: function(num, off, filter, callback)	{
			$http({
				url: Config.ApiURL + "/workshop/registrantslist/"+num+"/"+off,
				method: "POST",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
				data: $.param(filter)
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
			}).error(function (data, status, headers, config) {
				data = data;
				callback(data);
			});
		},
		removeregistrant: function(registrantid, callback)	{
			$http({
				url: Config.ApiURL + "/workshop/removeregistrant/"+registrantid,
				method: "GET",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
			}).error(function (data, status, headers, config) {
				data = data;
				callback(data);
			});
		},
		related: function(callback) {
			$http({
				url: Config.ApiURL + "/workshop/related",
				method: "GET",
			}).success(function (data, status, headers, config) {
				data = data;
				callback(data);
			}).error(function (data, status, headers, config) {
				data = data;
				callback(data);
			});
		},
		loadcenter: function(callback){
            $http({
                url: Config.ApiURL + "/news/listcenter",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
         },
         loadstate: function(callback){
            $http({
                url: Config.ApiURL + "/center/statelist",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
         },
         loadcity: function(statecode,callback){
            $http({
                url: Config.ApiURL + "/center/citylist/" + statecode,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
         },

         loadzip: function(cityname,callback){
            $http({
                url: Config.ApiURL + "/center/ziplist/" + cityname,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
         },
	}
})