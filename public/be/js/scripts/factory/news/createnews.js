app.factory('Createnews', function($http, $q, Config){
  	return {

  		data: {},
    	loadcategory: function(callback){
    	 	$http({
    	 		url: Config.ApiURL + "/news/listcategory",
    	 		method: "GET",
    	 		headers: {
    	 			'Content-Type': 'application/x-www-form-urlencoded'
    	 		}
    	 	}).success(function (data, status, headers, config) {
    	 		data = data;
    	 		callback(data);
    	 	}).error(function (data, status, headers, config) {
    	 		data = data;
    	 		callback(data);
    	 	});
    	 },
         loadcenter: function(callback){
            $http({
                url: Config.ApiURL + "/news/listcenter",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
         },
         workshoptitles: function(callback) {
            $http({
                url: Config.ApiURL + "/createnews/workshoptitles",
                method: "GET",
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
         }

    }
   
})