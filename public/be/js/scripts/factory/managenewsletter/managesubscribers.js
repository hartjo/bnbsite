app.factory('Managesubscribers', function($http, $q, Config){
  	return {
        data: {},
    	loadlist:  function(num, off, keyword, callback){
    	 	$http({
    	 		url: Config.ApiURL + "/subscriber/list/"+ num + '/' + off + '/' + keyword,
    	 		method: "GET",
    	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    	 	}).success(function (data, status, headers, config) {
    	 		data = data;
    	 		callback(data);
                pagetotalitem = data.total_items;
                currentPage = data.index;
    	 	}).error(function (data, status, headers, config) {
    	 		data = data;
    	 		callback(data);
    	 	});
    	 },
         addsubscriber: function(subscriber,callback) {
            $http({
                url: Config.ApiURL + "/subscriber/add",
                method: "POST",
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param(subscriber)
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
        },
        checkemail: function(email, callback) {
            $http({
                url: Config.ApiURL + "/subscriber/emailcheck/"+email,
                method: "GET",
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
        },
        deletesubscriber: function(id,callback){
         $http({
                url: Config.ApiURL + "/subscriber/delete/"+id,
                method: "GET",
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
                callback(data);
            });
        }
    }
   
})