<script type="text/ng-template" id="success.html">
  <div ng-include="'/be/tpl/success.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create Workshop</h1>
  <a id="top"></a>
</div>

<div class="container-fluid">
	<div class="row wrapper-md">
		<form name="workshopform" class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="createworkshop(workshop)">
		<div class="col-md-4">
			<div class="panel panel-info">
				<div class="panel-heading">&nbsp;</div>
					<div class="panel-body">

						<div class="form-group">
							<label class="control-label"><b>Title</b></label>
							<div ui-module="select2">
								<select ui-select2 class="w-lg form-control" ng-model="workshop.title" required>
									<optgroup label="Choose Workshop Title">
										<option ng-repeat="title in selectlist.titles" value="{[{title.workshoptitleid}]}">{[{title.title}]}</option>
									</optgroup>
								</select>
							</div>
						</div>

						<div class="line line-lg"></div>

						<div class="form-group">
							<label class="control-label"><b>Venue</b></label>
							<div ui-module="select2">
								<select ui-select2 class="w-lg form-control" ng-model="workshop.venue" required>
									<optgroup label="Choose Workshop Venue">
										<option value="bnbcenter">Body & Brain Center</option>
										<option ng-repeat="venue in selectlist.venues" value="{[{venue.workshopvenueid}]}">{[{venue.venuename}]}</option>
									</optgroup>
								</select>
							</div>
						</div>

						<div class="line line-lg" ng-show="workshop.venue == 'bnbcenter'"></div>

						<div class="form-group" ng-show="workshop.venue == 'bnbcenter'">
							<label class="control-label"><b>Center</b></label>
							<div ui-module="select2">
								<select ui-select2 class="w-lg form-control" ng-model="workshop.center" ng-required="workshop.venue == 'bnbcenter'">
									<optgroup label="Choose Workshop Venue">
										<option ng-repeat="mem in newslocation" value="{[{mem.centerid}]}">{[{mem.centertitle}]}, {[{mem.centerstate}]}</option>
									</optgroup>
								</select>
							</div>
						</div>

						<div class="line line-lg" ng-show="workshop.venue == 'bnbcenter'"></div>

						<div class="form-group" ng-show="workshop.venue == 'bnbcenter'">
							<label class="control-label"><b>Associated Center</b></label>

							<!-- <div ui-module="select2">
								<select ui-select2 class="form-control w-lg" ng-model="workshop.center" required>
									<optgroup label="Choose Workshop Center">
										<option ng-repeat="center in selectlist.centers" value="{[{center.centerid}]}">{[{center.centertitle}]}, {[{center.centerstate}]}</option>
									</optgroup>
								</select>
							</div> -->

							<select chosen multiple class=" chosen-choices form-control w-md" ng-model="workshop.associatedcenter" options="newslocation" ng-options="cat.centerid as cat.centertitle +', '+ cat.centerstate for cat in newslocation" ng-required="workshop.venue == 'bnbcenter'" ng-change="fucktion(workshop.associatedcenter)">
							</select>

						</div>

						<div class="line line-lg"></div>

						<div class="form-group">
							<label class="control-label"><b>Tuition</b></label>

							<div class="input-group m-b width_150">
								<span class="input-group-addon">$</span>
								<input type="text" class="form-control" ng-model="workshop.tuition" pattern="\d*" required>
							</div>
						</div>

						<div class="line line-lg"></div>

						<div class="form-group">
							<label class="control-label"><b>Schedule</b></label>
							<br>
							Start Date <br>
							<div class="input-group w-md">
								<span class="input-group-btn">
									<input id="date" name="date" class="form-control" ng-model="workshop.datestart" datepicker-popup="yyyy-MM-dd" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text"  disabled ng-change="puttoend(workshop.datestart)">
									<button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
								</span>
							</div>
							<select class="inputBox" ng-init="workshop.starthour = '07' " ng-model="workshop.starthour" required>
								<option ng-repeat="hour in hours" ng-value="{[{hour.time}]}">{[{hour.time}]}</option>
							</select>
							:
							<select class="inputBox" ng-init="workshop.startminute = '00' " ng-model="workshop.startminute" required>
								<option ng-repeat="minute in minutes" ng-value="{[{minute.time}]}">{[{minute.time}]}</option>
							</select>
							<select class="inputBox" ng-init="workshop.starttimeformat = 'AM' " ng-model="workshop.starttimeformat" required>
								<option ng-value="AM">AM</option>
								<option ng-value="PM">PM</option>
							</select>
						</div>

						<div class="line line-lg"></div>
						<div class="form-group">
							<label class="control-label"></label>
							End Date <br>
							<div class="input-group w-md">
								<span class="input-group-btn">
									<input id="date" name="date" class="form-control" ng-model="workshop.dateend" datepicker-popup="yyyy-MM-dd" is-open="opened1" datepicker-options="dateOptions"  close-text="Close" type="text" required disabled>
									<button type="button" class="btn btn-default" ng-click="open1($event)"><i class="glyphicon glyphicon-calendar"></i></button>
								</span>
							</div>
							<select class="inputBox" ng-init="workshop.endhour = '08' " ng-model="workshop.endhour" required>
								<option ng-repeat="hour in hours" ng-value="{[{hour.time}]}">{[{hour.time}]}</option>
							</select>
							:
							<select class="inputBox" ng-init="workshop.endminute = '00' " ng-model="workshop.endminute" required>
								<option ng-repeat="minute in minutes" ng-value="{[{minute.time}]}">{[{minute.time}]}</option>
							</select>
							<select class="inputBox" ng-init="workshop.endtimeformat = 'AM' " ng-model="workshop.endtimeformat" required>
								<option ng-value="AM">AM</option>
								<option ng-value="PM">PM</option>
							</select>
						</div>
	          		</div> <!-- panel-body -->
				</div> <!-- panel-wrapper -->
			</div> <!-- col-md-5 -->
			<div class="col-md-8">
				<div class="panel panel-info">
				<div class="panel-heading">&nbsp;</div>
					<div class="panel-body">
      					<div class="form-group">
      						<label class="control-label"><b>About Workshop</b></label>
      						<textarea class="form-control resize_vert" ng-model="workshop.about" rows="6" placeholder="Write something about this workshop"></textarea>
      					</div>

      					<div class="line line-lg"></div>

      					<div class="form-group">
      						<label class="control-label"><b>Note</b></label>

      						<textarea class="form-control resize_vert" ng-model="workshop.note" rows="3"></textarea>

      					</div>

      					<div class="line line-lg"></div>

      					<div class="form-group">
      						<label class="control-label"><b>Accomodation & Meals</b></label>

      						<textarea class="form-control resize_vert" ng-model="workshop.accomodation" rows="3"></textarea>

      					</div>

      					<div class="line line-lg"></div>

      					<div class="form-group">
      						<label class="control-label"><b>Miscellaneous</b></label>

      						<textarea class="form-control resize_vert" ng-model="workshop.miscellaneous" rows="3"></textarea>

      					</div>

      					<div class="line line-lg"></div>

      					<div class="form-group">
      						<label class="control-label"><b>Meta Title</b></label>
      						<em class="pull-right">Optional</em>
      						<input type="text" class="form-control title" ng-model="workshop.metatitle" ng-model="workshop.metatitle">
      						<span class="help-block m-b-none">Use comma (,) for multiple meta tags. E.g. (focus, strength, peace)</span>


      					</div>

      					<div class="line line-lg"></div>

      					<div class="form-group">
      						<label class="control-label"><b>Meta Description</b></label>
      						<em class="pull-right">Optional</em>
      						<textarea class="form-control resize_vert" ng-model="workshop.metadesc" rows="3"></textarea>

      					</div>

      					<div class="line line-lg"></div>

      					<div class="form-group">
      						<span class="pull-right">
      							<a ui-sref="createworkshop({tab: '3'})" class="btn btn-md btn-danger">Cancel</a>
      							<button class="btn btn-md btn-success" ng-disabled="workshopform.$invalid">Save</button>
      						</span>
      					</div>
					</div> <!-- panel-body -->
				</div> <!-- panel-wrapper -->
			</div> <!-- col-md-8 -->
		</form>
	</div> <!-- row -->
</div> <!-- container -->
