<script type="text/ng-template" id="success.html">
  <div ng-include="'/be/tpl/success.html'"></div>
</script>

<div class="container-fluid">
	<div class="row wrapper-md">
		<div class="panel panel-warning">
			<div class="panel-heading"><h4>Edit Workshop</h4></div>
			<div class="panel-body">
				<form name="workshopform" class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="updateworkshop(upworkshop)">
				<div class="col-md-4">
						<div class="form-group">
							<label class="col-md-3 control-label"><b>Title</b></label>
							<div class="col-md-9">

								<div ui-module="select2">
									<select ui-select2 class="form-control" ng-model="upworkshop.title" required>
										<optgroup label="Change workshop title">
											<option ng-repeat="taytel in selectlist.titles" ng-value="taytel.workshoptitleid">{[{taytel.title}]}</option>
										</optgroup>
									</select>
								</div>

							</div>
						</div>

						<div class="line line-lg"></div>

						<div class="form-group">
							<label class="col-md-3 control-label"><b>Venue</b></label>
							<div class="col-md-9">
								<div ui-module="select2">
									<select ui-select2 class="form-control" ng-model="upworkshop.venue" required>
										<optgroup label="Change Workshop Venue">
											<option value="bnbcenter">Body & Brain Center</option>
											<option ng-repeat="venue in selectlist.venues" value="{[{venue.workshopvenueid}]}">{[{venue.venuename}]}</option>
										</optgroup>
									</select>
								</div>
							</div>
						</div>

						<div class="line line-lg" ng-show="upworkshop.venue == 'bnbcenter'"></div>

						<div class="form-group" ng-show="upworkshop.venue == 'bnbcenter'">
							<label class="col-md-3 control-label"><b>Venue</b></label>
							<div class="col-md-9">
							<div ui-module="select2">
								<select ui-select2 class="w-lg form-control" ng-model="upworkshop.center" ng-required="upworkshop.venue == 'bnbcenter'">
									<optgroup label="Choose Workshop Venue">
										<option ng-repeat="mem in newslocation" value="{[{mem.centerid}]}">{[{mem.centertitle}]}, {[{mem.centerstate}]}</option>
									</optgroup>
								</select>
							</div>
							</div>
						</div>

						<div class="line line-lg" ng-show="upworkshop.venue == 'bnbcenter'"></div>

						<div class="form-group" ng-show="upworkshop.venue == 'bnbcenter'">
							<label class="control-label"><b>Associated Center</b></label>

							<!-- <div ui-module="select2">
								<select ui-select2 class="form-control w-lg" ng-model="workshop.center" required>
									<optgroup label="Choose Workshop Center">
										<option ng-repeat="center in selectlist.centers" value="{[{center.centerid}]}">{[{center.centertitle}]}, {[{center.centerstate}]}</option>
									</optgroup>
								</select>
							</div> -->

							<select chosen multiple class=" chosen-choices form-control w-md" ng-model="upworkshop.associatedcenter" options="newslocation" ng-options="cat.centerid as cat.centertitle +', '+ cat.centerstate for cat in newslocation" ng-required="workshop.venue == 'bnbcenter'" ng-change="fucktion(workshop.associatedcenter)">
							</select>

						</div>


					

					<div class="line line-lg"></div>

					<div class="form-group">
						<label class="col-md-3 control-label"><b>Tuition</b></label>
						<div class="col-md-9">
							<div class="input-group m-b width_150">
								<span class="input-group-addon">$</span>
								<input type="text" class="form-control" ng-model="upworkshop.tuition" pattern="\d*" required>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label"><span ng-show="discount"><b>Discount</b></span></label>
						<div class="col-md-9">
							<div class="input-group m-b width_150" ng-if="discount == true">
								<span class="input-group-addon bg-danger">$</span>
								<input type="text" class="form-control" ng-model="upworkshop.sale" pattern="\d*" placeholder="SALE" required>
								<span class="input-group-addon bg-danger" ng-click="nodiscount()">x</span>
							</div>
							<label class="btn-sm bg-primary" ng-click="setdiscount()" ng-hide="discount">Set Discount!</label>
						</div>
					</div>

					

					<div class="line line-lg"></div>

					<div class="form-group">

						<label class="col-md-3 control-label"><b>Schedule</b></label>
						<div class="col-md-9">
							Start Date<br>
							<div class="input-group w-md">
								<span class="input-group-btn">
									<input id="date" name="date" class="form-control" ng-model="upworkshop.datestart" datepicker-popup="yyyy-MM-dd" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
									<button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
								</span>
							</div>
							<select class="inputBox" ng-model="upworkshop.starthour" required>
								<option ng-repeat="hour in hours" ng-value="hour.time">{[{hour.time}]}</option>
							</select>
							:
							<select class="inputBox"  ng-model="upworkshop.startminute" required>
								<option ng-repeat="minute in minutes" ng-value="minute.time">{[{minute.time}]}</option>
							</select>
							<select class="inputBox"  ng-model="upworkshop.starttimeformat" required>
								<option ng-value="AM">AM</option>
								<option ng-value="PM">PM</option>
							</select>

						</div>
					</div>

					<div class="line line-lg"></div>
					<div class="form-group">
						<label class="col-md-3 control-label"></label>
						<div class="col-md-9">
							End Date {[{upworkshop.endhour}]}<br>
							<div class="input-group w-md">
								<span class="input-group-btn"> 
									<input id="date" name="date" class="form-control" ng-model="upworkshop.dateend" datepicker-popup="yyyy-MM-dd" is-open="opened1" datepicker-options="dateOptions"  close-text="Close" type="text" required disabled>
									<button type="button" class="btn btn-default" ng-click="open1($event)"><i class="glyphicon glyphicon-calendar"></i></button>
								</span>
							</div>
							<select class="inputBox" ng-model="upworkshop.endhour" required>
								<option ng-repeat="hour in hours" ng-value="hour.time">{[{hour.time}]}</option>
							</select>
							:
							<select class="inputBox" ng-model="upworkshop.endminute" required>
								<option ng-repeat="minute in minutes" ng-value="minute.time">{[{minute.time}]}</option>
							</select>
							<select class="inputBox" ng-init="workshop.endtimeformat = 'AM' " ng-model="workshop.endtimeformat" required>
								<option ng-value="AM">AM</option>
								<option ng-value="PM">PM</option>
							</select>
						</div>
					</div>

				</div>
				<div class="col-md-8">

					<div class="form-group">
						<label class="col-md-3 control-label"><b>About Workshop</b></label>
						<div class="col-md-9">
							<textarea class="form-control resize_vert" ng-model="upworkshop.about" rows="6" placeholder="Write something about this workshop" required></textarea>
						</div>
					</div>

					<div class="line line-lg"></div>

					<div class="form-group">
						<label class="col-md-3 control-label"><b>Note</b></label>
						<div class="col-md-9">
							<textarea class="form-control resize_vert" ng-model="upworkshop.note" rows="3" required></textarea>
						</div>
					</div>

					<div class="line line-lg"></div>

					<div class="form-group">
						<label class="col-md-3 control-label"><b>Accomodation & Meals</b></label>
						<div class="col-md-9">
							<textarea class="form-control resize_vert" ng-model="upworkshop.accomodation" rows="3" required></textarea>
						</div>
					</div>

					<div class="line line-lg"></div>

					<div class="form-group">
						<label class="col-md-3 control-label"><b>Miscellaneous</b></label>
						<div class="col-md-9">
							<textarea class="form-control resize_vert" ng-model="upworkshop.miscellaneous" rows="3" required></textarea>
						</div>
					</div>

					<div class="line line-lg"></div>

					<div class="form-group">
						<label class="col-md-3 control-label"><b>Meta Title</b></label>
						<em class="pull-right">Optional</em>
						<div class="col-md-9">
							<input type="text" class="form-control title" ng-model="upworkshop.metatitle" ng-model="workshop.metatitle">
						</div>
					</div>

					<div class="line line-lg"></div>

					<div class="form-group">
						<label class="col-md-3 control-label"><b>Meta Description</b></label>
						<em class="pull-right">Optional</em>
						<div class="col-md-9">
							<textarea class="form-control resize_vert" ng-model="upworkshop.metadesc" rows="3"></textarea>
						</div>
					</div>

					<div class="line line-lg"></div>

					<div class="form-group">
						<span class="pull-right">
							<a ui-sref="createworkshop({tab: '3'})" class="btn btn-md btn-danger" ng-click="isCollapsed2 = !isCollapsed2" ng-disabled="cancel">Close</a>
							<button class="btn btn-md btn-success" ng-click="isCollapsed2 = isCollapsed2" ng-disabled="workshopform.$invalid">Save</button>
						</span>
					</div>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>
