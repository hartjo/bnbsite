<script type="text/ng-template" id="offerAdd.html">
  <div ng-include="'/be/tpl/offerAdd.html'"></div>
</script>

<script type="text/ng-template" id="offerEdit.html">
  <div ng-include="'/be/tpl/offerEdit.html'"></div>
</script>

<script type="text/ng-template" id="offerDelete.html">
  <div ng-include="'/be/tpl/offerDelete.html'"></div>
</script>

  <!-- header -->
  <div class="wrapper bg-light lter b-b" style="height:61px;">
    <div class="btn-group m-r-sm">
      <h1 class="m-n font-thin h3">Special Offers</h1>
      <a id="top"></a>
    </div>
    <div class="btn-group m-r-sm">
      <a ng-click="addoffer()" class="btn btn-sm btn-success w-xm font-bold">Add New Offer</a>
    </div>
  </div>


<fieldset ng-disabled="isSaving">
  <div class="line line-dashed b-b line-lg"></div>
    <div class="col-sm-12">
      <alert ng-repeat="alert in offeralert" type="{[{alert.type }]}" close="closeAlert($index, type='default')" >{[{ alert.msg }]}</alert>
    </div>
  <div class="line line-dashed b-b line-lg"></div>
  
  <div class="row">
    <div class="col-sm-12">

      <div class="col-sm-4" ng-repeat="data in offerdata">
        <div class="bg-gray">
<!--           <a href="" ng-click="deleteoffer(data.offerid)" class="closebutton">&times;</a> -->
          <div class="col-sm-12">Offer Status</div>
          <div class="col-sm-12">
            <div class="statusswitch">
              <label class="i-switch bg-info l-t-xs l-r" style="margin-top:1px">
                <input type="checkbox" ng-true-value="1" ng-click="setstatus(data.status,data.offerid)" ng-model="data.status" ng-false-value="0">
                <i></i>
              </label>
            </div>
            <div class="statusinfo">
              <span class="label bg-info" ng-show="data.status == 1">Show</span>
              <span class="label bg-danger" ng-show="data.status == 0">Hidden</span>
            </div>
            <div class="pull-right mr_tp-10px">
              <a class="btn btn-sm btn-default" ng-click="editoffer(data.offerid)"><i class="glyphicon glyphicon-edit text-primary"></i></a>
              <a class="btn btn-sm btn-default" ng-click="deleteoffer(data.offerid)"><i class="glyphicon glyphicon-trash text-danger"></i></a>
            </div>
          </div>
          <div class="imagegallerystyle" style="background-image: url('<?php echo $this->config->application->amazonlink; ?>/uploads/offerimages/{[{data.image}]}');">
          </div>
          <div class="col-sm-12">
              <tabset class="tab-container">
                <tab heading="Title">
                  {[{data.title}]}
                </tab>
                <tab heading="Description">
                  {[{data.description}]}
                </tab>
                <tab heading="Expiration">
                  {[{data.expiration}]}
                </tab>
              </tabset>
          </div>
        </div>
      </div>


    </div>
  </div>
</fieldset>
