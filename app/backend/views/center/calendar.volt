<script type="text/ng-template" id="eventDelete.html">
  <div ng-include="'/be/tpl/eventDelete.html'"></div>
</script>

<div >
  <!-- header -->
  <div class="wrapper bg-light lter b-b" style="height:61px;">
    <div class="btn-group m-r-sm">
      <h1 class="m-n font-thin h3">Calendar</h1>
      <a id="top"></a>
    </div>
    <div class="btn-group m-r-sm">
      <a ng-click="showaddevent = true" class="btn btn-sm btn-success w-xm font-bold">Add New Event</a>
    </div>
  </div>

<form class="bs-example form-horizontal" name="formcalendar" ng-submit="saveEvent(calendar)">
 <fieldset ng-disabled="isSaving" class="fade-in-up" ng-show="showaddevent">
  <div class="wrapper-md">  
    <div class="row">
      <div class="col-sm-12">
          <div class="panel panel-primary">
              <div class="panel-heading font-bold">
              Add new Event
            </div>
            <div class="panel-body">
                
              Title
              <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="calendar.calendartitle" required="required">

              <div class="line line-dashed b-b line-lg"></div>

              Date
              <div class="input-group w-md">
                <span class="input-group-btn">
                  <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="calendar.calendardate" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
                  <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                </span>
              </div>

              <div class="line line-dashed b-b line-lg"></div>

              <div class="col-lg-3">
              From <em class="text-muted">(format 7:00AM)</em>
              <br>
              <select class="inputBox" ng-model="calendar.timefrom" 
               ng-options="mem.time as mem.time for mem in timelist" required>
              </select>
            
              <select class="inputBox" ng-model="calendar.timefromformat" required>
              <option value="AM">AM</option>
              <option value="PM">PM</option>
              </select>
              </div>
              <div class="col-lg-3">
              To  <em class="text-muted">(format 12:30PM)</em>
              <br>
              <select class="inputBox" ng-model="calendar.timeto" 
               ng-options="mem.time as mem.time for mem in timelist" required>
              </select>
            
              <select class="inputBox" ng-model="calendar.timetoformat" required>
              <option value="AM">AM</option>
              <option value="PM">PM</option>
              </select>
              </div>
              </div>

              <div class="line line-dashed b-b line-lg"></div>

               
              <div class="col-sm-12">
              Description
              <textarea  class="form-control replyckEditor" rows="6" placeholder="Type your message" ng-model="calendar.description" required>

              </textarea>

              </div>

              <div class="line line-dashed b-b line-lg"></div>

              <div class="col-sm-12">Status</div>
              <div class="line line-dashed b-b line-lg"></div>
              <div class="col-sm-3">

                <div class="col-sm-3">
                  <label class="i-switch bg-info l-t-xs l-r" style="margin-top:1px">
                    <input type="checkbox" ng-true-value="1" ng-model="calendar.status" ng-false-value="0">
                    <i></i>
                  </label>
                </div>

                <div class="col-sm-3">
                  <label class="col-sm-3 control-label">
                    <span class="label bg-info" ng-show="calendar.status == 1">Show</span>
                    <span class="label bg-danger" ng-show="calendar.status == 0">Hide</span>
                  </label>
                </div>

              </div>
                
              <footer class="panel-footer text-right bg-light lter">
                <a class="btn btn-default" ng-click="showaddevent = false"> Cancel </a>
                <button type="submit" class="btn btn-success" ng-disabled="formcalendar.$invalid">Submit</button>
              </footer>

            </div> <!-- end of panel body -->
        </div> <!-- end of panel panel-primary -->
      </div> <!-- end of col-sm-12 -->

    </div> <!-- end of row -->

  
  </div> <!-- end of wrapper-md -->
</fieldset>
</form>


<form class="bs-example form-horizontal" name="formupdatecalendar" ng-submit="updateEvent(calendar)">
 <fieldset ng-disabled="isSaving" class="fade-in-up" ng-show="showeditevent">
  <div class="wrapper-md">  
    <div class="row">
      <div class="col-sm-12">
          <div class="panel panel-primary">
            <div class="panel-heading font-bold">
              Edit Event
            </div>
            <div class="panel-body">
              <input type="hidden" ng-model="calendar.activityid">
              Title
              <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="calendar.activitytitle" required="required">

              <div class="line line-dashed b-b line-lg"></div>

              Date
              <div class="input-group w-md">
                <span class="input-group-btn">
                  <input id="date" name="date" class="form-control" datepicker-popup="yyyy-MM-dd" ng-model="calendar.activitydate" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
                  <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                </span>
              </div>


              <div class="line line-dashed b-b line-lg"></div>

              <div class="col-lg-3">
              From <em class="text-muted">(format 7:00AM)</em>
              <br>
              <select class="inputBox" ng-model="calendar.activitytimefrom" 
               ng-options="mem.time as mem.time for mem in timelist " required>
              </select>
            
              <select class="inputBox" ng-model="calendar.activitytimefromformat" required>
              <option value="AM">AM</option>
              <option value="PM">PM</option>
              </select>
              </div>
              <div class="col-lg-3">
              To  <em class="text-muted">(format 12:30PM)</em>
              <br>
              <select class="inputBox" ng-model="calendar.activitytimeto" 
               ng-options="mem.time as mem.time for mem in timelist" required>
              </select>
            
              <select class="inputBox" ng-model="calendar.activitytimetoformat" required>
              <option value="AM">AM</option>
              <option value="PM">PM</option>
              </select>
              </div>
              </div>

              <div class="line line-dashed b-b line-lg"></div>

              <div class="col-sm-12">
                Description
                <textarea  class="form-control replyckEditor" rows="6" placeholder="Type your message" ng-model="calendar.description" required>

                </textarea>

              </div>

              <div class="line line-dashed b-b line-lg"></div>

              <div class="col-sm-12">Status</div>
              <div class="line line-dashed b-b line-lg"></div>
              <div class="col-sm-3">

                <div class="col-sm-3">
                  <label class="i-switch bg-info l-t-xs l-r" style="margin-top:1px">
                    <input type="checkbox" ng-true-value="1" ng-model="calendar.status" ng-false-value="0">
                    <i></i>
                  </label>
                </div>

                <div class="col-sm-3">
                  <label class="col-sm-3 control-label">
                    <span class="label bg-info" ng-show="calendar.status == 1">Show</span>
                    <span class="label bg-danger" ng-show="calendar.status == 0">Hide</span>
                  </label>
                </div>

              </div>
                
              <footer class="panel-footer text-right bg-light lter">
                <a class="btn btn-default" ng-click="showeditevent = false"> Cancel </a>
                <button type="submit" class="btn btn-success" ng-disabled="formupdatecalendar.$invalid">Submit</button>
              </footer>

            </div> <!-- end of panel body -->
        </div> <!-- end of panel panel-primary -->
      </div> <!-- end of col-sm-12 -->

    </div> <!-- end of row -->

  
  </div> <!-- end of wrapper-md -->
</fieldset>
</form>


<fieldset ng-disabled="isSaving" ng-show="showaddevent != true && showeditevent != true">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

    <div class="row">

      <div class="col-sm-12">
        <div class="panel panel-primary">

          <div class="panel-heading font-bold">
            Calendar List
          </div>

          <div class="panel-body">


            <div class="row wrapper">
              <div class="col-sm-5 m-b-xs">
                <div class="input-group">
                  <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                  <span class="input-group-btn">
                    <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                  </span>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table table-striped b-t b-light">
                <thead>
                  <tr>

                    <th style="width:20%">Title</th>
                    <th style="width:15%">Date</th>
                    <th style="width:15%">From</th>
                    <th style="width:15%">To</th>
                    <th style="width:25%">Status</th>
                    <th style="width:30%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr ng-repeat="mem in data.data">

                    <td>{[{ mem.activitytitle }]}</td>
                    <td>{[{ mem.activitydate }]}</td>
                    <td>{[{ mem.activitytimefrom}]} {[{ mem.activitytimefromformat}]}</td>
                    <td>{[{ mem.activitytimeto}]} {[{ mem.activitytimetoformat}]}</td>
                    <td  ng-if="mem.status == 1">
                      <div class="pagestatuscontent fade-in-out"><span class="label bg-success" >Active</span></div>
                      <div class="checkstatuscontent">
                        <label class="i-switch bg-info m-t-xs m-r">
                          <input type="checkbox" checked="" ng-click="setstatus(mem.status,mem.activityid,searchtext,mem.activityid)">
                          <i></i>
                        </label>

                      </div>
                      <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.activityid"><i class="fa fa-check"></i></spand></div>
                    </td>
                    <td  ng-if="mem.status == 0">
                      <div class="pagestatuscontent fade-in-out"><span class="label bg-danger">Deactivated</span></div>
                      <div class="checkstatuscontent">
                        <label class="i-switch bg-info m-t-xs m-r">
                          <input type="checkbox" ng-click="setstatus(mem.status,mem.activityid,searchtext,mem.activityid)">
                          <i></i>
                        </label>

                      </div>
                      <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.activityid"><i class="fa fa-check"></i></spand></div>
                    </td>
                  </td>
                  <td>
                    <a href="" ng-click="editevent(mem.activityid)"><span class="label bg-warning" >Edit</span></a>
                    <a href="" ng-click="deleteevent(mem.activityid)"> <span class="label bg-danger">Delete</span></a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>


        </div> <!-- END OF panel Body-->


      </div>
    </div>
  </div> <!-- end of row -->


  <div class="row">
    <div class="panel-body">
      <footer class="panel-footer text-center bg-light lter">
        <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
      </footer>
    </div>
  </div>


</div>
</fieldset>
<script type="text/javascript">
 
;(function ( $, window, document, undefined ) {

    // Create the defaults once
    var pluginName = 'timeMask',
        defaults = {
            propertyName: "value"
        };

    // The actual plugin constructor
    function Plugin( element, options ) {
        this.$el = $(element);

        this.options = $.extend( {}, defaults, options) ;

        this._defaults = defaults;
        this._name = pluginName;
        this._val = this.$el.val();

        this.init();
    }
    // retruns array of numbers in a string
    
    Plugin.prototype = {
        _getValueInts: function() {
            var ints=[],
                j =0;

            // Get array of number values    
            for(i=0; i<this._val.length; i++) {
                if(parseInt(this._val[i]) >= 0) {
                    ints[j] = parseInt(this._val[i]);
                    j++;
                }
            }
            return ints;    
        },
        
        _isPM: function() {
            return (this._val.search(/p/gi) >= 0);
        },
        
        _setInvalid: function() {
            this.$el.addClass('invalid-time');
        },
        
        _rmInvalid: function() {
            this.$el.removeClass('invalid-time');
        },
        
        
        getTimeStr: function() {
            var n = this._getValueInts(),
                pm = this._isPM(),                    // Defaults to false (am)
                time = [0, 0, 0, 0];                // 00:00
        
            // No n -> invalid
            if(n.length < 1) {
                return false;    
            }
            
            // Set time array
            else if(n.length == 1) {
                time = [0, n[0], 0, 0];
            }
            
            else if (n.length == 2) {
                if(n[0] > 1) {                                // "91" => 09:10, 21 => 02:10
                    time = [0, n[0], n[1], 0]
                }
                else if(n[1] >= 6) {                        // "17" => 05:00 pm
                    time = [0, n[1]-2, 0, 0];
                    pm = true;
                }
                else {                                        // "12" => 12:00, "05" => 05:00pm
                    time = [n[0], n[1], 0, 0];
                }
            }
            
            else if (n.length == 3) {
                if(n[1] >= 6) {                                // "170" => invalid
                    return false;
                }
                else if (n[0] < 1) {                        // "012" => invalid
                    return false;
                }
                else {                                        // "123" => 1:23
                    time = [0, n[0], n[1], n[2]];
                }
            }
            
            else if (n.length == 4) {
                var hours = n[0]*10 + n[1];
                
                if (n[2] >= 6) {                            // 12:95
                    return false;
                }
                else if(hours > 24) {
                    return false;
                }
                else if(hours > 12) {                        // "2312" => 11:12pm; hours = 11
                    hours = hours-12;
                    pm = true
                    if(hours >= 10) {
                        time = [1, hours-10, n[2], n[3]];
                    }
                    else {
                        time = [0, hours, n[2], n[3]];
                    }
                }
                else if(n[0] == 0 && n[1] == 0) {            // "00:12" => 12:12 am
                    time = [1, 2, n[2], n[3]];
                    pm = false;
                }
                else {
                    time = [n[0], n[1], n[2], n[3]];
                }
            }
            
            return "" + ((time[0]==0)? "":time[0]) + time[1] + ":" + time[2] + time[3] + ((!pm)? "AM": "PM");
        }, // end getTimeStr()
        
        _setVal: function(newVal) {
            this.val = newVal;
            this.$el.val(newVal);
        },
    

        init: function() {
            var _this = this;
            this.$el.bind({
                'focus.timeMask': function() {
                    _this._rmInvalid();
                },
                'blur.timeMask': function() {
                    _this._val = _this.$el.val();
                    var timeStr = _this.getTimeStr()
                    
                    if(!timeStr) {
                        _this._setInvalid();
                    }
                    else {
                        _this._setVal(timeStr);
                    }    
                }
            }); // end this$el.bind
        }, // end init
        
        /* // This would be nice, but this plugin pattern isn't set up for public methods... 
        destroy: function() {
            console.log('destroying');
            this.$el.unbind('.timeMask');
        }*/
        
    }; // end Plugin.prototype
    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName,
                new Plugin( this, options ));
            }
        });
    }

        })( jQuery, window, document );
$(document).ready(function() {
  var $input1 = $('#timeInput');
  $input1.timeMask();

  var $input2 = $('#timeInput2');
  $input2.timeMask();
});

</script>