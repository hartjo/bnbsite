{{ content() }}
<script type="text/ng-template" id="regionAdd.html">
   <div ng-include="'/be/tpl/regionAdd.html'"></div>
</script>

<script type="text/ng-template" id="regionDelete.html">
   <div ng-include="'/be/tpl/regionDelete.html'"></div>
</script>

<script type="text/ng-template" id="districtAdd.html">
   <div ng-include="'/be/tpl/districtAdd.html'"></div>
</script>

<script type="text/ng-template" id="districtDelete.html">
   <div ng-include="'/be/tpl/districtDelete.html'"></div>
</script>

<script type="text/ng-template" id="districtEdit.html">
   <div ng-include="'/be/tpl/districtEdit.html'"></div>
</script>


<div class="bg-light lter b-b wrapper-md">
    <h1 class="m-n font-thin h3">Region and District Management</h1>
    <a id="top"></a>
</div>


<fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
        <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

        <div class="row">

            <div class="col-sm-6">

                <div class="panel panel-default">
                    <div class="panel-heading font-bold">
                      Region list
                    </div>

                        <div class="panel-body">
                            <button type="button" class="btn m-b-xs btn-sm btn-primary btn-addon" ng-click="addregion()"><i class="fa fa-plus" style="width=100%;"></i>Add New Region
                            </button>
                        </div>

                        <div class="panel-body">

                            <div class="row wrapper">
                                <div class="col-sm-5 m-b-xs">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm btn-default" type="button" ng-click="clearsearch()">Clear</button>
                                        </span>
                                        <input class="input-sm form-control" placeholder="Search" type="text" ng-model="regionsearchtext">
                                        <span class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button" ng-click="search(regionsearchtext)">Go!</button>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-striped b-t b-light">
                                    <thead>
                                        <tr>
                                            <th style="width:80%">Region name</th>
                                            <th style="width:25%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="region in regionlist" >
                                            <td> <span editable-text="region.regionname" onbeforesave="updateregion($data, region.regionid)" e-oninvalid="setCustomValidity('Please enter Alphabets and Numbers only ')"  e-required e-form="textBtnForm">{[{ region.regionname }]}</span></td>
                                            </td>
                                            <td>
                                            <a href="" ng-click="textBtnForm.$show()" ng-hide="textBtnForm.$visible"><span class="label bg-warning" >Edit</span></a>
                                            <a href="" ng-click="regionDelete(region.regionid)" ng-hide="textBtnForm.$visible"> <span class="label bg-danger">Delete</span>
                                            </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>




            <div class="panel-body">
                <footer class="panel-footer text-center bg-light lter">
                  <pagination total-items="regionTotalItems" ng-model="regionCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setregionPage(regionCurrentPage)"></pagination>
                </footer>
            </div>


            </div> <!-- end of region -->




            <!-- <div class="col-sm-6">

                <div class="panel panel-default">
                    <div class="panel-heading font-bold">
                      District list
                    </div>

                        <div class="panel-body">
                            <button type="button" class="btn m-b-xs btn-sm btn-primary btn-addon" ng-click="adddistrict()"><i class="fa fa-plus" style="width=100%;"></i>Add New District
                            </button>
                        </div>

                        <div class="panel-body">

                            <div class="row wrapper">
                                <div class="col-sm-5 m-b-xs">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm btn-default" type="button" ng-click="clearsearchdistrict()">Clear</button>
                                        </span>
                                        <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                                        <span class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-striped b-t b-light">
                                    <thead>
                                        <tr>
                                            <th style="width:40%">District name</th>
                                            <th style="width:40%">Region</th>
                                            <th style="width:25%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="list in districtlist" >
                                            <td> <span editable-text="list.districtname" onbeforesave="updatecategory($data, list.districtid)" e-oninvalid="setCustomValidity('Please enter Alphabets and Numbers only ')"  e-required e-form="textBtnForm">{[{ list.districtname }]}</span></td>
                                            <td> <span editable-text="list.regionname" onbeforesave="updatecategory($data, list.regionid)" e-oninvalid="setCustomValidity('Please enter Alphabets and Numbers only ')"  e-required e-form="textBtnForm">{[{ list.regionname }]}</span></td>
                                            <td>
                                            <a href="" ng-click="districtEdit(list.districtid)"><span class="label bg-warning" >Edit</span></a>
                                            <a href="" ng-click="districtDelete(list.districtid)" ng-hide="textBtnForm.$visible"> <span class="label bg-danger">Delete</span>
                                            </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>




            <div class="panel-body">
                <footer class="panel-footer text-center bg-light lter">
                  <pagination total-items="districtTotalItems" ng-model="districtCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setdistrictPage(districtCurrentPage)"></pagination>
                </footer>
            </div>


            </div> --> <!-- end of District -->


        </div> <!-- end of row -->

          

    </div>
</fieldset>

