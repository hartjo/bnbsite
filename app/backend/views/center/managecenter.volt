{{ content() }}
<?php $role = $username['pi_userrole']; ?>
<script type="text/ng-template" id="centerDelete.html">
  <div ng-include="'/be/tpl/centerDelete.html'"></div>
</script>

<script type="text/ng-template" id="centerEdit.html">
  <div ng-include="'/be/tpl/centerEdit.html'"></div>
</script>

<script type="text/ng-template" id="centerManage.html">
  <div ng-include="'/be/tpl/centerManage.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <button type="button" class="btn btn-default btn-addon pull-right m-t-n-xs" ui-toggle-class="show" target="#aside" ng-click="renderCalender(calendar1)">
  <i class="fa fa-bars"></i> Logs
  </button>
  <h1 class="m-n font-thin h3">Center List</h1>
  <a id="top"></a>
</div>


<fieldset ng-disabled="isSaving">
<div class="hbox hbox-auto-xs hbox-auto-sm">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Center List
            </div>

              <div class="panel-body">

                <div class="row wrapper">
                    <div class="col-sm-5 m-b-xs">
                        <div class="input-group">
                            <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                            <span class="input-group-btn">
                            <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                            </span>
                        </div>
                    </div>
                    <div class="line line-dashed b-b line-lg"></div>
                    <div class="col-sm-5 m-b-xs">
                      <div class="input-group">
                        <select ng-model="searchtext2" class="form-control m-b ng-pristine ng-valid ng-touched" ng-change="search(searchtext2)">
                          <option value="" style="display:none">Filter Center</option>
                          <option value="null">List All Center</option>
                          <option ng-repeat="list in centerdatalist.data" value="{[{ list.centertitle }]}">{[{ list.centertitle }]}</option>
                        </select>
                      </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Region</th>
                                <th>Type</th>
                                <th>Status</th>
                                <th>Date Created</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="mem in data.data">
                               
                                <td>{[{ mem.centertitle }]}</td>
                                <td>{[{ mem.regionname }]}</td>
                                <td ng-if="mem.centertype == 1">Body and Brain</td>
                                <td ng-if="mem.centertype == 2">Franchise</td>
                                <td ng-if="mem.centertype == 3">Affiliated</td>
                                <td  ng-if="mem.status == 1">
                                  <div class="pagestatuscontent fade-in-out"><span class="label bg-success" >Active</span></div>
                                  <div class="checkstatuscontent">
                                    <label class="i-switch bg-info m-t-xs m-r">
                                      <input type="checkbox" checked="" ng-click="setstatus(mem.status,mem.centerid,searchtext,mem.centerslugs)">
                                      <i></i>
                                    </label>
                                    
                                  </div>
                                  <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.centerslugs"><i class="fa fa-check"></i></spand></div>
                                </td>
                                <td  ng-if="mem.status == 0">
                                  <div class="pagestatuscontent fade-in-out"><span class="label bg-danger">Deactivated</span></div>
                                  <div class="checkstatuscontent">
                                    <label class="i-switch bg-info m-t-xs m-r">
                                      <input type="checkbox" ng-click="setstatus(mem.status,mem.centerid,searchtext,mem.centerslugs)">
                                      <i></i>
                                    </label>
                                   
                                  </div>
                                   <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.centerslugs"><i class="fa fa-check"></i></spand></div>
                                </td>
                                <td>
                                  {[{ mem.datecreated }]}
                                </td>
                                <td>
                                    <a href="" ng-click="managecenter(mem.centerid)"> <span class="label bg-info">Manage</span></a>
                                    <?php if ($role == 'Administrator') { ?>
                                    <a href="" ng-click="editcenter(mem.centerid)"><span class="label bg-warning" >Edit</span></a>
                                    <a href="" ng-click="deletecenter(mem.centerid)"> <span class="label bg-danger">Delete</span></a>
                                    <?php } ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

              </div>

          </div>
        </div>

      </div>

      <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-center bg-light lter">
              <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </footer>
        </div>
      </div>

  </div>
  <div class="col w-md w-auto-xs bg-light dk bg-auto b-l hide" id="aside">
  <div class="wrapper">

   <div class="padder-md">      
      <!-- streamline -->
      <div class="m-b text-md">Recent Activity</div>
      <div class="streamline b-l m-b">
        <div class="sl-item">
          <div class="m-l">
            <div class="text-muted">2 minutes ago</div>
            <p><a href class="text-info">God</a> is now following you.</p>
          </div>
        </div>
        <div class="sl-item b-success b-l">
          <div class="m-l">
            <div class="text-muted">11:30</div>
            <p>Join comference</p>
          </div>
        </div>
        <div class="sl-item b-danger b-l">
          <div class="m-l">
            <div class="text-muted">10:30</div>
            <p>Call to customer <a href class="text-info">Jacob</a> and discuss the detail.</p>
          </div>
        </div>
        <div class="sl-item b-primary b-l">
          <div class="m-l">
            <div class="text-muted">Wed, 25 Mar</div>
            <p>Finished task <a href class="text-info">Testing</a>.</p>
          </div>
        </div>
        <div class="sl-item b-warning b-l">
          <div class="m-l">
            <div class="text-muted">Thu, 10 Mar</div>
            <p>Trip to the moon</p>
          </div>
        </div>
        <div class="sl-item b-info b-l">
          <div class="m-l">
            <div class="text-muted">Sat, 5 Mar</div>
            <p>Prepare for presentation</p>
          </div>
        </div>
        <div class="sl-item b-l">
          <div class="m-l">
            <div class="text-muted">Sun, 11 Feb</div>
            <p><a href class="text-info">Jessi</a> assign you a task <a href class="text-info">Mockup Design</a>.</p>
          </div>
        </div>
        <div class="sl-item b-l">
          <div class="m-l">
            <div class="text-muted">Thu, 17 Jan</div>
            <p>Follow up to close deal</p>
          </div>
        </div>
      </div>
      <!-- / streamline -->
    </div>

  </div>
</div>
</div>
</fieldset>

