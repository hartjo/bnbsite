<script type="text/ng-template" id="deleteStory.html">
  <div ng-include="'/be/tpl/deleteStory.html'"></div>
</script>
<script type="text/ng-template" id="tpl_okay">
  <div ng-include="'/be/tpl/okay.html'"></div>
</script>

<form method="POST" name="viewstoryform" ng-submit="save(story, file)" class="form-validation ng-pristine ng-invalid ng-invalid-required">

<div class="bg-info lter b-b wrapper-md">

	  <span class="m-n font-thin h3">View Story</span>
	  <a id="top"></a>

	  <span class="pull-right">
	  <?php if($username['pi_userrole']!="Center Manager"){ ?>
	  	<a ui-sref="managestories" class="btn btn-md btn-default" title="Back"><i class="fa fa-mail-reply"></i></a><?php } ?>
	  	<a class="btn btn-md btn-danger" ng-click="delete(story.id)" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>
	  	<button class="btn btn-md btn-success" ng-disabled="viewstoryform.details.$invalid" title="Save"><i class="fa fa-floppy-o"></i></button>
	  </span>

</div>
<div class="container-fluid wrapper-md">
	<div class="row">
		<div class="col-sm-8">
			<div class="panel panel-info">
				<div class="panel-heading">
					<strong>SUCCESS STORY</strong>
				</div>
				<div class="panel-body">
					<div class="col-sm-4 b-b">
						<div class="form-group">
							<center>
							<label for="profile_pic" style="cursor:pointer"><i class="glyphicon glyphicon-edit"></i> File Upload</label>

							<img id="blah" src="<?php echo $this->config->application->amazonlink . '/uploads/testimonialimages/';?>{[{story.photo}]}" err-SRC="/img/blue-rose.jpg" class="img-responsive" alt="profile pricture" style="border:3px solid #ccc; box-shadow:0 2px 5px #333">

							<!-- <img  src="/img/default_profile_pic.jpg" alt="your image" /> -->

							<input id="profile_pic" ng-model="file" type="file" data-icon="false" class="form-control" accept="image/*" ngf-select ngf-allow-dir="true" onchange="readURL(this);" >
							</center>
						</div>
					</div>

					<div class="col-sm-8">
						<div class="form-group">
							<label class="font-bold">Name</label>
								<div class="well b bg-light lter wrapper-sm">
									<input type="text" class="form-control" ng-model="story.author" required />
								</div>
						</div>
						<div class="form-group">
							<label class="font-bold">Email</label>
								<div class="well b bg-light lter wrapper-sm">
									<input type="email" class="form-control" ng-model="story.email" required />
								</div>
						</div>
						<div class="form-group">
							<label class="font-bold">Age</label> <br>
								<div class="well b bg-light lter wrapper-sm">
									<input type="text" class="form-control" ng-model="story.age" pattern="\d{0,3}" maxlength="3"  />
								</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="line line-dashed b-b line-lg pull-in centered"></div>
					</div>

					<div class="col-sm-12">
						<div class="form-group">
							<label class="font-bold">State</label> <br>
							<div class="well b bg-light lter wrapper-sm">
							<input type="text" class="form-control" value="{[{story.statename}]} - {[{story.state}]}"  disabled="" />
							</div>
						</div>
					</div>


					<div class="col-sm-12">
						<div class="line line-dashed b-b line-lg pull-in centered"></div>
					</div>

					<div class="col-sm-12">
						<div class="form-group">
							<label class="font-bold">Center</label> <br>
							<div class="well b bg-light lter wrapper-sm">

								<div ui-module="select2">
									<select ui-select2 class="form-control" ng-model="story.center" required>
										<option ng-repeat="center in centers" ng-value="center.centerid">{[{center.centertitle}]}</option>
									</select>
								</div>
								
							</div>
						</div>

						<div class="form-group">
							<label class="font-bold">Subject</label> <br>
							<div class="well b bg-light lter wrapper-sm">
								<input type="text" class="form-control" ng-model="story.subject" required/>
							</div>
						</div>

						<div class="form-group">
							<label class="font-bold">Story</label>
							<div class="well b bg-light lter wrapper-sm">
								<textarea class="ck-editor" name="details" ng-model="story.details" rows="8" minlength="200" required>{[{story.details}]}</textarea>
							</div>
						</div>
					</div>
				</div>
			</div>

			<?php if($username['pi_userrole'] == 'Administrator' || $username['pi_userrole'] == 'Editor' ) { ?>
			<div class="panel panel-info">
				<div class="panel-heading">
					<strong>Workshop Titles</strong>
				</div>
				<div class="panel-body">
					<!-- <form name="relatedform"> -->
						<span ng-repeat="title in workshoptitles" ng-model="title" class="btn m-b-xs btn-sm btn-default btn-rounded btn-addon margin_right_5px" ng-click="addrelated(title)"><i class="fa fa-plus pull-right"></i>{[{title}]}</span>
					<!-- </form> -->
				</div>
			</div>
			
			<div class="panel panel-info">
				<div class="panel-heading">
					<strong>Related Workshop</strong>
				</div>
				<div class="panel-body">
					<!-- <form name="relatedform"> -->
						<span ng-repeat="title in related track by $index" ng-hide="empty" ng-model="titles" class="btn m-b-xs btn-sm btn-success btn-rounded btn-addon margin_right_5px" ng-click="removerelated(title, related)"><i class="fa fa-minus pull-right"></i>{[{title}]}</span>
						<span ng-show="empty">Empty</span>
<!-- 					</form> -->
				</div>
			</div>
			<?php } ?>

		</div>


		<div class="col-sm-4">
			<input type="hidden" ng-model="story.id"/>
			<div class="panel panel-info">
				<div class="panel-heading">
					<strong>Manage</strong>

					<label class="font-bold pull-right bg-light">
						Optimized 
						(
						<i class="fa fa-check text-success" ng-show="optimized"></i>
						<i class="fa fa-times text-danger" ng-hide="optimized"></i>
						)
					</label>
				</div>
				<div class="panel-body">
					<?php if ($username['pi_userrole'] == 'Administrator') { ?> 
					<div class="form-group">
						<label class="font-bold">Published</label> <br>
						<div class="well bg-light wrapper-sm">
							<label class="label bg-danger check_align">No</label>
							<label class="i-switch i-switch-md bg-info m-t-xs m-r check_align_checkbox">
								<input type="checkbox" id="switch" checked="" ng-true-value="'1'" ng-false-value="'2'" ng-model="story.status" >
								<i></i>
							</label>
							<label class="label bg-info check_align" for="switch == 1">Yes</label>
						</div>
						<!-- <label ng-if="optimized==false" class="text-danger">*Publish button is disabled if not optimized.</label> -->
					</div>
					<?php } ?>

					<div class="form-group">
						<label class="font-bold">Confirm by Center</label> <br>
						<div class="well bg-light wrapper-sm">
							<label class="label bg-danger check_align">No</label>
							<label class="i-switch i-switch-md bg-info m-t-xs m-r check_align_checkbox">
                  <input type="checkbox" checked="" ng-true-value="'1'" ng-false-value="'0'" ng-model="story.center_status" >
                  <i></i>
                </label >
              <label class="label bg-info check_align">Yes</label>
             </div>
					</div>

					<?php if($username['pi_userrole'] == 'Administrator' || $username['pi_userrole'] == 'Editor') { ?>
					<div class="form-group">
						<label class="font-bold">Spotlight</label> <br>
						<div class="well bg-light wrapper-sm">
							<label class="label bg-danger check_align">No</label>
							<label class="i-switch i-switch-md bg-primary m-t-xs m-r check_align_checkbox">
								<input type="checkbox" checked="" ng-true-value="'1'" ng-false-value="'0'" ng-model="story.spotlight" >
								<i></i>
							</label >
							<label class="label bg-info check_align">Yes</label>
						</div>
					</div>
					<?php } ?>

					<div class="form-group">
						<label class="font-bold">Date Submitted</label> <br>
						<div class="well bg-light wrapper-sm">
							<div class="input-group w-md">
								<span class="input-group-btn">
									<input id="date" name="date" class="form-control" datepicker-popup="yyyy-MM-dd" ng-model="story.date_submitted" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled datepicker-popup>
									<button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
								</span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="font-bold">Date Published</label> <br>
						<div class="well bg-light wrapper-sm">
							<div class="input-group w-md">
								<span class="input-group-btn">
									<input id="date" name="date" class="form-control" datepicker-popup="yyyy-MM-dd" ng-model="story.date_published" is-open="opened1" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled datepicker-popup>
									<button type="button" class="btn btn-default" ng-click="open1($event)"><i class="glyphicon glyphicon-calendar"></i></button>
								</span>
							</div>
						</div>
					</div>
<!-- 
					<div class="form-group">
						<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
					</div> -->
				</div>
			</div>
			<?php if($username['pi_userrole'] == 'Administrator' || $username['pi_userrole'] == 'Editor') { ?>
			<div class="panel panel-info">
				<div class="panel-heading font-bold">
					Meta
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label>Title</label>
						<input type="text" class="input-md form-control" placeholder="Meta Title"  ng-model="story.metatitle">
					</div>
					<div class="line line-lg"></div>

					<div class="form-group">
						<label>Description</label>
						<textarea class="form-control resize_vert" ng-model="story.metadesc" rows="6" placeholder="Meta Description"></textarea>
					</div>
					<div class="line line-lg"></div>
				</div>
			</div>
</form>
<form name="metaform" class="form-validation ng-pristine ng-invalid ng-invalid-required">
			<div class="panel panel-info">
				<div class="panel-heading font-bold">Meta Tags</div>
				<div class="panel-body">
					<div class="input-group">							
						<input type="text" class="input-md form-control" placeholder="Meta Tags"  ng-model="metatag" minlegth="1" required>
						<span class="input-group-btn">
							<button type="button" class="btn btn-md btn-info" ng-disabled="metaform.$invalid" ng-click="addmetatag(metatag, story.id)">Add</button>
						</span>		
					</div>
					<span class="help-block m-b-none">Use comma (,) for multiple meta tags. E.g. (focus, strength, peace)</span>
					<div class="line line-lg"></div>
					<div class="form-group" ng-show="empty_tag">
						<span ng-repeat="metatag in metatags track by $index" 
							class="btn m-b-xs btn-sm btn-primary btn-rounded btn-addon margin_right_5px"
							ng-model="metatags"  
							ng-click="removemetatag(metatag, metatags)">

							<i class="fa fa-minus pull-right"></i>{[{metatag}]}
						</span>
					</div>
</form>
				</div>
			</div>
			<?php } ?>
		</div>

	</div>
</div>
<div class="back_chrono" ng-show="processing">
  <center>
  <div class="wobblebar-loader">
    Loading…
  </div>
  </center>
</div>

