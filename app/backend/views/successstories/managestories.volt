<script type="text/ng-template" id="viewStory.html">
  <div ng-include="'/be/tpl/manageStory.html'"></div>
</script>
<script type="text/ng-template" id="deleteStory.html">
  <div ng-include="'/be/tpl/deleteStory.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Manage Stories</h1>
  <a id="top"></a>
</div>

<div class="container-fluid">
	<div class="row wrapper-md">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading font-bold">
					Stories List
				</div>
				<div class="panel-body">
					<div class="row wrapper-md">
						<div class="col-sm-5">
							<div class="input-group">
								<span class="input-group-btn">
									<button class="btn btn-sm btn-default" type="button" ng-click="clearsearch()">Clear</button>
								</span>
								<input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
								<span class="input-group-btn">
									<button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
								</span>
							</div>
							<div class="line line-dashed b-b line-lg"></div>
							<div class="input-group">
								<select ng-model="searchtext2" class="form-control input-sm ng-pristine ng-valid ng-touched" ng-change="search(searchtext2)">
	                              <option value="" style="display:none">Filter</option>
	                              <option value="null">List All</option>
	                              <option ng-repeat="list in centerdatalist.data" value="{[{ list.centertitle }]}">{[{ list.centertitle }]}</option>
	                            </select>
							</div>
						</div>
					</div>
					<div class="row wrapper-md">
						<div class="table-responsive">
							<table class="table table-striped b-t b-light">
								<thead>
									<tr>
										<th>id</th>
										<th>Status</th>
										<th>Author</th>
										<th>State</th>
										<th>Center</th>
										<th>Subject</th>
										<th>Date Created</th>
										<th>Date Published</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="story in stories.data">
										<!-- <td>{[{ CurrentPage * 10 - 10 + 1 + $index }]}</td> -->
										<td ng-bind="story.ssid"></td>
										<td>
											<span class="label bg-success" ng-show="story.status == 0">Unpublished</span>
											<span class="label bg-primary" ng-show="story.status == 2">Reviewed</span>
											<span class="label bg-info" ng-show="story.status == 1 && story.spotlight == 0">Published</span>
											<span class="label bg-info" ng-show="story.status == 1 && story.spotlight == 1">Published <i class="icon-star"></i></span>
										</td>
										<td>{[{story.author}]}</td>
										<td>{[{story.centerstate}]}</td>
										<td ng-if="story.centertitle != null">{[{story.centertitle}]}</td>
										<td ng-if="story.centertitle == null">{[{story.center}]}</td>
										<td>{[{story.subject}]}</td>
										<td>{[{story.date_submitted}]}</td>
										<td>{[{story.date_published}]}</td>
										<td>
											<a class="label bg-info" ng-click="viewStory(story.id)">Manage</a>
											<a class="label bg-danger" ng-click="deleteStory(story.id)">Delete</a>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<footer class="panel-footer text-center bg-light lter">
		          <pagination total-items="TotalItems" ng-model="CurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(CurrentPage)"></pagination>
		        </footer>
			</div>
		</div>
	</div>
</div>