 <div class="container w-xxl w-auto-xs">
  <a href class="navbar-brand block m-t">Body and Brain</a>
  <div class="m-b-lg">
    <div class="wrapper text-center">

    </div>

    <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="send(cpassword)" name="formpassword" id="formpassword">
      <h3 style="text-align:center;">Change Password</h3>
      <div class="list-group list-group-sm">
        <div class="list-group-item">
          <input type="password" id="oldpassword" class="form-control" ng-model="cpassword.oldpassword" required="required" placeholder="Old Password">
        </div>
        <div class="list-group-item">
          <input type="password" id="password" class="form-control" 
            ng-model="cpassword.password" 
            required="required" 
            placeholder="Password" 
            ng-change="checkpassword(cpassword.password,cpassword.repassword)" 
            ng-pattern="/^(?=.*^[a-zA-Z])(?=.*[a-z])(?=.*[A-Z])(?=.{8,})(?=.*\d)(?=.*(_|[^\w])).+$/" /> <!-- neil -->
        </div>
        <div class="list-group-item">
          <input type="password" id="repassword" class="form-control" 
            ng-model="cpassword.repassword" 
            required="required" 
            placeholder="Re-Type Password" 
            ng-change="checkpassword(cpassword.password,cpassword.repassword)"
            ng-pattern="/^(?=.*^[a-zA-Z])(?=.*[a-z])(?=.*[A-Z])(?=.{8,})(?=.*\d)(?=.*(_|[^\w])).+$/" /> <!-- neil -->
        </div>
        <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
        <div ng-show="IsMatch" style="text-align:center;color:red;">{[{errormsg}]}</div>
        <div class="list-group-item">
          <em class="text-muted">(New Password must start with an alphabeth. Combination of at least 1 small letter, 1 capital letter, 1 number and 1 special character . Minimum length (8)</em>
        </div>
      </div>
      <button type="submit" class="btn btn-lg btn-primary btn-block btn-success" ng-disabled='formpassword.$invalid'>Change Password</button>
      <div class="line line-dashed"></div>
    </form>
    <p class="text-center"><small>Dont share your password to anyone.</small></p>
  </div>
</div>