{{ content() }}
<div id="Scrollup"></div>
<script type="text/ng-template" id="success.html">
  <div ng-include="'/be/tpl/success.html'"></div>
</script>


<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create User</h1>
  <a id="top"></a>
</div>
<div>
  <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="submitData(user,files)" name="form">
    <fieldset ng-disabled="isSaving">
      <div class="wrapper-md">
        <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index, type='default')">{[{ alert.msg }]}</alert>
        <alert ng-repeat="alert in imagealert" type="{[{alert.type }]}" close="closeAlert($index, type='image')">{[{ alert.msg }]}</alert>
        <div class="panel panel-default">
        <div class="panel-body">
        <div class="col-md-6">
          <div class="panel-heading font-bold">Account Information</div>
            <div class="form-group">
              <label class="col-sm-3 control-label">Username</label>
              <div class="col-sm-9">
                <input type="text"  id="" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.username" ng-change="chkusername(user.username)" ng-minlength="6" maxlength="50" ng-pattern="/^[a-zA-Z0-9-@._]*$/" required="required" >
                <div class="popOver" ng-show="usrname">
                  Username is already taken.
                  <span class="pop-triangle"></span>
                </div>
                <em class="text-muted">(allow 'a-zA-Z0-9', 6-50 length)</em>
              </div>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>
            <div class="form-group">
              <label class="col-sm-3 control-label">Email Address</label>
              <div class="col-sm-9">
                <input type="email" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.email" ng-change="chkemail(user.email)" required="required" >
                <div class="popOver" ng-show="usremail">
                    Email is already taken.
                    <span class="pop-triangle"></span>
                </div>
              </div>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>
            <div class="form-group">
              <label class="col-sm-3 control-label">Password</label>
              <div class="col-sm-9">
                <input type="password" id="password" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.password" 
                          ng-change="confirmpass(user.conpass)" 
                          ng-minlength="8" 
                          ng-pattern="/^(?=.*^[a-zA-Z])(?=.*[a-z])(?=.*[A-Z])(?=.{8,})(?=.*\d)(?=.*(_|[^\w])).+$/"
                          required="required">
                <em class="text-muted">(New Password must start with an alphabeth. Combination of at least 1 small letter, 1 capital letter, 1 number and 1 special character . Minimum length (8)</em>
              </div>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>
            <div class="form-group">
              <label class="col-sm-3 control-label">Confirm Password</label>
              <div class="col-sm-9">
                <input type="password" id="conpass" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" 
                  ng-model="user.conpass" 
                  ng-change="confirmpass(user.conpass)" 
                  compare-to="user.password" 
                  required="required" 
                  ui-validate=" '$value==user.password' " 
                  ui-validate-watch=" 'user.password' "
                  ng-pattern="/^(?=.*^[a-zA-Z])(?=.*[a-z])(?=.*[A-Z])(?=.{8,})(?=.*\d)(?=.*(_|[^\w])).+$/">
                <span ng-show='form.confirm_password.$error.validator'>Passwords dont</span>
                <!-- <span class="label md text-danger" ng-show="confpass">{[{confpass}]} <br/></span> -->
                 <div class="popOver" ng-show="confpass">
                    Please match your passwords
                    <span class="pop-triangle"></span>
                </div>

              </div>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>
            <div class="form-group" ng-init="user.userrole = 'Administrator' ">
              <label class="col-lg-3 control-label">User Role</label>
              <div class="col-sm-9">
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="userrole" value="Administrator" ng-model="user.userrole" required="required">
                    <i></i>
                    [ Administrator ]
                  </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="userrole" value="Customer Service" ng-model="user.userrole" required="required">
                    <i></i>
                    [ Customer Service ]
                  </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="userrole" value="Editor" ng-model="user.userrole" required="required">
                    <i></i>
                    [ Editor ]
                  </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in" ng-hide="hideregion"></div>
                <div class="radio" ng-hide="hideregion">
                  <label class="i-checks">
                    <input type="radio" name="userrole" value="Region Manager" ng-model="user.userrole" required="required">
                    <i></i>
                    [ Region manager ]
                  </label>
                </div>
                <div class="padding_left_27px form-group" ng-if="user.userrole == 'Region Manager' " >
                  <select ui-jq="chosen" class="w-md" ng-model="user.region" required>
                    <optgroup label="Choose your Region">
                      <option ng-repeat="region in availableregion" value="{[{region.regionid}]}">{[{region.regionname}]}</option>
                    </optgroup>
                  </select>
                </div>

                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="radio" ng-hide="hidecenter">
                  <label class="i-checks">
                    <input type="radio" name="userrole" value="Center Manager" ng-model="user.userrole" required="required">
                    <i></i>
                    [ Center Manager ]
                  </label>
                </div>
                <div class="padding_left_27px form-group" ng-if="user.userrole == 'Center Manager' ">
                  <select ui-jq="chosen" class="w-md" ng-model="user.center" required>
                    <optgroup label="Choose your Center">
                      <option ng-repeat="center in availablecenter" value="{[{center.centerid}]}">{[{center.centertitle}]}</option>
                    </optgroup>
                  </select>
                </div>
              </div> 
            </div>
        </div> <!-- end of col-md-6 -->
        <div class="col-md-6">
          <div class="panel-heading font-bold">
            User Profile

          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Profile Picture</label>
              <div class="col-sm-9">
                <input id="profile_pic" ng-model="file" type="file" data-icon="false" class="form-control" accept="image/*" ngf-select ngf-allow-dir="true" onchange="readURL(this);">
                
                <img id="blah" src="/img/default_profile_pic.jpg" alt="your image" />
                <label for="profile_pic" class="label_profile_pic">Change Picture</label>
              </div>
            </div>

            <div class="line line-dashed b-b line-lg pull-in"></div>

            <div class="form-group">
              <label class="col-sm-3 control-label">First Name</label>
              <div class="col-sm-9">
                <input type="text" id="" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.fname" required="required" > 
              </div>
            </div>

            <div class="line line-dashed b-b line-lg pull-in"></div>

            <div class="form-group">
              <label class="col-sm-3 control-label">Last Name</label>
              <div class="col-sm-9">
                <input type="text" id="" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.lname" required="required" >
              </div>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>
            <div class="form-group">
              <label class="col-sm-3 control-label"> Birthday</label>
                <div class="col-sm-9">
                <div class="input-group w-md">
                  <span class="input-group-btn">
                    <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="user.bday" is-open="opened" datepicker-options="dateOptions" close-text="Close" type="text" disabled>
                    <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                  </span>
                </div>
                </div>
            </div>

            <div class="line line-dashed b-b line-lg pull-in"></div>

            <div class="form-group">
              <label class="col-sm-3 control-label">Contact Number</label>
              <div class="col-sm-9">
                <input type="text" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern phone_us" ng-model="user.contact" required="required" placeholder="(123) 123-1234" minlength=14 >
              </div>
            </div>

            <div class="line line-dashed b-b line-lg pull-in"></div>

            <div class="form-group" ng-init="user.gender='Male'">
              <label class="col-sm-3 control-label">Gender</label>
              <div class="col-sm-9">
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="gender" value="Male" ng-model="user.gender" required="required">
                    <i></i>
                    Male
                  </label>
                </div>
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="gender" value="Female" ng-model="user.gender" required="required">
                    <i></i>
                    Female
                  </label>
                </div>
              </div>
            </div> 

             <div class="line line-dashed b-b line-lg pull-in"></div>

            <div class="form-group">
              <label class="col-sm-3 control-label">Status</label>
              <div class="col-sm-9">
                <label class="i-switch i-switch-md bg-info m-t-xs m-r">
                  <input type="checkbox" checked="" ng-true-value="'1'" ng-false-value="'0'" ng-model="user.status" >
                  <i></i>
                </label>
              </div>
            </div>
          </div>
          </div> <!-- end of col-md-6 -->
          <div class="col-md-12">
            <div class="line b-b line-lg pull-in"></div>
            <div class="form-group">
                <div class="col-md-2 col-md-offset-10">
                  <button type="button" class="btn btn-default" ng-click="reset()">Cancel</button>
                  <button type="submit" class="btn btn-success" ng-disabled="form.$invalid || form.$pending || usrname==true || usremail==true"  scroll-to="Scrollup">Submit</button>

                </div>
              </div>
          </div>
        </div> <!-- panel body -->
        </div>
      </div>
      </fieldset>
  </form>
</div>

<div class="back_chrono" ng-show="processing">
  <center>
  <div class="wobblebar-loader">
    Loading…
  </div>
  </center>
</div>

<script>
 function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(200)
                        .height(200);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
