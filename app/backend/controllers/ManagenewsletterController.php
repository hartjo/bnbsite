<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class ManagenewsletterController extends ControllerBase {
	public function managesubscribersAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function createnewsletterAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function managenewsletterAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function editnewsletterAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function editnewspdfAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
}