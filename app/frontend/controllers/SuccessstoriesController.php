<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class SuccessstoriesController extends ControllerBase
{
    public function indexAction($offset) {
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');

        $this->view->metatitle = "Body & Brain yoga Success Stories: Stress, Pain, Flexibility, Headache, etc.";
        $this->view->metatags = "Stress, Pain, Flexibility, Headache";
        $this->view->metadesc = "The benefits of Body & Brain yoga are various. Many Body & Brain yoga practitioners are saying that they got lots of physical and mental benefits.";

        $this->view->leftsidebarname = "";

        $gotoroute = $this->config->application->ApiURL.'/success-stories/indexpage/'.$offset;
        $curl = curl_init($gotoroute);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
                    $info = curl_getinfo($curl);
                    curl_close($curl);
                    die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        if($decoded == true) {
          $this->view->spotlights = $decoded->spotlights;  //SPOTLIGHT LIMIT 10
          $this->view->totalstories = $decoded->totalstories; //TOTALSTORIES TOTAL NUMBERS OVERALL
          $this->view->stories = $decoded->stories; //STORIES LIST WITH PAGINATION
          $this->view->mainnews = $decoded->mainnews; //SIDEBAR MAIN NEWS
          //show total news category item PAGINATION BAR
          $itemperpage = 10;
          $this->view->page = $offset;
          $this->view->totalpage = ceil($decoded->totalpage / $itemperpage);
        } else {
          $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
          $this->view->pick("index/route404");
        }
    }
    public function tagAction($tag, $offset) {
        $this->view->tag = $tag;
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');

        $this->view->metatitle = "Body & Brain yoga Success Stories: Stress, Pain, Flexibility, Headache, etc.";
        $this->view->metatags = "Stress, Pain, Flexibility, Headache";
        $this->view->metadesc = "The benefits of Body & Brain yoga are various. Many Body & Brain yoga practitioners are saying that they got lots of physical and mental benefits.";

        $this->view->leftsidebarname = "";

        $gotoroute = $this->config->application->ApiURL.'/success-stories/tagpage/'.$tag.'/'.$offset;
        $curl = curl_init($gotoroute);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
                    $info = curl_getinfo($curl);
                    curl_close($curl);
                    die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->spotlights = $decoded->spotlights;  //SPOTLIGHT LIMIT 10
        $this->view->totalstories = $decoded->totalstories; //TOTALSTORIES TOTAL NUMBERS OVERALL
        $this->view->stories = $decoded->stories; //STORIES LIST WITH PAGINATION

        //show total news category item PAGINATION BAR
        $itemperpage = 10;
        $this->view->page = $offset;
        $this->view->totalpage = ceil($decoded->totalpage / $itemperpage);

        $this->view->popular_features = $decoded->mainnews; //SIDEBAR MAIN NEWS
    }
    public function writeAction()
    {
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
        $this->view->leftsidebarname = "";

    }
    public function viewAction($storyid) {
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');

    	//GENERAL REQUEST
        $this->view->leftsidebarname = "";
        $gotoroute = $this->config->application->ApiURL. '/fe/success-stories/view/'.$storyid;
        $curl = curl_init($gotoroute);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        if($decoded->story == true) {
          $this->view->viewstory = $decoded->story;
          $this->view->popular_features = $decoded->mainnews;
          $this->view->metatitle = 'Body & Brain yoga | Success Story: '.$decoded->story->subject;
          $this->view->metatags = $decoded->story->metatags;
          $this->view->metadesc = $decoded->story->metadesc;
        } else {
          $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
          $this->view->pick("index/route404");
        }
    }

    public function centerstoryAction($state, $center) {
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
        $this->view->leftsidebarname = "";
        $this->view->state = $state;
        $this->view->center = $center;
    }

}
