<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class NewsletterController extends ControllerBase
{
    public function enewsletterAction(){
        //list news by Category
        $this->view->leftsidebarname = "";
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');

        $this->view->metatitle = "e-newsletter | Yoga archive, Tai Chi, Meditation, Qigong";
        $this->view->metatags = "Yoga archive, Tai Chi, Meditation, Qigong";
        $this->view->metadesc = "E-newsletter archive of dahnyoga.com. Have the best of dahnyoga.com delivered directly to your inbox.";

    }
    public function pdfnewsletterAction(){
        //list news by Category
        $this->view->leftsidebarname = "";
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
        $this->view->metatitle = "Body & Brain yoga monthly newsletter | Yoga tips, Tai Chi, Qigong, Meditation";
        $this->view->metatags = "Yoga archive, Tai Chi, Meditation, Qigong";
        $this->view->metadesc = "Find an electronic archive of current and past issues of Body & Brain yoga's monthly newsletter in a downloadable PDF format.";

    }
    public function enewslettershowAction($id){
        //list news by Category
        $this->view->leftsidebarname = "";
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');

        $service_url = $this->config->application->ApiURL.'/fe/enewslettershow/'.$id;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
        	$info = curl_getinfo($curl);
        	curl_close($curl);
        	die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->content = $decoded;

        $this->view->metatitle = "e-newsletter | Yoga archive, Tai Chi, Meditation, Qigong";
        $this->view->metatags = "Yoga archive, Tai Chi, Meditation, Qigong";
        $this->view->metadesc = "E-newsletter archive of dahnyoga.com. Have the best of dahnyoga.com delivered directly to your inbox.";  
    }

}