<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;


class LayoutsController extends ControllerBase
{
    public function mainAction()
    {
    	$this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
    	$this->view->metatitle = "Body & Brain: Yoga classes combining Tai chi, Meditation";
    	$this->view->metadesc = "Body &amp; Brain offers dynamic classes in Korean style power yoga, tai chi and meditation. Beginners are welcome.";
    }
}