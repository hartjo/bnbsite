<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;


class MaintenanceController extends ControllerBase
{
	public function indexAction()
	{
		$this->view->script_google = $this->curl('/settings/script');
		$this->view->logoimage = $this->curl('/settings/managesettings');
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}

	public function maintenanceAction() { 
		return $this->response->redirect($this->config->application->baseURL . '../maintenance');

	}
	
}
