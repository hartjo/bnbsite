<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;


class RegistrationController extends ControllerBase
{
    public function indexAction()
    {
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
        $this->angularLoader(array(
            'registration' => 'fe/scripts/controllers/registration.js',
            'birthdayfactory' => 'fe/scripts/factory/birthday.js',
            'userfactory' => 'fe/scripts/factory/user.js',
            'validation' => 'fe/scripts/directives/validations.js',
            'passstrength' => 'vendors/pass-strength/src/strength.min.js'
        ));
    }
    public function successAction(){
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function activationAction($code){
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
        $params = array(
            "code" => $code
        );
        $decoded = $this->httpPost($this->config->application->apiURL. '/user/activation',$params);
        $msg = json_decode($decoded);
        if(isset($msg->success)){
            echo $msg->success;
        }else{
            echo $msg->error;
        }
    }
}

