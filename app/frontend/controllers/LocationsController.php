<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class LocationsController extends ControllerBase
{
	public function indexAction() {
		$this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
		$this->view->leftsidebarname = "";
	}
}