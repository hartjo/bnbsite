<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class ControllerBase extends \Phalcon\Mvc\Controller
{
    protected function initialize()
    {
        $this->view->articles = "/bnb-buzz";
       $this->view->fbcapc = '';
       $this->view->banner = '';
       $this->view->titleslugs = '';
       $this->view->newsslugs = '';
        $this->createJsConfig();
        $clientip= $this->get_client_ip_server();
        $countvisit =$this->config->application->ApiURL.'/getip/visit/'. $clientip;

        $curl = curl_init($countvisit);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false)
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            var_dump(die('error occured during curl exec. Additional info: ' . var_export($info))) ;
        }
    }

    public  function get_client_ip_server() {

        if (isset($_SERVER)) {

            if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
                return $_SERVER["HTTP_X_FORWARDED_FOR"];

            if (isset($_SERVER["HTTP_CLIENT_IP"]))
                return $_SERVER["HTTP_CLIENT_IP"];

            return $_SERVER["REMOTE_ADDR"];
        }

        if (getenv('HTTP_X_FORWARDED_FOR'))
            return getenv('HTTP_X_FORWARDED_FOR');

        if (getenv('HTTP_CLIENT_IP'))
            return getenv('HTTP_CLIENT_IP');

        return getenv('REMOTE_ADDR');
    }
    public function angularLoader($ang){
        $modules = array();
        $scripts = '';
        foreach($ang as $key => $val){
            $scripts .= $this->tag->javascriptInclude($val);
            $modules[] = $key;
        }
        $this->view->modules = (!empty($modules) ? $modules : array());
        $this->view->otherjvascript = $scripts;
    }
    public function httpPost($url,$params)
    {
        $postData = '';
        //create name value pairs seperated by &
        foreach($params as $k => $v)
        {
            $postData .= $k . '='.$v.'&';
        }
        rtrim($postData, '&');

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

        $output=curl_exec($ch);

        curl_close($ch);
        return $output;
    }

    private function createJsConfig(){
        $script="app.constant('Config', {
// This is a generated Config
// Any changes you make in this file will be replaced
// Place you changes in app/config.php file \n";
            foreach( $this->config->application as $key=>$val){
                $script .= $key . ' : "' . $val .'",' . "\n";
            }
        $script .= "});";
        $fileName="../public/be/js/scripts/config.js";

        $exist =  file_get_contents($fileName);
        if($exist == $script){

        }else{
            file_put_contents($fileName, $script);
        }

    }
    public function curl($url){
        $service_url = $this->config->application->ApiURL.'/'.$url;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        return $decoded = json_decode($curl_response);

    }
}
