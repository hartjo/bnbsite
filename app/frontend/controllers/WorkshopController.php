<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class WorkshopController extends ControllerBase {
	public function indexAction($workshoptitle) {
		$this->view->leftsidebarname = "";
		$this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
	}
	public function pageAction($workshoptitle) {
		$this->view->leftsidebarname = "";
		$this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
	}
	public function workshopschedulesAction($titleslugs) {
		$this->view->leftsidebarname = "";
		$this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
		$this->view->metatitle = "Yoga Workshops, Events, Seminars Schedule | Body & Brain yoga";
		$this->view->metatags = "Workshops";
		$this->view->metadesc = "Search schedules of Body & Brain yoga workshops by any combination of workshop, location, and date.";

	}
	public function detailAction($workshopid) {
		$this->view->leftsidebarname = "";
		$gotoroute = $this->config->application->ApiURL.'/workshop/detail/' . $workshopid;
		$curl = curl_init($gotoroute);
		curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$curl_response = curl_exec($curl);
		if ($curl_response === false) {
		            $info = curl_getinfo($curl);
		            curl_close($curl);
		            die('error occured during curl exec. Additioanl info: ' . var_export($info));
		}
		curl_close($curl);
		$decoded = json_decode($curl_response);
		$this->view->workshop = $decoded->workshopprop;
		$this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
		$this->view->metatitle = "Workshop Details: Change Intro Workshop | Body & Brain yoga";
		$this->view->metatags = "Workshops";
		$this->view->metadesc = "Change Intro Workshop in Body & Brain yoga: Venue, schedule and miscellaneous information for you.";

	}
	public function registrationAction($workshopid) {
		$this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
		$this->view->leftsidebarname = "";
		$this->view->metatitle = "Registration: Body &amp; Brain yoga Workshops for Personal Development";
		$this->view->metadesc = "Sign up for Body &amp; Brain yoga workshops now and start your journey for personal development.";
	}
	public function overviewAction() {
		$this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
		$this->view->leftsidebarname = "";
		$this->view->metatitle = "Yoga Workshops for Personal Development | Body & Brain yoga";
		$this->view->metatags = "Workshops";
		$this->view->metadesc = "In addition to our regular Body & Brain yoga classes, we offer workshops that help you reach your personal potential.";
	}
}