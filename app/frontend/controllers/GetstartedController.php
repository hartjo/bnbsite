<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class GetstartedController extends ControllerBase
{
    public function indexAction()
    {

        $this->view->leftsidebarname = "";
        $this->view->metatitle = "Yoga classes, Tai Chi, Meditation, Qigong | How to Start";
        $this->view->metadesc = "Schedule a one-on-one consultation with an instructor. Learn about how to improve your breathing, balance and flexibility.";
    	$this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
    }

    public function starterspackageAction(){
        $this->view->leftsidebarname = "";
        $this->view->metatitle = "Yoga classes, Tai Chi, Meditation, Qigong | How to Start";
        $this->view->metadesc = "Schedule a one-on-one consultation with an instructor. Learn about how to improve your breathing, balance and flexibility.";
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
    }

    public function successAction()
    {
        $this->view->metatitle = "Yoga classes, Tai Chi, Meditation, Qigong | How to Start";
        $this->view->metadesc = "Schedule a one-on-one consultation with an instructor. Learn about how to improve your breathing, balance and flexibility.";
        
        $this->view->leftsidebarname = "";

        $returndata = explode('|', $_GET['cm']);
        $confirmationnumber = $_GET['tx'];
        $name = $returndata[0];
        $email = $returndata[1];
        $centerid = $returndata[2];
        $phone = $returndata[3];
        $hour = $returndata[4];
        $day = $returndata[5];
        $comment = $returndata[6];
        $qty = $returndata[7];
        $device = $returndata[8];
        $deviceos = $returndata[9];
        $devicebrowser = $returndata[10];
        $itemprice = $returndata[11];
        $sessionclass = $returndata[12];
        $sessiontype = $returndata[13];
        $moduletype = $returndata[14];
        $totalprice = $_GET['amt'];

        $service_url = $this->config->application->ApiURL.'/fe/center/getcenterdetails/'.$centerid;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->centeremail = $decoded->data->email;
        $this->view->centerphone = $decoded->data->phonenumber;
        $this->view->lat = $decoded->data->lat;
        $this->view->lon = $decoded->data->lon;
        $this->view->centertitle = $decoded->data->centertitle;
        $this->view->centeraddress = $decoded->data->centeraddress;
        $this->view->centerzip = $decoded->data->centerzip;

        $this->view->name = $name;
        $this->view->day = $day;
        $this->view->hour = $hour;
        $this->view->totalprice = $totalprice;
        $this->view->sessionclass = $sessionclass;
        $this->view->confirmationnumber = $confirmationnumber;

        $this->view->moduletype = $moduletype;

        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
    }

    public function sampleAction(){
      
    }

   
}

