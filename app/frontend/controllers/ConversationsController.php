<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class ConversationsController extends ControllerBase
{
	public function indexAction() {
		$this->view->leftsidebarname = "";
		$this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
	}

	public function myexperienceAction() {
		$this->view->leftsidebarname = "";
		$this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
	}

	public function communityAction() {
		$this->view->leftsidebarname = "";
		$this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->script_google = $this->curl('/settings/script');
	}
}