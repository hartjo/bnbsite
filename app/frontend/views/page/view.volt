<?php
if($logoimage->value1 == 1){
  header('Location: /../../maintenance/');
}
?>
<?php echo $this->getContent()?>

<?php $page = "/page/view"; ?>
<?php 
if($pagelayout == 1) {
 echo $body; 

} else {
	//IF THERES NO LEFTBAR
	if ($leftsidebartoggle == false) { ?>
		<aside id="sidebar-right">
		<?php if($item1 == true) { ?>
			<a href="/getstarted/starterspackage" class="add"><img src="/img/frontend/banner_getstarted_small.jpg"></a>
		<?php } else  { } ?>
		<?php if($item2 == true) { ?>
			<div class="col-4" ng-controller="pageCtrl">
					<div class="success_stories_container">
						<div class="success_stories_wrapper">
							<div class="success_stories padding_vert_s">
								<div class="frmtitle marg_lft suc_stor" >Success Stories</div>
								<div class="story_cont" ng-repeat="story in stories">
									<a href="/successstories/view/{[{story.ssid}]}/successstories/view/{[{story.ssid}]}/{[{story.subject | replaceslugs}]}">
									<!-- <div class="thumbnail_auto" style="background:url(<?php echo $this->config->application->amazonlink . '/uploads/testimonialimages/{[{story.photo}]}'; ?>);">
									</div> -->
									<img src="<?php echo $this->config->application->amazonlink . '/uploads/testimonialimages/{[{story.photo}]}'; ?>" 
										class="thumbnail_auto"
										err-SRC="/img/blue-rose.jpg"/>
									<div class="story_info">
										<span class="story_title size_18">{[{story.subject}]}</span> <br>
										<span class="story_add">{[{story.author}]},  {[{story.centercity}]} Center, ({[{story.centerstate}]})</span>
										<p class="story_msg auto_text"> {[{story.metadesc }]}</p>
									</div>
									</a>
								</div>
							</div>
						</div> 
					</div>
				</div>
			<div class="clearboth"></div>
		<?php } else { } ?>
		<?php if($item3 == true) { ?>

			<div class="col-4">
				  <div class="popularpost">
					<div class="frmtitle marg_lft pop_feat" >Popular Features</div>
			  <?php foreach($popularnews as $data) { ?>
					  <div class="post">
					  	<a href="<?php echo $articles; ?>/view/<?php echo $data->newsslugs;?>">
							<img class="post_pic img_thumbnail" src="<?php echo $this->config->application->amazonlink.'/uploads/newsimage/'.$data->banner; ?>" />
						</a>
						<div class="small-text">
							<a href="<?php echo $articles; ?>/view/<?php echo $data->newsslugs;?>">
								<?php  echo substr($data->description, 0, 50) . ".."; ?>
							</a>
						</div>
						<div class="clearboth"></div>
					  </div>
			  <?php } ?>
					<div class="clearboth"> </div>
				  </div>
				 </div>
		<?php } else { } ?>
		</aside>
		<div class="clearboth"></div>
		<section id="body-left" >
			<h5 class="h5title"><?php echo @$leftsidebarname; ?></h5>
			<h3 class="h3title"><?php echo $title; ?></h3>
			<?php echo $body; ?>
		</section>	
<?php
	} else if ($rightsidebartoggle == false) { ?> <!-- ******************** NO RIGHT SIDE BAR ************************** -->
		<section id="sidebar-left">
	      	<div id="sidebarmenu">
		      <ul>
		      <?php 
					$getginfo = $leftsidebar;
					foreach ($getginfo as $key => $value) { ?>
		        	  <li>
		        	  	<a href="<?php echo $pageslugs[$key];?>"> 
		        	  		<label class="dot"> ▪</label> <?php echo $getginfo[$key]->item;  ?>
		        	  	</a>
		        	  </li>
		      <?php } ?>  
		      </ul> 
		    </div>
		    <div id="newsletter">
		        <div class="frmtitle">e-Newsletter</div>
		        <form>
		          <div><input type="text" class="text" placeholder="E-mail Address" ></div>
		          <input type="button" class="send" value="" />
		        </form>
		        <div class="clearboth"></div>
		    </div>
	    </section>
		<section id="body-right" >
			<!-- <h5 class="h5title"><?php echo @$leftsidebarname; ?></h5> -->
	<?php 	if($leftsidebarname != 'Press') { 
				echo @$leftsidebarname; ?>
				<h3 class="h3title"><?php echo $title; ?></h3>
	<?php	} else { ?>
				<span class="inbl franchisingTitle"><?php echo $title; ?></span>
	<?php	} 	?>
			
			<?php echo $body; ?>
		</section>
		<div class="clearboth"></div>
				
<?php	
	} else { ?> <!-- ******************** 3 COLUMNS ******************************** -->	
		<section id="content">
	      <div id="sidebarmenu">
	      	<ul>
	      		<?php 
	      		$getginfo = $leftsidebar;
	      		foreach ($getginfo as $key => $value) { ?>
	      		<li id="<?php echo $pageslugs[$key]; ?>">
	      			<a href="<?php echo $pageslugs[$key];?>">  
	      				<label class="dot"> ▪ </label> 
	      				<?php echo $getginfo[$key]->item; ?>
	      			</a>
	      		</li>
	      		<?php } ?>  
	      	</ul> 
	      </div>
	      <div id="newsletter">
	        <div class="frmtitle">e-Newsletter</div>
	        <form action="//bodynbrain.us3.list-manage.com/subscribe/post?u=cfc22757143336e0cfcf90eef&amp;id=eef0458b00" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
	        	<input type="email" value="" name="EMAIL" class="required email text" id="mce-EMAIL" placeholder="E-mail Address">
	        	<input type="submit" value="" name="subscribe" id="mc-embedded-subscribe" class="send">
	        </form>
	        <div class="clearboth"></div>
	      </div>
	    </section>

		<section id="middle">
		  <h5 class="h5title">

		  	<?php if($leftsidebarname != 'Press' && $leftsidebarname != 'Miscellaneous' && $leftsidebarname != 'Workshop') {
		  		echo $leftsidebarname;
		  	} ?>
		  </h5>
		  <h3 class="h3title"><?php echo $title; ?></h3>
		  <?php echo $body; ?>
		</section>

		<div class="clearboth"></div>

		<aside id="sidebar-right">
			<div class="sidebar-container">
					<!-- <div class="col-4">
				      <div id="newsletter2">
				        <div class="frmtitle">e-Newsletter</div>
				        <form>
				          <div><input type="text" class="text" placeholder="E-mail Address" ></div>
				          <input type="button" class="send" value="" />
				        </form>
				        <div class="clearboth"></div>
				      </div>
			      	</div> -->
			<?php if($item1 == true) { ?>
				<div class="col-4">
					<a href="/getstarted/starterspackage" class="add"><img src="/img/frontend/banner_getstarted_small.jpg" id="starter-package"></a>
				</div>
			<?php } ?>

			<?php if($item2 == true) { ?>
				<div class="col-4" ng-controller="pageCtrl" ng-show="existing">
					<a href="/workshops/workshopschedules/initial-awakening">
					<div class="bg_red2 padding_m text_center">
						<h5>Initial Awakening</h5>
						<hr class="hr_thin">
						Signup now! <br>
						<span ng-if="sale == true">
							<strike>${[{initawakening.tuition}]}</strike> &#x27a0; <span class="font_24_px">${[{initawakening.sale}]}</span>
						</span>
						<span ng-if="sale == false">
							{[{initawakening.tuition}]}
						</span>
					</div>
					</a>

					<div class="bg_orangeish margin_top_5px padding_m">
						<b>Upcoming Schedule</b>
					</div>
					<div class="border_ltr_gray padding_m font_85">
						{[{date}]}
						<a href="/workshops/registration/{[{initawakening.workshopid}]}" class="red_text font_85 border_light_gray padding_s border_radius_s float_right margin_top-5px">Sign Up</a>
						<hr class="hr_thin2">
						<a href="/workshops/detail/{[{initawakening.workshopid}]}" class="auto_text">
						{[{initawakening.centertitle}]}, {[{initawakening.centerstate}]} <br>
						<span class="teal_text">{[{initawakening.venuename}]}</span> 
						</a>
						<br>
						
					</div>
				</div>
			<?php } ?>
			
			<?php if($item3 == true) { ?>
				<div class="col-4" ng-controller="pageCtrl">
					<div class="success_stories_container">
						<div class="success_stories_wrapper">
							<div class="success_stories padding_vert_s">
								<div class="frmtitle marg_lft suc_stor" >Success Stories</div>
								<div class="story_cont" ng-repeat="story in stories">
									<a href="/successstories/view/{[{story.ssid}]}/{[{story.subject | replaceslugs}]}">
									<!-- <div class="thumbnail_auto" style="background:url(<?php echo $this->config->application->amazonlink . '/uploads/testimonialimages/{[{story.photo}]}'; ?>);">
									</div> -->
									<img src="<?php echo $this->config->application->amazonlink . '/uploads/testimonialimages/{[{story.photo}]}'; ?>" 
										class="thumbnail_auto"
										err-SRC="/img/blue-rose.jpg"/>
									<div class="story_info">
										<span class="story_title size_18">{[{story.subject}]}</span> <br>
										<span class="story_add">{[{story.author}]},  {[{story.centercity}]} Center, ({[{story.centerstate}]})</span>
										<p class="story_msg auto_text"> {[{story.metadesc }]}</p>
									</div>
									</a>
								</div>
							</div>
						</div> 
					</div>
				</div>
			<?php } ?>

			<!-- RELATED ARTICLES HERE -->

		    <?php if($item5 == true) { ?>
				<div class="col-4">
				  <div class="popularpost">
					<div class="frmtitle marg_lft pop_feat" >Popular Features</div>
			  <?php foreach($popularnews as $data) { ?>
					  <div class="post">
					  	<a href="<?php echo $articles; ?>/view/<?php echo $data->newsslugs;?>">
							<img class="post_pic img_thumbnail" src="<?php echo $this->config->application->amazonlink.'/uploads/newsimage/'.$data->banner; ?>" />
						</a>
						<div class="small-text">
							<a href="<?php echo $articles; ?>/view/<?php echo $data->newsslugs;?>">
								<?php  echo substr($data->description, 0, 50) . ".."; ?>
							</a>
						</div>
						<div class="clearboth"></div>
					  </div>
			  <?php } ?>
					<div class="clearboth"> </div>
				  </div>
				 </div>
		    <?php } ?>
		    </div>
		</aside>
<?php 
	}
} ?>