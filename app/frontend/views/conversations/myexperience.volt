<?php
if($logoimage->value1 == 1){
  header('Location: /../../maintenance/');
}
?><div id="crumbs_cont">
  <ul id="crumbs">
    <li class="crumbsli"><a class="crumbs" href="/bnbsite">Home</a></li>
    <li class="crumbsli"><a class="crumbs" href="#">Conversations</a></li> 
    <li class="crumbsli"><a class="crumbs" href="#">My Experience</a></li> 
  </ul>
</div>
<div class="divseparetor"></div>
<div class="row">
	
	<div class="col s12">
		<img src="/img/frontend/community_visual01.jpg">
	</div>

</div>
<div class="divseparetor"></div>

<div class="row">
    <div class="col s12">
      	<div class="newsindexcontent">
	        <div class="newsindexnewsdata">
	        <h4 class="title">Conversation</h4>


		        <!-- DISCUSSION PLUGIN -->
		        <div class="view_discussion">
		        	<div id="disqus_thread"></div>
		        	<script type="text/javascript">
		        		/* * * CONFIGURATION VARIABLES * * */
		        		var disqus_shortname = 'bodynbrain';

		        		/* * * DON'T EDIT BELOW THIS LINE * * */
		        		(function() {
		        			var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
		        			dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
		        			(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
		        		})();
		        	</script>
		        	<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
		        </div> <!-- END OF DISCUSSION PLUGIN -->

	        </div> <!-- end of newsindexnewsdata -->

	        <div class="newsindexsidebar">
	          <a href="/getstarted/starterspackage"><img src="/img/frontend/banner_getstarted.jpg" style="width:100%;height:auto;"></a>
	          <div class="twitter_wrap">
	          	<h4 class="title">Community Conversations</h4>
	          	<p class="sub_title">Here you can post and read <br>messages on topics that interest, <br>intrigue, and relate to you.<br><a href="/conversations"><img src="/img/frontend/banner_btn.gif"></a></p>
	          	<div class="twitter_list" data-twttr-id="twttr-sandbox-0">
	          		<a class="twitter-timeline" href="https://twitter.com/bodynbrainyoga" data-widget-id="284465771520860161">Tweets by @bodynbrainyoga</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

	          	</div>
	          </div>
	        </div>


      	</div>
    </div>
</div>