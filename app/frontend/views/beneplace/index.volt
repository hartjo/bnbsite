<?php
if($logoimage->value1 == 1){
  header('Location: /../../maintenance/');
}
?><div ng-controller="BeneplaceCtrl">
	<div class="getstarted top" ng-show="firststep"  id="non-printable">
		<span class="title">Beneplace Packages</span>
		<!-- state class on -->
		<ul class="state">
			<li class="on"><p class="program">Choose Program</p></li>
			<li class="arrow"><img src="/img/frontend/ico_state_arrow.gif" alt=""></li>
			<li><p class="location">Find Location</p></li>
			<li class="arrow"><img src="/img/frontend/ico_state_arrow.gif" alt=""></li>
			<li><p class="schedule">Schedule Session &amp; Class</p></li>
			<li class="arrow"><img src="/img/frontend/ico_state_arrow.gif" alt=""></li>
			<li class="last"><p class="confirm">Confirm Your Appointment</p></li>
		</ul>

		<!-- choose program -->
		<div class="box_wrap" id="gototop">
			<div class="inner_wrap">
				<span class="sub_title">
					<p class="location_txt"></p>
					<p class="ready_txt">GET STARTED</p>
				</span>
				<div class="program_wrap">
					<div class="introsessioncontainer">
						<!-- An Introductory Session -->
						<div class="introduc" style="text-align:center">
							<span class="sub_title2">
								<span>1-on-1 Intro Session</span>
							</span>
							<span style="color:#444; font-size:14px; ">Online Special Rate</span>
							<p class="ng-cloak">${[{introonlinerate}]}.00 <span>(${[{introofflinerate}]} value)</span></p>
							<a href="" ng-click="selectsession('intro')" class="gbtn ng-cloak"><span>Sign Me Up!</span></a>
						</div>
						<p class="or">OR</p>

						<!-- A Trial Class -->
						<div class="trial" style="text-align:center">
							<h6 class="sub_title2"><span>1 Group Class +<br>1-on-1 Intro Session</span></h6>
							<span style="color:#444; font-size:14px; ">Online Special Rate</span>
							<p class="ng-cloak">${[{grouponlinerate}]}.00 <span>(${[{groupofflinerate}]} value)</span></p>
							<a href="" ng-click="selectsession('classintro')" class="gbtn ng-cloak"><span>Sign Me Up!</span></a>
						</div>
					</div>
					<p style="margin:50px 20px 0px 20px; line-height:1.5em">Intro Sessions are a 45-minute personal session where we check your flexibility, breathing, and balance to give you a holistic assessment and plan for your unique condition.</p>
					<p style="margin:20px 20px 0px 20px; line-height:1.5em">                            Group Class is a regular Body &amp; Brain class where we utilize stretching, tapping, and meditation to totally relax yet energize the body by opening up all energetic pathways.</p>
					<p style="margin:20px 20px 20px 20px; line-height:1.5em; font-style:italic">Disclaimer: The information provided during the session are not intended as professional medical advice and should not relace the advice of your treating healthcare practitioner.</p>

					<div class="help_wrap">
						<div class="help">Still have questions? Call us at <a href="tel:1-877-477-YOGA">1-877-477-YOGA</a></div>
					</div>
				</div>
			</div>
			<!-- end of first step -->
		</div>

	</div>




	<div class="getstarted secondstep" ng-show="secondstep">
	<span class="title">Beneplace Packages</span>
		<ul class="state">
			<li><a href="" ng-click="backtofirststep()"><p class="program">Choose Program</p></a></li>
			<li class="arrow"><img src="/img/frontend/ico_state_arrow.gif" alt=""></li>
			<li class="on"><p class="location">Find Location</p></li>
			<li class="arrow"><img src="/img/frontend/ico_state_arrow.gif" alt=""></li>
			<li ><p class="schedule">Schedule Session &amp; Class</p></li>
			<li class="arrow"><img src="/img/frontend/ico_state_arrow.gif" alt=""></li>
			<li class="last"><p class="confirm">Confirm Your Appointment</p></li>
		</ul>


		<div class="box_wrap" id="gototop">
		<div class="inner_wrap">
			<h6 class="sub_title">Find the center nearest you to schedule <span>your first class.</span></h6>
			<div class="form">
				<form name="formsearch" ng-submit="searchcenter(search)">
				<legend>Find a Center</legend>
				<div class="input_box">
					<label for="input_zip">Search By <span>Zip Code</span></label>
					<input name="txtZipcode" type="text" id="txtZipcode" class="input_text" ng-model="search.zip">

					<span class="desc">(Ex : 85252)</span>
				</div>
				<div class="input_box">
					<label for="input_city">Search By <span>City &amp; State</span></label>
					<input name="txtCity" type="text" id="txtCity" class="input_text" ng-model="search.city">

					<span class="desc">(Ex : Phoenix, AZ)</span>
				</div>
				<div class="input_box">
					<label for="input_state">Search By <span>State</span></label>
					
						<select class="select_wrap" ng-model="search.state">
							<option value="">Select State</option>
							<option value="AZ">Arizona</option>
							<option value="CA">California</option>
							<option value="CO">Colorado</option>
							<option value="DC">District of Columbia</option>
							<option value="FL">Florida</option>
							<option value="GA">Georgia</option>
							<option value="HI">Hawaii</option>
							<option value="IL">Illinois</option>
							<option value="MA">Massachusetts</option>
							<option value="MD">Maryland</option>
							<option value="MN">Minnesota</option>
							<option value="NC">North Carolina</option>
							<option value="NJ">New Jersey</option>
							<option value="NM">New Mexico</option>
							<option value="NV">Nevada</option>
							<option value="NY">New York</option>
							<option value="OR">Oregon</option>
							<option value="TX">Texas</option>
							<option value="VA">Virginia</option>
							<option value="WA">Washington</option>
							<option value="WI">Wisconsin</option>
						</select>
					<span class="desc">Select State</span>
				</div>
				<div class="btn_wrap">
					<div id="divErrorMsr" class="processMsg" style="display:none;">
						<p>* Please provide a valid 5 character zip code.</p>
					</div>
					<div class="processMsg">

					</div>
					<button type="submit" class="btn_find"><span>Find a Center</span></button>
					
				</div>
				</form>
			</div>
			<!-- find result -->

			<div class="location_wrap" ng-show="noresults">
				<div class="result_wrap">
					<center><h5 class="sub_title2">No Result Found</h5></center>
				</div>
			</div>

			<div class="location_wrap" ng-show="locationresults">
				<div class="result_wrap">
					<p class="desc">Please select the center that’s most convenient for you:</p>
					<p class="desc2">* Franchises &nbsp;** Operated by affiliated company</p>
					<br>
					<p class="table_desc">Center Name and Address Table</p>
					<br>
					<table class="result_table radio_container" id="result_tb">
						<colgroup>
						<col width="80">
						<col width="80">
						<col width="520">
						<col width="120">
					</colgroup>
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col" colspan="2">Center Name and Address</th>
							<th scope="col">Distance</th>
						</tr>
					</thead>
					<tbody>

						<tr ng-repeat="list in locationlist">
							<td>{[{$index + 1}]}</td>
							<td class="radio">
								<div class=""><input type="radio" name="centername" ng-click="centerradiopick(list.centerid,list.centertitle,list.centeraddress,list.centerzip,list.phonenumber,list.lat,list.lon,list.email,list.paypalid,list.authorizeid,list.authorizekey)"></div>
							</td>
							<td class="center">
								<p class="name"><label for="radio_center_01"><a href="" ng-click="centerpick(list.centerid,list.centertitle,list.centeraddress,list.centerzip,list.phonenumber,list.lat,list.lon,list.email,list.paypalid,list.authorizeid,list.authorizekey)">{[{list.centertitle}]} <span style="color:#666;"></span></a></label> </p>
								<p> {[{list.centeraddress}]}, {[{list.centerzip}]}</p>
								<p>Phone : {[{list.phonenumber}]} </p>
							</td>
							<td>{[{list.distance}]} <span ng-if="list.distance">Miles</span></td>
						</tr>

						

					</tbody>
				</table>
				<div class="carderror">{[{pickcentermsg}]}</div>
				<div class="btn_wrap" ng-show="btnpickcenter">
					<input ng-click="gotoschedule()" type="button" value="Choose this center" class="btn_find">
				</div>

				<!-- google map -->

				<h5 class="sub_title2">US Body & Brain Center Locations</h5>

				<div class="result_map">
					<div id="map_canvas">
						<ui-gmap-google-map center="map.center" zoom="map.zoom" draggable="true" options="options" bounds="map.bounds">
							<ui-gmap-markers models="randomMarkers" coords="'self'"  icon="'icon'">
								<ui-gmap-windows show="show">
					                <div ng-non-bindable>{[{title}]}</div>
					            </ui-gmap-windows>
							</ui-gmap-markers>
						</ui-gmap-google-map>
			        </div>
				</div>

			</div>
			</div>





			</div>
		</div>
	</div>







	<form name="scheduleform" ng-submit="paymentsubmit()">
	<div class="getstarted thirdstep" ng-show="thirdstep">
	<span class="title">Beneplace Packages</span>
		<ul class="state">
			<li><a href="" ng-click="backtofirststep()"><p class="program">Choose Program</p></a></li>
			<li class="arrow"><img src="/img/frontend/ico_state_arrow.gif" alt=""></li>
			<li><a href="" ng-click="backtosecondstep()"><p class="location">Find Location</p></a></li>
			<li class="arrow"><img src="/img/frontend/ico_state_arrow.gif" alt=""></li>
			<li class="on"><p class="on schedule">Schedule Session &amp; Class</p></li>
			<li class="arrow"><img src="/img/frontend/ico_state_arrow.gif" alt=""></li>
			<li class="last"><p class="confirm">Confirm Your Appointment</p></li>
		</ul>
	<div class="box_wrap" id="gototop">
	<div class="schedule_wrap ">
		
		<div class="inner_wrap">
			<h6 class="sub_title">You’ve chosen to take a {[{sessionclass}]}.</h6>

			<div id="UpdatePanel">

			
					<legend>You’ve chosen to take {[{sessionclass}]}.</legend>
					<div id="ValidationSummary" class="processMsg" style="color:Red;display:none;">

					</div>
					<span id="NameRequired" title="Please enter your name." style="color:Red;display:none;"></span>
					<span id="EmailRequired" title="Please enter your email address." style="color:Red;display:none;"></span>
					<span id="EmailRegularExpression" style="color:Red;display:none;"></span>    
					<span id="PhoneRequired1" title="Please enter your phone number." style="color:Red;display:none;"></span>
					<span id="PhoneRegularExpression1" style="color:Red;display:none;"></span>
					<span id="PhoneRequired2" title="Phone Number #2 is required." style="color:Red;display:none;"></span>
					<span id="PhoneRegularExpression2" style="color:Red;display:none;"></span> 
					<span id="PhoneRequired3" title="Phone Number #3 is required." style="color:Red;display:none;"></span>
					<span id="PhoneRegularExpression3" style="color:Red;display:none;"></span> 
					<span id="DateRegularExpression" style="color:Red;display:none;"></span>  
					<span id="DateRequired" title="Please choose a date." style="color:Red;display:none;"></span>



					<br>
					<div class="form">
						<div class="input_box">
							<label for="input_name">Name</label>
							<input name="txtName" type="text" id="txtName" class="input_text" ng-model='fullname' required ng-change="putname(fullname)"><span class= "req"> *</span>
						</div>
						<div class="input_box">
							<label for="input_email">Email Address</label>
							<input name="txtEmail" type="email" id="txtEmail" class="input_text" ng-model='email' required ng-change="putemail(email)"><span class= "req"> *</span>
						</div>
						<div class="input_box">
							<label for="input_phone">Phone Number</label>
								<div class="phone-number-container">
									<input name="txtPhone1" type="text" id="txtPhone1" maxlength="3" class="input_text input_phone" ng-model="phone1" ng-change="putphone1(phone1)" onkeypress="return isNumberKey(event)" required>
									<input name="txtPhone2" type="text" id="txtPhone2" maxlength="3" class="input_text input_phone" ng-model="phone2" ng-change="putphone2(phone2)"  onkeypress="return isNumberKey(event)" required>
									<input name="txtPhone3" type="text" id="txtPhone3" maxlength="4" class="input_text input_phone" ng-model="phone3" ng-change="putphone3(phone3)"  onkeypress="return isNumberKey(event)" required>
								</div><span class= "req">*</span>
						</div>
						<div class="input_box">
							<label for="input_preferred">Preferred Location</label>
							<input name="txtLocation" type="text" id="txtLocation" class="input_text" disabled value="{[{centertitle}]}">
							<div class="text">
								<p class="text_title">{[{centertitle}]}</p>
								<p>{[{centeraddress}]}, {[{centerzip}]}</p>
								<p>{[{centerphone}]}</p>
								<span style="display:none;">					
									GlendaleAZ@bodynbrain.com
									AZ
								</span>
							</div>
						</div>
					</div>
					
					<div class="infocontainer">
					<br>
					<p class="sub_title2">Schedule your intro session.</p>		
						<br>
						<p class="desc_error" ng-show="dateerror" style="color:red !important;">{[{dateerrormsg}]}</p>
						<div class="date_box">
							<label for="input_date">Date</label>

							<div class="input-container">
								<input name="txtDate" type="text" id="datepicker" class="input_text" ng-model="scheduledate" ng-change="checkdate(scheduledate)" required >
							</div>
							<span id="DateEx" class="desc" >(Ex: 2015-11-21)</span>
							<div ng-if="classhour == true">
							<label for="input_time">Time</label>
							<div class="select_wrap">
								<select class="inputBox" ng-model="classtime" ng-change="puttime(classtime)" required>
									<option value="" selected=""></option>
									<option value="07:00 AM">07:00 AM</option>
									<option value="07:30 AM">07:30 AM</option>
									<option value="08:00 AM">08:00 AM</option>
									<option value="08:30 AM">08:30 AM</option>
									<option value="09:00 AM">09:00 AM</option>
									<option value="09:30 AM">09:30 AM</option>
									<option value="10:00 AM">10:00 AM</option>
									<option value="10:30 AM">10:30 AM</option>
									<option value="11:00 AM">11:00 AM</option>
									<option value="11:30 AM">11:30 AM</option>
									<option value="12:00 PM">12:00 PM</option>
									<option value="12:30 PM">12:30 PM</option>
									<option value="01:00 PM">01:00 PM</option>
									<option value="01:30 PM">01:30 PM</option>
									<option value="02:00 PM">02:00 PM</option>
									<option value="02:30 PM">02:30 PM</option>
									<option value="03:00 PM">03:00 PM</option>
									<option value="03:30 PM">03:30 PM</option>
									<option value="04:00 PM">04:00 PM</option>
									<option value="04:30 PM">04:30 PM</option>
									<option value="05:00 PM">05:00 PM</option>
									<option value="05:30 PM">05:30 PM</option>
									<option value="06:00 PM">06:00 PM</option>
									<option value="06:30 PM">06:30 PM</option>
									<option value="07:00 PM">07:00 PM</option>
									<option value="06:30 PM">07:30 PM</option>
									<option value="07:00 PM">08:00 PM</option>
									<option value="06:30 PM">08:30 PM</option>
									<option value="07:00 PM">09:00 PM</option>
									<option value="06:30 PM">09:30 PM</option>
									<option value="07:00 PM">10:00 PM</option>

								</select><span class= "req"> *</span>
							</div>
							</div>

						</div>
						<p class="date_desc">An instructor will contact you to confirm your appointment or propose another time.</p>


						<div ng-if="classhour == false" id="gotooffer_sche">

							<p class="date_desc">Choose one of the classes you want to attend. Please arrive at the studio 15 minutes early. We will contact you to confirm your appointment. You’ll schedule the 1-on-1 session at the center.</p>
							<br>
							<br>
							<p class="date_desc" title="Please Choose Schedule from table below" style="color:Red !important;text-align:center !important;" ng-show="classhourwarning">Please Choose Schedule from table below!</p>
							<h4 class="sub_title2 schedule_title">Make an Appointment for One of These Classes.</h4>

							<!-- <table class="result_table schedule_table schedule_class_table">
								<caption>Class Schedule Table</caption>
								<colgroup>
								
								<col width="92">
								<col width="92">
								<col width="92">
								<col width="92">
								<col width="92">
								<col width="92">
								<col width="92">
							</colgroup>
							<thead>
								<tr>

									<th scope="col">MON</th>
									<th scope="col">TUE</th>
									<th scope="col">WED</th>
									<th scope="col">THU</th>
									<th scope="col">FRI</th>
									<th scope="col">SAT</th>
									<th scope="col">SUN</th>
								</tr>
							</thead>
							<tbody>

								<tr>
									<td>
										<div class="col-sm-12 divtable" ng-if="today != 'Monday'" ng-repeat="list in schedlelist | filter: 'Monday'">
											{[{list.classtype}]}
										</div>
										<div class="col-sm-12 divtable" ng-if="today == 'Monday'" ng-repeat="list in schedlelist | filter: 'Monday'">
											<input type="radio" id="{[{list.classid}]}" name="radios" value="{[{list.starttime}]} {[{list.starttimeformat}]}" >
											<label for="{[{list.classid}]}">{[{list.classtype}]}</label>
										</div>
									</td>
									<td>
										<div class="col-sm-12 divtable" ng-if="today != 'Tuesday'" ng-repeat="list in schedlelist | filter: 'Tuesday'">
											{[{list.classtype}]}
										</div>
										<div class="col-sm-12 divtable" ng-if="today == 'Tuesday'" ng-repeat="list in schedlelist | filter: 'Tuesday'">
											<input type="radio" id="{[{list.classid}]}" name="radios" value="{[{list.starttime}]} {[{list.starttimeformat}]}" >
											<label for="{[{list.classid}]}">{[{list.classtype}]}</label>
										</div>
									</td>
									<td>
										<span id="rptSchedule_ctl00_lblMonText" ng-repeat="list in schedlelist | filter: 'Wednesday'">{[{list.classtype}]}</span>
									</td>
									<td>
										<span id="rptSchedule_ctl00_lblMonText" ng-repeat="list in schedlelist | filter: 'Thursday'">{[{list.classtype}]}</span>
									</td>
									<td>
										<span id="rptSchedule_ctl00_lblMonText" ng-repeat="list in schedlelist | filter: 'Friday'">{[{list.classtype}]}</span>
									</td>
									<td>
										<span id="rptSchedule_ctl00_lblMonText" ng-repeat="list in schedlelist | filter: 'Saturday'">{[{list.classtype}]}</span>
									</td>
									<td>
										<span id="rptSchedule_ctl00_lblMonText" ng-repeat="list in schedlelist | filter: 'Sunday'">{[{list.classtype}]}</span>
									</td>
								</tr>

								</tbody>
							</table> -->

							<!-- <table class="responsive-table sched_table" style="float: left;">
								<thead>
									<tr>
										<th data-field="Sunday" class="tb_header">Time</th>
										<th data-field="Sunday" class="tb_header">Sunday</th>
										<th data-field="Monday" class="tb_header">Monday</th>
										<th data-field="Tuesday" class="tb_header">Tuesday</th>
										<th data-field="Wednesday" class="tb_header">Wednesday</th>
										<th data-field="Thursday" class="tb_header">Thursday</th>
										<th data-field="Friday" class="tb_header">Friday</th>
										<th data-field="Saturday" class="tb_header">Saturday</th>
									</tr>
								</thead>

								<tbody>
									<tr>
										<td>
											<div class="divtable" ng-if="today != 'Sunday'" ng-repeat="list in schedlelist | filter: 'Sunday'">
												{[{list.classtype}]}<br>
												{[{list.starttime}]} {[{list.starttimeformat}]}

											</div>
											<div class="divtable" ng-if="today == 'Sunday'" ng-repeat="list in schedlelist | filter: 'Sunday'">
												<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]} {[{list.starttimeformat}]}" ng-click="puttime(radiodata)">
												<label class="bluetext" for="{[{list.classid}]}">{[{list.classtype}]} <br>{[{list.starttime}]} {[{list.starttimeformat}]}</label>
												

											</div>
										</td>
										<td>
											<div class="divtable" ng-if="today != 'Monday'" ng-repeat="list in schedlelist | filter: 'Monday'">
												{[{list.classtype}]}<br>
												{[{list.starttime}]} {[{list.starttimeformat}]}

											</div>
											<div class="divtable" ng-if="today == 'Monday'" ng-repeat="list in schedlelist | filter: 'Monday'">
												<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]} {[{list.starttimeformat}]}" ng-click="puttime(radiodata)">
												<label class="bluetext" for="{[{list.classid}]}">{[{list.classtype}]} <br>{[{list.starttime}]} {[{list.starttimeformat}]}</label>
											</div>
										<td>
											<div class="divtable" ng-if="today != 'Tuesday'" ng-repeat="list in schedlelist | filter: 'Tuesday'">
												{[{list.classtype}]}<br>
												{[{list.starttime}]} {[{list.starttimeformat}]}

											</div>
											<div class="divtable" ng-if="today == 'Tuesday'" ng-repeat="list in schedlelist | filter: 'Tuesday'">
												<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]} {[{list.starttimeformat}]}" ng-click="puttime(radiodata)">
												<label class="bluetext" for="{[{list.classid}]}">{[{list.classtype}]} <br>{[{list.starttime}]} {[{list.starttimeformat}]}</label>

											</div>
										</td>
										<td>
											<div class="divtable" ng-if="today != 'Wednesday'" ng-repeat="list in schedlelist | filter: 'Wednesday'">
												{[{list.classtype}]}<br>
												{[{list.starttime}]} {[{list.starttimeformat}]}

											</div>
											<div class="divtable" ng-if="today == 'Wednesday'" ng-repeat="list in schedlelist | filter: 'Wednesday'">
												<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]} {[{list.starttimeformat}]}" ng-click="puttime(radiodata)">
												<label class="bluetext" for="{[{list.classid}]}">{[{list.classtype}]} <br>{[{list.starttime}]} {[{list.starttimeformat}]}</label>

											</div>
										</td>
										<td>
											<div class="divtable" ng-if="today != 'Thursday'" ng-repeat="list in schedlelist | filter: 'Thursday'">
												{[{list.classtype}]}<br>
												{[{list.starttime}]} {[{list.starttimeformat}]}

											</div>
											<div class="divtable" ng-if="today == 'Thursday'" ng-repeat="list in schedlelist | filter: 'Thursday'">
												<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]} {[{list.starttimeformat}]}" ng-click="puttime(radiodata)">
												<label class="bluetext" for="{[{list.classid}]}">{[{list.classtype}]} <br>{[{list.starttime}]} {[{list.starttimeformat}]}</label>

											</div>
										</td>
										<td>
											<div class="divtable" ng-if="today != 'Friday'" ng-repeat="list in schedlelist | filter: 'Friday'">
												{[{list.classtype}]}<br>
												{[{list.starttime}]} {[{list.starttimeformat}]}

											</div>
											<div class="divtable" ng-if="today == 'Friday'" ng-repeat="list in schedlelist | filter: 'Friday'">
												<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]} {[{list.starttimeformat}]}" ng-click="puttime(radiodata)">
												<label class="bluetext" for="{[{list.classid}]}">{[{list.classtype}]} <br>{[{list.starttime}]} {[{list.starttimeformat}]}</label>

											</div>
										</td>
										<td>
											<div class="divtable" ng-if="today != 'Saturday'" ng-repeat="list in schedlelist | filter: 'Saturday'">
												{[{list.classtype}]}<br>
												{[{list.starttime}]} {[{list.starttimeformat}]}

											</div>
											<div class="divtable" ng-if="today == 'Saturday'" ng-repeat="list in schedlelist | filter: 'Saturday'">
												<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]} {[{list.starttimeformat}]}" ng-click="puttime(radiodata)">
												<label class="bluetext" for="{[{list.classid}]}">{[{list.classtype}]} <br>{[{list.starttime}]} {[{list.starttimeformat}]}</label>

											</div>
										</td>
										
									</tr>
								</tbody>
							</table> -->

							<table class="striped bordered sched_table" style="float: left;">
								<thead>
									<tr>
										<th data-field="Sunday" class="tb_header">Time</th>
										<th data-field="Sunday" class="tb_header">Sunday</th>
										<th data-field="Monday" class="tb_header">Monday</th>
										<th data-field="Tuesday" class="tb_header">Tuesday</th>
										<th data-field="Wednesday" class="tb_header">Wednesday</th>
										<th data-field="Thursday" class="tb_header">Thursday</th>
										<th data-field="Friday" class="tb_header">Friday</th>
										<th data-field="Saturday" class="tb_header">Saturday</th>
									</tr>
								</thead>

								<tbody>
									<tr ng-repeat="list in schedlelist">
										<td>
											<div>
												{[{list.starttime}]} - <br>
												{[{list.endtime}]}
											</div>
										</td>
										<td>
											<div ng-if="today != 'Sunday'">
												<div class="divtable" ng-if="list.su != null && list.su != ''">
													{[{list.su}]}
												</div>
											</div>
											<div ng-if="today == 'Sunday'">
												<div class="divtable" ng-show="list.su != null && list.su != ''">
													<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]}" ng-click="puttime(radiodata)">
													<label class="bluetext" for="{[{list.classid}]}">{[{list.su}]}{[{list.su.length}]}</label>
												</div>
											</div>
										</td>
										<td>
											<div ng-if="today != 'Monday'">
												<div class="divtable" ng-if="list.mo != null && list.mo != ''">
													{[{list.mo}]}
												</div>
											</div>
											<div ng-if="today == 'Monday'">
												<div class="divtable" ng-if="list.mo != null && list.mo != ''">
													<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]}" ng-click="puttime(radiodata)">
													<label class="bluetext" for="{[{list.classid}]}">{[{list.mo}]}</label>
												</div>
											</div>
										<td>
											<div ng-if="today != 'Tuesday'">
												<div class="divtable" ng-if="list.tu != null && list.tu != ''">
													{[{list.tu}]}
												</div>
											</div>
											<div ng-if="today == 'Tuesday'">
												<div class="divtable" ng-if="list.tu != null && list.tu != ''">
													<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]}" ng-click="puttime(radiodata)">
													<label class="bluetext" for="{[{list.classid}]}">{[{list.tu}]}</label>
												</div>
											</div>
										</td>
										<td>
											<div ng-if="today != 'Wednesday'">
												<div class="divtable" ng-if="list.we != null && list.we != ''">
													{[{list.we}]}
												</div>
											</div>
											<div ng-if="today == 'Wednesday'">
												<div class="divtable" ng-if="list.we != null && list.we != ''">
													<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]}" ng-click="puttime(radiodata)">
													<label class="bluetext" for="{[{list.classid}]}">{[{list.we}]}</label>
												</div>
											</div>
										</td>
										<td>
											<div ng-if="today != 'Thursday'">
												<div class="divtable" ng-if="list.th != null && list.th != ''">
													{[{list.th}]}
												</div>
											</div>
											<div ng-if="today == 'Thursday'">
												<div class="divtable" ng-if="list.th != null && list.th != ''">
													<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]}" ng-click="puttime(radiodata)">
													<label class="bluetext" for="{[{list.classid}]}">{[{list.th}]}</label>
												</div>
											</div>
										</td>
										<td>
											<div ng-if="today != 'Friday'">
												<div class="divtable" ng-if="list.fr != null && list.fr != ''">
													{[{list.fr}]}
												</div>
											</div>
											<div ng-if="today == 'Friday'">
												<div class="divtable" ng-if="list.fr != null && list.fr != ''">
													<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]}" ng-click="puttime(radiodata)">
													<label class="bluetext" for="{[{list.classid}]}">{[{list.fr}]}</label>
												</div>
											</div>
										</td>
										<td>
											<div ng-if="today != 'Saturday'">
												<div class="divtable" ng-if="list.sa != null && list.sa != ''">
													{[{list.sa}]}
												</div>
											</div>
											<div ng-if="today == 'Saturday'">
												<div class="divtable" ng-if="list.sa != null && list.sa != ''">
													<input type="radio" id="{[{list.classid}]}" name="radios" ng-model="radiodata" value="{[{list.starttime}]}" ng-click="puttime(radiodata)">
													<label class="bluetext" for="{[{list.classid}]}">{[{list.sa}]}</label>
												</div>
											</div>
										</td>
										
									</tr>
								</tbody>
							</table>

						</div>

						<!-- schedule for -->



						<!-- what interests -->


						<!-- to know -->
						<h4 class="sub_title2">Is there anything you'd like us to know? </h4>
						<p class="date_desc" style="margin-bottom:0px!important;">(max 80 character)</p>
						<textarea name="txtComment" id="txtComment" class="textarea_know" ng-model="comment" ng-change="putcomment(comment)" maxlength="80" onkeypress="return onlyAlphabets(event)"></textarea>
						<p class="textarea_desc">All of your personal information will be kept private: <a href="/privacy-policies" target="_blank">Privacy Policy</a></p>
											
						<!-- payment -->
						
					


						<table class="result_table radio_container">
							
							<colgroup>
							<col width="215">
							<col width="215">
							<col width="215">
							<col width="215">
						</colgroup>
						<thead>
							<tr>
								<th scope="col">Item</th>
								<th scope="col">Price</th>
								<th scope="col">Quantity</th>
								<th scope="col">Total</th>
							</tr>
						</thead>
						<tbody>

							<tr>
								<td>{[{sessionclass}]}</td>
								<td>${[{finalonlinerate}]}</td>
								<td><input type="text" ng-init="itemquantity = 1" ng-model="itemquantity" id="txtQuantity"  class="input_text" style="width:30px !important; text-align:center !important;  padding: 0 8px !important; height: 25px !important; color: #666 !important; border: 1px solid #ccc !important; border-bottom: 1px solid #ccc !important; box-shadow: 0 1px 0 0 #FFF !important;" onkeypress="return isNumberKey(event)" ng-change="calculateprice(itemquantity)" required maxlength="3"></td>
								<td><strong>$<span id="lblPrice" ng-if="totalprice">{[{totalprice}]}</span> <span id="lblPrice" ng-if="!totalprice">0</span></strong></td>
							</tr>

						</tbody>
					</table>
				</div>

				<div class="submit-query">

					<input type="submit" name="btnSubmit"  id="btnSubmit" class="btn_submit_new"><span style="margin:0 20px;">Or</span><input type="submit" name="btnPaypalSubmit"  id="btnPaypalSubmit" class="btn_submit_paypal" ng-show="invalidpaypal"></form>
					<form action="https://www.paypal.com/cgi-bin/webscr" method="post" name="frmPayPal1" class="btn_submit_paypal" ng-show="validpaypal">
				    <input type="hidden" name="business" value="{[{paypalid}]}">
				    <input type="hidden" name="cmd" value="_xclick">
				    <input type="hidden" name="item_name" value="{[{sessionclass}]}">
				    <input type="hidden" name="item_number" value="1">
				    <input type="hidden" name="custom" value="{[{submitfullname}]}|{[{submitemail}]}|{[{submitcenterid}]}|{[{submitphonenumber}]}|{[{submittime}]}|{[{submitschedday}]}|{[{submitcomment}]}|{[{itemquantity}]}|{[{device}]}|{[{deviceos}]}|{[{devicebrowser}]}|{[{itemprice}]}|{[{sessionclass}]}|{[{sessiontype}]}|beneplace">
				    <!-- <input type="hidden" name="credits" value="510"> -->
				    <!-- <input type="hidden" name="userid" value="1"> -->
				    <input type="hidden" name="amount" value="{[{itemprice}]}">

				    <input type="hidden" name="quantity" value="{[{itemquantity}]}">
				    <!-- <input type="hidden" name="cpp_header_image" value="http://www.phpgang.com/wp-content/uploads/gang.jpg"> -->
				    <input type="hidden" name="no_shipping" value="1">
				    <input type="hidden" name="currency_code" value="USD">
				    <input type="hidden" name="handling" value="0">
				    <!-- <input type="hidden" name="cancel_return" value="http://demo.phpgang.com/payment_with_paypal/cancel.php"> -->
				    <!-- <input type="hidden" name="return" value="http://bnb.gotitgenius.com/getstarted/success"> -->
				    <input type="submit" name="btnPaypalSubmit"  id="btnPaypalSubmit" class="btn_submit_paypal" ng-show="validpaypal">

				    </form> 
				    
				</div>

			</div>
		</div>


	</div>
	</div>

	</div>

<!-- 	<input type="hidden" name="business" value="{[{paypalid}]}">
	<input type="hidden" name="cmd" value="_xclick">
	<input type="hidden" name="item_name" value="{[{sessionclass}]}">
	<input type="hidden" name="item_number" value="1">
	<input type="hidden" name="custom" value="{[{submitfullname}]}|{[{submitemail}]}|{[{submitcenterid}]}|{[{submitphonenumber}]}|{[{submittime}]}|{[{submitschedday}]}|{[{submitcomment}]}|{[{itemquantity}]}|{[{device}]}|{[{deviceos}]}|{[{devicebrowser}]}|{[{itemprice}]}|{[{sessionclass}]}|{[{sessiontype}]}|beneplace">

	<input type="hidden" name="amount" value="{[{itemprice}]}">

	<input type="hidden" name="quantity" value="{[{itemquantity}]}"> -->








	<form name="checkout" ng-submit="submitcheckout(card)">
		<div class="getstarted paymentcheck" ng-show="paymentcheck">
			<span class="title">Beneplace Packages</span>
			<ul class="state">
				<li><a href="" ng-click="backtofirststep()"><p class="program">Choose Program</p></a></li>
			<li class="arrow"><img src="/img/frontend/ico_state_arrow.gif" alt=""></li>
				<li><a href="" ng-click="backtosecondstep()"><p class="location">Find Location</p></a></li>
			<li class="arrow"><img src="/img/frontend/ico_state_arrow.gif" alt=""></li>
				<li class="on"><p class="on schedule">Schedule Session &amp; Class</p></li>
			<li class="arrow"><img src="/img/frontend/ico_state_arrow.gif" alt=""></li>
				<li class="last"><p class="confirm">Confirm Your Appointment</p></li>
			</ul>
			<div class="box_wrap" id="gototop">
				<div class="inner_wrap">
					<div class="payment_wrap">
						<!-- payment method -->
						<h6 class="sub_title2"><span>Payment</span> Method</h6>

						<div class="form_box">

							<div>

								<img src="/img/frontend/amex.png" width="49px" height="29px">
								<img src="/img/frontend/discover.png" width="49px" height="29px">
								<img src="/img/frontend/mastercard.png" width="49px" height="29px">
								<img src="/img/frontend/visa.png" width="49px" height="29px">

							</div>

							<!-- <div class="input_box">

								<label for="select_card">Select card type</label>
								<div class="select_wrap">
									<span class="value">Discover</span>
									<select name="ddlCardType" id="ddlCardType" class="inputBox" ng-init="card.cardtype = 'Visa'" ng-model="card.cardtype" required>
										<option value="Visa">Visa</option>
										<option value="Master card">Master card</option>
										<option value="American express">American express</option>
										<option value="Discover">Discover</option>

									</select>
								</div>
							</div> -->

							<div class="input_box" >
								<div class="carderror">{[{cardmsg}]}</div>
							</div>

							<div class="input_box">
								<label for="input_cardnumber">Card number</label>
								<input name="txtCardNumber" type="text" id="txtCardNumber" class="input_text" ng-model="card.cardnumber" onkeypress="return isNumberKey(event)" required>
							</div>

							<div class="input_box">
								<label for="input_cardname">First name</label>
								<input name="txtHoldersName" type="text" id="txtHoldersName" class="input_text" value="" ng-model="card.firstname" required>
								
							</div>

							<div class="input_box">
								<label for="input_cardname">Last name</label>
								<input name="txtHoldersName" type="text" id="txtHoldersName" class="input_text" value="" ng-model="card.lastname" required>
							</div>

							

							<div class="input_box">
								<label for="select_month">Expiration date</label>
								<div class="select_wrap select_month">
									<span class="value">01</span>
									<select name="ddlMonth" id="ddlMonth" class="inputBox" ng-init="card.expimonth = '01'" ng-model="card.expimonth" required>
										<option selected="selected" value="01">01</option>
										<option value="02">02</option>
										<option value="03">03</option>
										<option value="04">04</option>
										<option value="05">05</option>
										<option value="06">06</option>
										<option value="07">07</option>
										<option value="08">08</option>
										<option value="09">09</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>

									</select>
								</div>

								<div class="select_wrap select_year">
									<span class="value">2015</span>
									<select name="ddlYear" id="ddlYear" class="inputBox" ng-init="card.expiyear = '15'" ng-model="card.expiyear" required>
										<option value="15">2015</option>
										<option value="16">2016</option>
										<option value="17">2017</option>
										<option value="18">2018</option>
										<option value="19">2019</option>
										<option value="20">2020</option>
										<option value="21">2021</option>
										<option value="22">2022</option>
										<option value="23">2023</option>
										<option value="24">2024</option>
										<option value="25">2025</option>
										<option value="26">2026</option>
										<option value="27">2027</option>
										<option value="28">2028</option>
										<option value="29">2029</option>
									</select>
								</div>

							</div>
						</div>
				<!-- <div class="auth_wrap">
					
					<div class="AuthorizeNetSeal" style="margin-left:700px;"> 
					<script type="text/javascript" language="javascript">                                   
					var ANS_customer_id = "165ea88e-b1aa-456a-bfca-0c57e4b4b94f";
					</script> 
					<script type="text/javascript" language="javascript" src="//verify.authorize.net/anetseal/seal.js">
					</script>
					<style type="text/css">
						div.AuthorizeNetSeal{text-align:center;margin:0;padding:0;width:90px;font:normal 9px arial,helvetica,san-serif;line-height:10px;}
						div.AuthorizeNetSeal a{text-decoration:none;color:black;}
						div.AuthorizeNetSeal a:visited{color:black;}
						div.AuthorizeNetSeal a:active{color:black;}
						div.AuthorizeNetSeal a:hover{text-decoration:underline;color:black;}
						div.AuthorizeNetSeal a img{border:0px;margin:0px;text-decoration:none;}
					</style>
					<a href="http://www.authorize.net/" id="AuthorizeNetText" target="_blank" rel="nofollow">Credit Card Merchant</a> 
					
				</div>  -->



				<!-- billing information -->
				<h5 class="sub_title2"><span>Billing</span> Information</h5>
				<div class="form_box billing_wrap">
					<div class="input_box">
						<label for="input_fullname">Full Name</label>
						<input name="txtFullname" type="text" id="txtFullname" class="input_text" value="" ng-model="card.billfullname" required>
					</div>
					<div class="input_box">
						<label for="input_addr1">Address Line 1</label>
						<input name="txtAddr1" type="text" id="txtAddr1" class="input_text" ng-model="card.billadd1" required>
					</div>
					<div class="input_box">
						<label for="input_addr2">Address Line 2</label>
						<input name="txtAddr2" type="text" id="txtAddr2" class="input_text" ng-model="card.billadd2">
						<span class="desc">(optional)</span>
					</div>
					<div class="input_box">
						<label for="input_city">City</label>
						<input name="txtCity" type="text" id="txtCity" class="input_text" ng-model="card.billcity" required>
					</div>
					<div class="input_box">
						<label for="select_state">State</label>
						<div class="select_wrap select_state">
							<span class="CA">CA</span>
							<select name="ddlBillingState" id="ddlBillingState" class="inputBox" ng-init="card.billstate = 'CA'" ng-model="card.billstate" required>
								<option value="AK">AK</option>
								<option value="AL">AL</option>
								<option value="AR">AR</option>
								<option value="AZ">AZ</option>
								<option value="CA">CA</option>
								<option value="CO">CO</option>
								<option value="CT">CT</option>
								<option value="DC">DC</option>
								<option value="DE">DE</option>
								<option value="FL">FL</option>
								<option value="GA">GA</option>
								<option value="HI">HI</option>
								<option value="IA">IA</option>
								<option value="ID">ID</option>
								<option value="IL">IL</option>
								<option value="IN">IN</option>
								<option value="KS">KS</option>
								<option value="KY">KY</option>
								<option value="LA">LA</option>
								<option value="MA">MA</option>
								<option value="MD">MD</option>
								<option value="ME">ME</option>
								<option value="MI">MI</option>
								<option value="MN">MN</option>
								<option value="MO">MO</option>
								<option value="MS">MS</option>
								<option value="MT">MT</option>
								<option value="NC">NC</option>
								<option value="ND">ND</option>
								<option value="NE">NE</option>
								<option value="NH">NH</option>
								<option value="NJ">NJ</option>
								<option value="NM">NM</option>
								<option value="NV">NV</option>
								<option value="NY">NY</option>
								<option value="OH">OH</option>
								<option value="OK">OK</option>
								<option value="OR">OR</option>
								<option value="PA">PA</option>
								<option value="RI">RI</option>
								<option value="SC">SC</option>
								<option value="SD">SD</option>
								<option value="TN">TN</option>
								<option value="TX">TX</option>
								<option value="UT">UT</option>
								<option value="VA">VA</option>
								<option value="VI">VI</option>
								<option value="VT">VT</option>
								<option value="WA">WA</option>
								<option value="WI">WI</option>
								<option value="WY">WY</option>

							</select>
						</div>
					</div>
					<div class="input_box">
						<label for="input_zipcode">Zip Code</label>
						<input name="txtBillingZipCode" type="text" id="txtBillingZipCode" pattern="\d{3,5}" class="input_text" ng-model="card.billzip" onkeypress="return isNumberKey(event)" title="a minimum of 3 and a maximum of 5 digits" required>
					</div>
					<div class="input_box">
						<label for="input_phonenumber">Phone number</label>
						<input name="txtPhoneNumber" type="text" id="txtPhoneNumber" class="input_text" value="" onkeypress="return isNumberKey(event)" ng-model="card.billphone" required>
					</div> 
				</div> <!-- end of billing_wrap -->

				<!-- terms of use -->
				<h5 class="sub_title2"><span>Terms</span> of Use</h5>
				<div class="form_box terms_wrap" tabindex="0">
					<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">Body &amp; Brain yoga &amp; Health Centers, Inc. ("Body &amp; Brain yoga ") operates the web site www.bodynbrain.com (“Site”) to provide online access to information about Body &amp; Brain yoga and the products, services, and opportunities we provide (the "Service"). By accessing and using this Site, you agree to each of the terms and conditions set forth herein ("Terms of Use"). Additional terms and conditions applicable to specific areas of this Site or to particular content or transactions are also posted in particular areas of the Site and, together with these Terms of Use, govern your use of those areas, content or transactions. These Terms of Use, together with applicable additional terms and conditions, are referred to as this "Agreement".</p>

					<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">Body &amp; Brain yoga reserves the right to modify this Agreement at any time without giving you prior notice. Your use of the Site following any such modification constitutes your agreement to follow and be bound by the Agreement as modified. The last date these Terms of Use were revised is set forth below.</p>
					<ol class="policy_list" style="margin: 0px; padding-top: 15px; padding-right: 0px; padding-left: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline; list-style: none;">
	<li style="margin: 0px 0px 15px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline;">
	<p class="list_title" style="margin: 0px 0px 3px; padding: 0px; border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">1. Products, Content and Specifications</p>

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">All features, content, specifications, products and prices of products and services described or depicted on this Site, are subject to change at any time without notice. Certain weights, measures and similar descriptions are approximate and are provided for convenience purposes only. Body &amp; Brain yoga will make all reasonable efforts to accurately display the attributes of our products, including the applicable colors; however, the actual color you see will depend on your computer system, and we cannot guarantee that your computer will accurately display such colors. The inclusion of any products or services on this Site at a particular time does not imply or warrant that these products or services will be available at any time. It is your responsibility to ascertain and obey all applicable local, state, federal and international laws (including minimum age requirements) in regard to the possession, use and sale of any item purchased from this Site. By placing an order, you represent that the products ordered will be used only in a lawful manner. All videocassettes, DVDs and similar products sold are for private, home use (where no admission fee is charged), non-public performance and may not be duplicated.</p>
	</li>
	<li style="margin: 0px 0px 15px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline;">
	<p class="list_title" style="margin: 0px 0px 3px; padding: 0px; border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">2. Shipping Limitations</p>

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">When an order is placed, it will be shipping to an address designated by the purchaser as long as that shipping address is compliant with the shipping restrictions contained on this Site. All purchases from this Site are made pursuant to a shipment contract. As a result, risk of loss and title for items purchased from this Site pass to you upon delivery of the items to the carrier. You are responsible for filing any claims with carriers for damaged and/or lost shipments. Click&nbsp;<a href="http://www.bodynbrain.com/community/privacy.aspx?menu=Privacy" style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline; color: rgb(21, 132, 195);">here</a>&nbsp;to see&nbsp;<a href="http://www.bodynbrain.com/community/privacy.aspx?menu=Privacy" style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline; color: rgb(21, 132, 195);">Body &amp; Brain yoga's Privacy Policy.</a></p>
	</li>
	<li style="margin: 0px 0px 15px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline;">
	<p class="list_title" style="margin: 0px 0px 3px; padding: 0px; border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">3. Accuracy of Information</p>

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">Body &amp; Brain yoga attempts to ensure that information on this Site is complete, accurate and current. Despite our efforts, the information on this Site may occasionally be inaccurate, incomplete or out of date. We make no representation as to the completeness, accuracy or currentness of any information on this Site. For example, products included on this Site may be unavailable, may have different attributes than those listed, or may actually carry a different price than that stated on this Site. In addition, we may make changes in information about price and availability without notice. The price displayed on the website may differ from the price for the same item sold as merchandise at one of our locations. While it is our practice to confirm orders by email, the receipt of an email order confirmation does not constitute our acceptance of an order or our confirmation of an offer to sell a product or service. We reserve the right, without prior notice, to limit the order quantity on any product or service and/or to refuse service to any customer. We also may require verification of information prior to the acceptance and/or shipment of any order.</p>
	</li>
	<li style="margin: 0px 0px 15px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline;">
	<p class="list_title" style="margin: 0px 0px 3px; padding: 0px; border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">4. Use of Site</p>

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">You may use the Service, the Site, and the information, writings, images and/or other works that you see, hear or otherwise experience on the Site (singly or collectively, the "Content") solely for your non-commercial, personal purposes and/or to learn about Body &amp; Brain yoga products and services. No right, title or interest in any Content is transferred to you, whether as a result of downloading such Content or otherwise. Body &amp; Brain yoga reserves complete title and full intellectual property rights in all Content. Except as expressly authorized by this Agreement, you may not use, alter, copy, distribute, transmit, or derive another work from any Content obtained from the Site or the Service, except as expressly permitted by the Terms of Use.</p>
	</li>
	<li style="margin: 0px 0px 15px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline;">
	<p class="list_title" style="margin: 0px 0px 3px; padding: 0px; border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">5. Copyright</p>

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">The Site and the Content are protected by U.S. and/or foreign copyright laws, and belong to Body &amp; Brain yoga or its partners, affiliates, contributors or third parties. The copyrights in the Content are owned by Body &amp; Brain yoga or other copyright owners who have authorized their use on the Site. You may download and reprint Content for non-commercial, non-public, personal use only. (If you are browsing this Site as an employee or member of any business or organization, you may download and reprint Content only for educational or other non-commercial purposes within your business or organization, except as otherwise permitted by Body &amp; Brain yoga, for example in certain password-restricted areas of the Site and in our&nbsp;<a href="http://www.bodynbrain.com/terms-of-use#" style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline; color: rgb(21, 132, 195);">Frequently Asked Questions</a>). No manipulation or alteration, in any way, of images or other Content on the Site is permitted.</p>
	</li>
	<li style="margin: 0px 0px 15px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline;">
	<p class="list_title" style="margin: 0px 0px 3px; padding: 0px; border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">6. Trademarks</p>

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">You are prohibited from using any of the marks or logos appearing throughout the Site without permission from the trademark owner, except as permitted by applicable law.</p>
	</li>
	<li style="margin: 0px 0px 15px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline;">
	<p class="list_title" style="margin: 0px 0px 3px; padding: 0px; border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">7. Links to Third-Party Web Sites</p>

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">Links on the Site to third party web sites or information are provided solely as a convenience to you. If you use these links, you will leave the Site. Such links do not constitute or imply an endorsement, sponsorship, or recommendation by Body &amp; Brain yoga of the third party, the third-party web site, or the information contained therein. Body &amp; Brain yoga is not responsible for the availability of any such web sites. Body &amp; Brain yoga is not responsible or liable for any such web site or the content thereon. If you use the links to the web sites of Body &amp; Brain yoga affiliates or service providers, you will leave the Site, and will be subject to the terms of use and privacy policy applicable to those web sites.</p>
	</li>
	<li style="margin: 0px 0px 15px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline;">
	<p class="list_title" style="margin: 0px 0px 3px; padding: 0px; border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">8. Linking to this Site</p>

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">If you would like to link to the Site, you must follow Body &amp; Brain yoga's&nbsp;<a href="http://www.bodynbrain.com/terms-of-use" style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline; color: rgb(21, 132, 195);">link guidelines.</a>&nbsp;Unless specifically authorized by Body &amp; Brain yoga, you may not connect "deep links" to the Site, i.e, create links to this site that bypass the home page or other parts of the Site. You may not mirror or frame the home page or any other pages of this Site on any other web site or web page.</p>
	</li>
	<li style="margin: 0px 0px 15px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline;">
	<p class="list_title" style="margin: 0px 0px 3px; padding: 0px; border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">9. Downloading Files</p>

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">Body &amp; Brain yoga cannot and does not guarantee or warrant that files available for downloading through the Site will be free of infection by software viruses or other harmful computer code, files or programs.</p>
	</li>
	<li style="margin: 0px 0px 15px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline;">
	<p class="list_title" style="margin: 0px 0px 3px; padding: 0px; border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">10. Disclaimer of Warranties</p>

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">Body &amp; Brain yoga MAKES NO EXPRESS OR IMPLIED WARRANTIES, REPRESENTATIONS OR ENDORSEMENTS WHATSOEVER WITH RESPECT TO THE SITE, THE SERVICE OR THE CONTENT. Body &amp; Brain yoga EXPRESSLY DISCLAIMS ALL WARRANTIES OF ANY KIND, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT, WITH REGARD TO THE SITE, THE SERVICE, THE CONTENT, AND ANY PRODUCT OR SERVICE FURNISHED OR TO BE FURNISHED VIA THE SITE. Body &amp; Brain yoga DOES NOT WARRANT THAT THE FUNCTIONS PERFORMED BY THE SITE OR THE SERVICE WILL BE UNINTERRUPTED, TIMELY, SECURE OR ERROR-FREE, OR THAT DEFECTS IN THE SITE OR THE SERVICE WILL BE CORRECTED. Body &amp; Brain yoga DOES NOT WARRANT THE ACCURACY OR COMPLETENESS OF THE CONTENT, OR THAT ANY ERRORS IN THE CONTENT WILL BE CORRECTED. THE SITE, THE SERVICE AND THE CONTENT ARE PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS.</p>
	&nbsp;

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">ALL PRODUCTS AND SERVICES PURCHASED ON OR THROUGH THIS SITE ARE SUBJECT ONLY TO ANY APPLICABLE WARRANTIES OF THEIR RESPECTIVE MANUFACTURES, DISTRIBUTORS AND SUPPLIERS, IF ANY. TO THE FULLEST EXTENT PERMISSIBLE BY APPLICABLE LAW, WE HEREBY DISCLAIM ALL WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, ANY IMPLIED WARRANTIES WITH RESPECT TO THE PRODUCTS AND SERVICES LISTED OR PURCHASED ON OR THROUGH THIS SITE. WITHOUT LIMITING THE GENERALITY OF THE FOREGOING, WE HEREBY EXPRESSLY DISCLAIM ALL LIABILITY FOR PRODUCT DEFECT OR FAILURE, CLAIMS THAT ARE DUE TO NORMAL WEAR, PRODUCT MISUSE, ABUSE, PRODUCT MODIFICATION, IMPROPER PRODUCT SELECTION, NON-COMPLIANCE WITH ANY CODES, OR MISAPPROPRIATION. WE MAKE NO WARRANTIES TO THOSE DEFINED AS "CONSUMERS" IN THE MAGNUSON-MOSS WARRANTY-FEDERAL TRADE COMMISSION IMPROVEMENTS ACT. THE FOREGOING EXCLUSIONS OF IMPLIED WARRANTIES DO NOT APPLY TO THE EXTENT PROHIBITED BY LAW. PLEASE REFER TO YOUR LOCAL LAWS FOR ANY SUCH PROHIBITIONS.</p>
	</li>
	<li style="margin: 0px 0px 15px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline;">
	<p class="list_title" style="margin: 0px 0px 3px; padding: 0px; border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">11. Limitation of Liability</p>

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">IN NO EVENT WILL Body &amp; Brain yoga BE LIABLE FOR ANY DAMAGES WHATSOEVER, INCLUDING, BUT NOT LIMITED TO ANY DIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL, EXEMPLARY OR OTHER INDIRECT DAMAGES ARISING OUT OF (I) THE USE OF OR INABILITY TO USE THE SITE, THE SERVICE, OR THE CONTENT, (II) ANY TRANSACTION CONDUCTED THROUGH OR FACILITATED BY THE SITE; (III) ANY CLAIM ATTRIBUTABLE TO ERRORS, OMISSIONS, OR OTHER INACCURACIES IN THE SITE, THE SERVICE AND/OR THE CONTENT, (IV) UNAUTHORIZED ACCESS TO OR ALTERATION OF YOUR TRANSMISSIONS OR DATA, OR (V) ANY OTHER MATTER RELATING TO THE SITE, THE SERVICE, OR THE CONTENT, EVEN IF Body &amp; Brain yoga HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. IF YOU ARE DISSATISFIED WITH THE SITE, THE SERVICE, THE CONTENT, OR WITH THE TERMS OF USE, YOUR SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE USING THE SITE. BECAUSE SOME STATES DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, SOME OF THE ABOVE LIMITATIONS MAY NOT APPLY TO YOU. IN SUCH STATES, Body &amp; Brain yoga'S LIABILITY IS LIMITED AND WARRANTIES ARE EXCLUDED TO THE GREATEST EXTENT PERMITTED BY LAW, BUT SHALL, IN NO EVENT, EXCEED $100.00.</p>
	</li>
	<li style="margin: 0px 0px 15px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline;">
	<p class="list_title" style="margin: 0px 0px 3px; padding: 0px; border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">12. Indemnification</p>

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">You understand and agree that you are personally responsible for your behavior on the Site. You agree to indemnify, defend and hold harmless Body &amp; Brain yoga, its parent companies, subsidiaries, affiliated companies, joint venturers, business partners, licensors, employees, agents, and any third-party information providers to the Service from and against all claims, losses, expenses, damages and costs (including, but not limited to, direct, incidental, consequential, exemplary and indirect damages), and reasonable attorneys' fees, resulting from or arising out of your use, misuse, or inability to use the Site, the Service, or the Content, or any violation by you of this Agreement.</p>
	</li>
	<li style="margin: 0px 0px 15px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline;">
	<p class="list_title" style="margin: 0px 0px 3px; padding: 0px; border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">13. Privacy</p>

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">Click&nbsp;<a href="http://www.bodynbrain.com/terms-of-use" style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline; color: rgb(21, 132, 195);">here</a>&nbsp;to see&nbsp;<a href="http://www.bodynbrain.com/terms-of-use" style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline; color: rgb(21, 132, 195);">Body &amp; Brain yoga's Privacy Policy.</a></p>
	</li>
	<li style="margin: 0px 0px 15px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline;">
	<p class="list_title" style="margin: 0px 0px 3px; padding: 0px; border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">14. User Conduct</p>

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">You agree to use the Site only for lawful purposes. You agree not to take any action that might compromise the security of the Site, render the Site inaccessible to others or otherwise cause damage to the Site or the Content. You agree not to add to, subtract from, or otherwise modify the Content, or to attempt to access any Content that is not intended for you. You agree not to use the Site in any manner that might interfere with the rights of third parties.</p>
	</li>
	<li style="margin: 0px 0px 15px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline;">
	<p class="list_title" style="margin: 0px 0px 3px; padding: 0px; border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">15. Unsolicited Idea Submission Policy</p>

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">Body &amp; Brain yoga or any of its employees do not accept or consider unsolicited ideas, including ideas for new advertising campaigns, marketing strategies, new or improved products, technologies, services, processes, materials, or new product names. We have found this policy necessary in order to avoid misunderstandings should Body &amp; Brain yoga's business activities bear coincidental similarities with one or more of the thousands of unsolicited ideas offered to us. Please do not send your unsolicited ideas to Body &amp; Brain yoga or anyone at Body &amp; Brain yoga. If, in spite of our request that you not send us your ideas, you still send them, then regardless of what your posting, email, letter, or other transmission may say, (1) your idea will automatically become the property of Body &amp; Brain yoga, without any compensation to you; (2) Body &amp; Brain yoga will have no obligation to return your idea to you or respond to you in any way; (3) Body &amp; Brain yoga will have no obligation to keep your idea confidential; and (4) Body &amp; Brain yoga may use your idea for any purpose whatsoever, including giving your idea to others. However, Body &amp; Brain yoga does welcome feedback regarding many areas of Body &amp; Brain yoga's existing businesses that will help satisfy customer's needs, and feedback can be provided through the many listed contact areas on the Site. Any feedback you provide shall be deemed a Submission under the terms in the User Supplied Information section above.</p>
	</li>
	<li style="margin: 0px 0px 15px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline;">
	<p class="list_title" style="margin: 0px 0px 3px; padding: 0px; border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">16. User Supplied Information</p>

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">Body &amp; Brain yoga does not want to receive confidential or proprietary information from you via the Site. You agree that any material, information, or data you transmit to us or post to the Site (each a "Submission" or collectively "Submissions") will be considered non confidential and non proprietary. For all Submissions, (1) you guarantee to us that you have the legal right to post the Submission and that it will not violate any law or the rights of any person or entity, and (2) you give Body &amp; Brain yoga the royalty-free, irrevocable, perpetual, worldwide right to use, distribute, display and create derivative works from the Submission, in any and all media, in any manner, in whole or in part, without any restriction or responsibilities to you.</p>
	</li>
	<li style="margin: 0px 0px 15px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline;">
	<p class="list_title" style="margin: 0px 0px 3px; padding: 0px; border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">17. Password Security</p>

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">If you register to become a DahnYoga.com member, you are responsible for maintaining the confidentiality of your member identification and password information, and for restricting access to your computer. You agree to accept responsibility for all activities that occur under your member identification and password.</p>
	</li>
	<li style="margin: 0px 0px 15px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline;">
	<p class="list_title" style="margin: 0px 0px 3px; padding: 0px; border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">18. General Provisions</p>

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">a. Entire Agreement/No Waiver. These Terms of Use constitute the entire agreement of the parties with respect to the subject matter hereof. No waiver by Body &amp; Brain yoga of any breach or default hereunder shall be deemed to be a waiver of any preceding or subsequent breach or default.</p>
	&nbsp;

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">b. Correction of Errors and Inaccuracies. The Content may contain typographical errors or other errors or inaccuracies and may not be complete or current. Body &amp; Brain yoga therefore reserves the right to correct any errors, inaccuracies or omissions and to change or update the Content at any time without prior notice. Body &amp; Brain yoga does not, however, guarantee that any errors, inaccuracies or omissions will be corrected.</p>
	&nbsp;

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">c. Enforcement/ Choice of Law/ Choice of Forum. If any part of this Agreement is determined by a court of competent jurisdiction to be invalid or unenforceable, it will not impact any other provision of this Agreement, all of which will remain in full force and effect. Any and all disputes relating to this Agreement, Body &amp; Brain yoga's Privacy Policy, your use of the Site, any other Body &amp; Brain yoga web site, the Service, or the Content are governed by, and will be interpreted in accordance with, the laws of the State of Arizona, without regard to any conflicts of laws provisions.</p>
	</li>
	<li style="margin: 0px 0px 15px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline;">
	<p class="list_title" style="margin: 0px 0px 3px; padding: 0px; border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(51, 51, 51);">19. Refund Policy</p>

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">At DahnYoga.com we value our relationship with you and hope to provide the highest quality services and information. If at any time you feel that our services do not meet your needs or you have changed your mind about reserving an appointment at one of our locations, you can request a refund within the applicable 30 day period.</p>
	&nbsp;

	<p style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: 13px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 18px; vertical-align: baseline; color: rgb(102, 102, 102);">a. Refunds are only accepted for transactions completed via www.DahnYoga.com.<br>
	b. Refunds may be requested up to 30 days from the date of issuance of a Transaction ID.<br>
	c. Any deviation from the return policy outlined above could result in a delay of your refund.<br>
	d. If you have any questions about your Refund Request or our Refund Policy, please contact<a href="mailto:customerservice@bodynbrain.com" style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline; color: rgb(21, 132, 195);">customerservice@dahnyoga.com.</a></p>
	</li>
</ol>
                                
				</div> <!-- end of terms -->
				<div class="terms_check" id="gototerm">
					<div class="check_wrap" >
						<input id="chbTerms" type="checkbox" name="chbTerms" ng-init="acceptterm = false" ng-model="acceptterm"><label for="chbTerms"> I accept the Terms of Use</label>
						<span id="CustomValidatorTerms" style="color:Red;" ng-show="checkthis">(Please Accept Terms of Use!)</span>
					</div>

				</div> <!-- end of terms check -->


				<!-- order summary -->
				<h5 class="sub_title2"><span>Order</span> summary</h5>
				<div class="form_box summary_wrap">
					<p class="summary_title">{[{sumsession}]}</p>
					<ul>
						<li><span class="lb">Name :</span>{[{sumname}]}</li>
						<li><span class="lb">preferred Location :</span>{[{sumcentername}]} {[{sumlocation}]},{[{sumzip}]}</li>
						<li><span class="lb">Expected Date &amp; Time:</span>{[{sumdate}]} {[{sumtime}]}</li>
						<li><span class="lb">Amount to be charged to your card :</span>${[{sumtotal}]}</li>
					</ul>
				</div> <!-- end of summary_warp -->




				<div class="btnwrap">
					<input type="submit" name="btnSubmit" value="Submit" id="btnSubmit" class="btn_submit">
					<p class="btn_desc">The confirmation page may take several seconds to appear.</p>
				</div> <!-- end btn wrap -->

			</div> <!-- end of payment_wrap -->

		</div>
	</div>
	</div>
	</form>




	<div class="getstarted finalstep" ng-if="finalstep">
		<span class="title"  id="non-printable">Beneplace Packages</span>
		<!-- state class on -->
		<ul class="state"  id="non-printable">
			<li><p class="program">Choose Program</p></li>
			<li class="arrow"><img src="/img/frontend/ico_state_arrow.gif" alt=""></li>
			<li><p class="location">Find Location</p></li>
			<li class="arrow"><img src="/img/frontend/ico_state_arrow.gif" alt=""></li>
			<li><p class="schedule">Schedule Session &amp; Class</p></li>
			<li class="arrow"><img src="/img/frontend/ico_state_arrow.gif" alt=""></li>
			<li class="last on"><p class="confirm">Confirm Your Appointment</p></li>
		</ul>

		<!-- choose program -->
		<div class="box_wrap">
			<div class="inner_wrap">
				<span class="sub_title" id="non-printable">
					<p class="graytxt">Dear <span class="ready_txt">{[{fullname}]}</span>,</p>
				</span>
				<div class="program_wrap">
					<div  id="non-printable">
						<p class="graytxt minwidth">Thank you for your purchase with Body & Brain Yoga! You made an appointment at the <span class="bluetext">{[{centertitle}]}</span> center on <span class="blacktext">{[{scheddate | date: 'fullDate'}]}</span> at <span class="blacktext">{[{schedhour}]}</span>. One of our friendly center staff will contact you shortly to confirm your appointment. At the time of your session, we recommend loosely-fitted clothing and socks. No yoga mats are necessary.</p>
						<br>
						<p class="graytxt minwidth">If you have any questions, you can reach us at:<br>Phone: <span class="blacktext">{[{centerphone}]}</span> or Email: <a href="mailto:{[{centeremail}]}">{[{centeremail}]}</a><br>Confirmation Number is <span class="blacktext">{[{confirmnumber}]}</span><br>Starter Package: <span class="bluetext">{[{sessionclass}]}</span></p>
						<br>
						<br>
						<div class="printwrap">
						<button type="submit" onclick="myFunction()" name="btnSubmit" value="Submit" id="btnSubmit" class="btn_print"><p class="printicospan">print</p></button>
						
						</div> <!-- end btn wrap -->
					</div>
					<div class="printdetails" id="printable">

						<div class="dahnyogalogo"><img id="dahnyogalogo" src="/img/bnblogo.gif"></div>

						<p class="bigred">Your Appointment</p>

						<p class="graytxt">Starter Package: <span class="blacktext">{[{sessionclass}]}</span></p>

						<p class="graytxt">Name: <span class="blacktext">{[{fullname}]}</span></p>

						<p class="graytxt">When: <span class="blacktext">{[{scheddate | date: 'fullDate'}]}, {[{schedhour}]} </span><em>(Your appointment is subject to availability.)</em></p>

						<p class="graytxt">Where: <span class="bluetext">{[{centertitle}]}</span><br>
													<em>{[{centeraddress}]}, {[{centerzip}]}</em><br>
													<em>Phone:{[{centerphone}]}</em>
						</p>

						<p class="graytxt">Confirmation Number: <span class="blacktext">{[{confirmnumber}]}</span></p>
						
						<p class="graytxt">Payment: <span class="blacktext">Paid ${[{totalprice}]} {[{paymenttype}]}</span></p>

						<div class="result_map">
							<div id="map_canvas">
								<ui-gmap-google-map center="map.center" zoom="map.zoom" draggable="true" options="options" bounds="map.bounds">
									<ui-gmap-markers models="randomMarkers" coords="'self'"  icon="'icon'">
										<ui-gmap-windows show="show">
							                <div ng-non-bindable>{[{title}]}</div>
							            </ui-gmap-windows>
									</ui-gmap-markers>
								</ui-gmap-google-map>
					        </div>
						</div>
					</div>

				</div>
			</div>
			<!-- end of first step -->
		</div>

	</div>


</div> <!-- end of controller -->

<script language="javascript">

	function onlyAlphabets(){
		var textInput = document.getElementById("txtComment").value;
		textInput = textInput.replace(/[^a-zA-Z0-9\s\.\?]/g, "");
		document.getElementById("txtComment").value = textInput;
	}

	function isNumberKey(evt){
		var charCode = (evt.which) ? evt.which : event.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	}
</script>



<style>

.loaders,
.loaders:before,
.loaders:after {
  background: #ffffff;
  -webkit-animation: load1 1s infinite ease-in-out;
  animation: load1 1s infinite ease-in-out;
  width: 1em;
  height: 4em;
}
.loaders:before,
.loaders:after {
  position: absolute;
  top: 0;
  content: '';
}
.loaders:before {
  left: -1.5em;
  -webkit-animation-delay: -0.32s;
  animation-delay: -0.32s;
}
.loaders {
  text-indent: -9999em;
  margin: 250px auto;
  position: relative;
  font-size: 11px;
  -webkit-transform: translateZ(0);
  -ms-transform: translateZ(0);
  transform: translateZ(0);
  -webkit-animation-delay: -0.16s;
  animation-delay: -0.16s;
}
.loaders:after {
  left: 1.5em;
}
@-webkit-keyframes load1 {
  0%,
  80%,
  100% {
    box-shadow: 0 0 #ffffff;
    height: 4em;
  }
  40% {
    box-shadow: 0 -2em #ffffff;
    height: 5em;
  }
}
@keyframes load1 {
  0%,
  80%,
  100% {
    box-shadow: 0 0 #ffffff;
    height: 4em;
  }
  40% {
    box-shadow: 0 -2em #ffffff;
    height: 5em;
  }
}

.txtloading{
	width: 100%;
	overflow: hidden;
	position:fixed;
	margin: 10px auto;
	top:350px;
}
.loadingtext{
	width: 200px;
	margin: 10px auto;
	top:350px;
}

</style>

<div class="chrono hidden"  id="non-printable">
	<div class="loaders">Loading...</div>
	<div class="txtloading">
		<div class="white_text loadingtext"><center>Loading Please wait...</center></div>
	</div>
</div>