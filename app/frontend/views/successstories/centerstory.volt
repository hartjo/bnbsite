<?php echo $this->getContent()?>
<style>

</style>
<?php
if($logoimage->value1 == 1){
  header('Location: /../../maintenance/');
}
?>
<div id="crumbs_cont">
  <ul id="crumbs">
    <li class="crumbsli"><a class="crumbs red_text" href="/">Home</a></li>
    <li class="crumbsli"><a class="red_text" href="/success-stories/1" style="font-size:12px">Success Stories</a></li>
  </ul>
</div>

<div class="container-fluid padding_top_md" ng-controller="WriteCtrl">

 	<fieldset class="borderless" ng-disabled="isSaving" ng-init="state_code('<?php echo $state; ?>')">
	  <div class="row">
	  	<div class="col s12">
				<section id="special_page_sidecontent">
				  <div id="special_page_sidebarmenu">
				    <div class="sidebar-head">B&B Yoga helped my...</div>
				    <ul class="sidebarmenu-list">
				    	<li><a href="/success-stories/stress/1"><label class="dot"> &#x25AA; </label>stress</a></li>
				    	<li><a href="/success-stories/peace/1"><label class="dot"> &#x25AA; </label>peace</a></li>
				    	<li><a href="/success-stories/pain/1"><label class="dot"> &#x25AA; </label>pain</a></li>
				    	<li><a href="/success-stories/flexibility/1"><label class="dot"> &#x25AA; </label>flexibility</a></li>
				    	<li><a href="/success-stories/meditation/1"><label class="dot"> &#x25AA; </label>meditation</a></li>
				    	<li><a href="/success-stories/strength/1"><label class="dot"> &#x25AA; </label>strength</a></li>
				    	<li><a href="/success-stories/anxiety/1"><label class="dot"> &#x25AA; </label>anxiety</a></li>
				    	<li><a href="/success-stories/focus/1"><label class="dot"> &#x25AA; </label>focus</a></li>
				    	<li><a href="/success-stories/back-pain/1"><label class="dot"> &#x25AA; </label>back pain</a></li>
				    	<li><a href="/success-stories/joint-pain/1"><label class="dot"> &#x25AA; </label>joint pain</a></li>
				    </ul>
				  </div>

				  <a href="#" class="a_share_your_experience">
				  	<img src="/img/frontend/share.gif"> <br>
				    Share <br> Your Experience
				  </a>

				  <div id="newsletter">
				    <div class="frmtitle">e-Newsletter</div>
					  <form action="//bodynbrain.us3.list-manage.com/subscribe/post?u=cfc22757143336e0cfcf90eef&amp;id=eef0458b00" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
			            <input type="email" value="" name="EMAIL" class="required email text" id="mce-EMAIL" placeholder="E-mail Address">
			            <input type="submit" value="" name="subscribe" id="mc-embedded-subscribe" class="send">
			          </form>
					<div class="clearboth"></div>
				  </div>
				</section>

				<section id="special_page_body">
				 <form ng-submit="write(testimony)" class="form-validation ng-pristine ng-invalid ng-invalid-required" name="writeform" onsubmit="return checkform(this);">
				  <label class="grey_title" >Success Stories</label><br>
				  <label class="Title">Share Your Experience </label><br>

			  	Tell others how you’ve benefited from Body & Brain yoga
			  	<br>
			  	Thank you for taking the time to share your experiences with Body & Brain yoga. This is a place where you can tell other people how Body & Brain yoga practice has helped you. It would be great if you could share something about your background, like how long you’ve been practicing and your condition before you started. If possible, please provide a photo of yourself or allow us to use a photo of you that we may have on file. 
				  <hr class="hr_thin">

				  <div class="col s2">
				  	<label class="xs_dot"> &#x25AA; </label>Name
				  </div>
				  <div class="col s10 form-group">
				  	<input type="text" name="fullname" ng-model="testimony.fullname" class="form_element_normal" required> 
				  	<span class="red_text" ng-show="writeform.fullname.$error.required">*</span>
					</div>

					<div class="col s2">
				  	<label class="xs_dot"> &#x25AA; </label>Age
				  </div>
				  <div class="col s10 form-group">
				  	<input type="text" name="age" ng-model="testimony.age" class="form_element_normal form_element_small" maxlength="3">
					</div>

					<div class="col s2">
				  	<label class="xs_dot"> &#x25AA; </label>Email
				  </div>
				  <div class="col s10 form-group">
				  	<input type="text" name="email" ng-model="testimony.email" class="form_element_normal" required> 
				  	<span class="red_text" ng-show="writeform.email.$error.required">*</span>
					</div>

					<div class="col s2">
				  	<label class="xs_dot"> &#x25AA; </label>Center
				  </div>
				  <div class="col s10 form-group">
				  	<select ng-model="testimony.state" ng-init="testimony.state = '<?php echo $state; ?>' " class="form_element_normal" ng-change="centerViaState(testimony.state)" ng-options="stateValue.state_code as stateValue.state for stateValue in states" required>
<!-- 				  		<option selected="selected" value="">State</option> -->
							<!-- <option ng-repeat="stateValue in states" value="{[{stateValue.state_code}]}">{[{stateValue.state_code}]}</option> -->
				  	</select>

				  	<select ng-model="testimony.center" class="form_element_normal" ng-options="center.centerid as center.centertitle for center in centers">
				  		<!-- <option selected="selected" value="">Center</option> -->
				  		<!-- <option ng-repeat="center in centers" value="{[{center.centerid}]}">{[{center.centertitle}]}</option> -->
				  	</select>

				  	<br><br>
				  	Or <input type="text" ng-model="testimony.othercenter" class="form_element_normal form_element_medium" placeholder="If you can't find your center, type here">
					</div>

					<div class="col s2">
				  	<label class="xs_dot"> &#x25AA; </label>Photo
				  </div>
				  <div class="col s10 form-group">
				  	<input type="file" name="photo" ng-model="testimony.photo" class="form_element_normal" class="form-control" ngf-select ngf-allow-dir="true" onchange="readURL(this);" required>
				  	<span class="red_text" ng-show="writeform.photo.$error.required">* Required</span>
				  	<br>
				  	<img src="" id="photopreview"/>
					</div>

				  <div class="col s12 form-group">
				  	<br><br>
				  	<label class="xs_dot"> &#x25AA; </label>What's your story all about 
				  	<span class="red_text" ng-show="writeform.subject.$error.required">*</span><br>
				  	<input type="text" name="subject" ng-model="testimony.subject" class="form_element_normal form_element_semi_full" required>
				  	
					</div>

					<div class="col s12 form-group">
				  	<label class="xs_dot"> &#x25AA; </label>Share the details here &nbsp;&nbsp;
				  	<span ng-hide="writeform.details.$error.required">({[{ contentcounter }]} characters)</span>
				  	<em class="red_text" ng-show="contentcounter < 200">(200 minimum)</em>
				  	<span class="red_text" ng-show="writeform.details.$error.required">*</span><br>
				  	<textarea name="details" ng-change="detailscounter(testimony.details)" ng-model="testimony.details" class="form_element_normal form_textarea" ng-trim="false" required></textarea> 
				  	<br>
	          <br>
	          <center>

	          	<table class="tablengcontact2">
	          		<tr>
	          			<td class="captcha" width="20%"><span id="txtCaptchaDiv" class="captchatxt"></span>
	          				<input type="hidden" id="txtCaptcha" ng-model="str1" name="captcha" />
	          			</td>
	          			<td width="50%"><input type="text" name="txtInput" id="txtInput" size="15" class="input_text2" placeholder="Type in the characters in the image" ng-model="str2" required name="captcha2" width="248px" /><span class="import">*</span>
	          			</td>
	          		</tr>
	          	</table>
	          <span class="red_text" ng-show="writeform.captchaname.$error.required">*</span>
	          <br><br>
	          <span class="red_text" ng-show="detailserror">Details is too short. Minimum of 200 characters</span>
	          <br>
	          <button type="submit" id="formsubmit" class="btn_lg bg_lime semi_bold white_text" ng-disabled="writeform.$invalid">Submit</button>
						</center>
					</div>

				</section>

				<section id="special_page_rightsidebar">
					<a href="/getstarted/starterspackage">
						<img src="/img/frontend/banner_getstarted_small.jpg" class="full_width">
					</a>
				</section>
	    </div> <!-- end of col s12 -->
	  </div> <!-- end of row -->
 	</fieldset>
 </form>
 <script type="text/javascript">
          function checkform(theform){
            var why = "";

            if(theform.txtInput.value == ""){
              why += "Captcha code should not be empty";
            }
            if(theform.txtInput.value != ""){
              if(ValidCaptcha(theform.txtInput.value) == false){
                why += "Captcha code did not match";
              }
            }
            if(why != ""){
              alert(why);
              return false;
            }
          }

          //Generates the captcha function

          function randomString() {
            var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ";
            var string_length = 6;
            var randomstring = '';
            for (var i=0; i<string_length; i++) {
              var rnum = Math.floor(Math.random() * chars.length);
              randomstring += chars.substring(rnum,rnum+1);
            }
            return randomstring;
          }

          var code = randomString();

          document.getElementById("txtCaptcha").value = code;
          document.getElementById("txtCaptchaDiv").innerHTML = code;

          // Validate the Entered input aganist the generated security code function
          function ValidCaptcha(){
            var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
            var str2 = removeSpaces(document.getElementById('txtInput').value);
            if (str1 == str2){
              return true;
            }else{
              return false;
            }
          }
          // Remove the spaces from the entered and generated code
          function removeSpaces(string){
            return string.split(' ').join('');
          }
          </script>
</div> <!-- end of container -->

<style>

</style>

<div class="chrono hidden">
	<div class="loader">Loading...</div>
	<div id="writeModal" class="modal writeModal hidden">
	    <div class="modal-content">
	      <h4>Testimonial</h4>
	      <p>Thank you so much for sharing your experience with us!. Please check your email.  <br><br> -The BodynBrain.com Team</p>
	    </div>
	    <div class="modal-footer">
	      <a id="modalOK" class=" modal-action modal-close waves-effect waves-green btn-flat" onclick='setTimeout("location.reload(true);",2000);'>OK</a>
	    </div>
	  </div>
</div>