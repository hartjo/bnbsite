<?php
{#$TZ = date_default_timezone_set($timezone); // http://sourceforge.net/projects/zip2timezone/#}
echo $this->getContent();
if($pagestatus != null) {

	$center = $centerpage->centerprop;
	$centernews = $centerpage->centernews;
	$centerphonenumber = $centerpage->centerphonenumber->phonenumber;
	$centerLAT = $centerpage->centerlocation->lat;
	$centerLON = $centerpage->centerlocation->lon;
	$centerslugs = $center->centerslugs;

?>
<?php
if($logoimage->value1 == 1){
  header('Location: /../../maintenance/');
}
?>
<div ng-controller="mapCtrl">
<h3 class="ubuntu"><?php echo $center->centertitle;  ?></h3>

<div style="overflow:hidden">
	<div class="body_8">
    <?php if(count($centerpage->centerimages) != 0){ ?>
		<ul class="pgwSlideshow">
	    <?php foreach($centerpage->centerimages as $image) { ?>
				<li><img src="<?php echo $this->config->application->amazonlink . '/uploads/center/' . $centerpage->centerprop->centerid . '/' . $image->imagename; ?>"  alt=""></li>
		  <?php } ?>
		</ul>
    <?php } else {
            echo '<span class="warning_message">No Available Images for Slider</span>';
          } ?>
    <span class="font_85 dev_text bl"><?php echo $center->centerdesc ?></span>
		<br><br>

    <div class="side_4_flex_wrapper hidden_sidebar">
      <div class="side_4_half">
        <a href="/getstarted/starterspackage/<?php echo $center->centerid; ?>">
        <img src="/img/frontend/online-special-pricing.jpg" class="full_width responsive_margin">
        </a>
        <br>
        <center>
        <div class="flexy responsive_margin">
          <div class="trio">
            <a href="" data-target="modal-s_h" id="modal-schedule_h">
              <img src="/img/frontend/btn_schedule_on.gif" class="full_width">
            </a>
          </div>
          <div class="trio"><a href="" data-target="modal-p_h" id="modal-pricing_h"><img src="/img/frontend/btn_pricing_on.gif" class="full_width"></a></div>
          <div class="trio"><a href="" data-target="modal-m_h" id="modal-map_h" ng-click="openmap()"><img src="/img/frontend/btn_map_on.gif" class="full_width"></a></div>
        </div>
        <div class="bg_full_width valign-wrapper" id="centerpage_callus" style="background:url('/img/frontend/callus.gif');">
          <h5 class="font_bold responsive_text"><?php echo $centerphonenumber; ?></h5>
        </div>
        </center>
      </div> <!-- END OF HALF -->
      <div class="side_4_half">
        <div class="wrapper_default">
          <span class="size_18 font_bolder">HOURS</span>
          <br>
          <!-- <label style="margin-left:70px"><b><u><?php  date('D h:i A')."";?></u></b></label> -->
          <br>
          <?php
            $days = array("mon","tue","wed","thu","fri");
            $daynow = date('D');

            //LOOPING MONDAY TO FRIDAY
            foreach($days as $day) {
              $timestart = $day."timestart";
              $formatstart = $day."formatstart";
              $timeend = $day."timeend";
              $formatend = $day."formatend";

              if(ucwords($day) === $daynow) { echo "<b>"; }
          ?>
              <span class="red_text inbl" style="width:30px;"><?php echo ucwords($day); ?></span>
                <span class="dev_text margin_left_s">
                  <?php echo $centerhours->$timestart." ".$centerhours->$formatstart; ?>  ~
                  <?php echo $centerhours->$timeend." ".$centerhours->$formatend; ?>
                </span>
          <?php
                  if(ucwords($day) === $daynow) { echo "</b>";
                    $start = date('Y-m-d'). " ".$centerhours->$timestart.":00 ".$centerhours->$formatstart;
                    $end = date('Y-m-d'). " ".$centerhours->$timeend.":00 ".$centerhours->$formatend;
                    $newstart = date_create($start);
                    $newstart = date_format($newstart, 'Y-m-d H:i:s');
                    $newend = date_create($end);
                    $newend = date_format($newend, 'Y-m-d H:i:s');

                    $datenow = date('Y-m-d H:i:s');
                    if($datenow > $newstart && $datenow < $newend) {
                       "<span class='bluish_text'>Open</span>";
                    } else {
                       "<span class='bluish_text'>Closed</span>";
                    }
                  }
                ?>
                <br>
        <?php }
            if($centerhours->satstatus != '0' ) {
              if('Sat' === $daynow) { echo "<b>"; }
        ?>
              <span class="red_text inbl" style="width:30px;">Sat</span>
                <span class="dev_text margin_left_s">
                  <?php echo $centerhours->sattimestart." ".$centerhours->satformatstart; ?>  ~
                  <?php echo $centerhours->sattimeend." ".$centerhours->satformatend;; ?>
                </span>
                <?php
                  if('Sat' === $daynow) {
                    echo "</b>";
                    $start = date('Y-m-d'). " ".$centerhours->sattimestart.":00 ".$centerhours->satformatstart;
                    $end = date('Y-m-d'). " ".$centerhours->sattimeend.":00 ".$centerhours->satformatend;
                    $newstart = date_create($start);
                    $newstart = date_format($newstart, 'Y-m-d H:i:s');
                    $newend = date_create($end);
                    $newend = date_format($newend, 'Y-m-d H:i:s');

                    $datenow = date('Y-m-d H:i:s');
                    if($datenow > $newstart && $datenow < $newend) {
                       "<span class='bluish_text'>Open</span>";
                    } else {
                       "<span class='bluish_text'>Closed</span>";
                    }
                  }
                ?>
                <br>
        <?php   }
            if($centerhours->sunstatus != '0' ) {
              if('Sun' === $daynow) { echo "<b>"; }
        ?>
              <span class="red_text inbl" style="width:30px;">Sun</span>
                <span class="dev_text margin_left_s">
                  <?php echo $centerhours->suntimestart." ".$centerhours->sunformatstart; ?> ~
                  <?php echo $centerhours->suntimeend." ".$centerhours->sunformatend; ?>
                </span>
                <?php
                  if('Sun' === $daynow) {
                    echo "</b>";
                    $start = date('Y-m-d'). " ".$centerhours->suntimestart.":00 ".$centerhours->sunformatstart;
                    $end = date('Y-m-d'). " ".$centerhours->suntimeend.":00 ".$centerhours->sunformatend;
                    $newstart = date_create($start);
                    $newstart = date_format($newstart, 'Y-m-d H:i:s');
                    $newend = date_create($end);
                    $newend = date_format($newend, 'Y-m-d H:i:s');

                    $datenow = date('Y-m-d H:i:s');
                    if($datenow > $newstart && $datenow < $newend) {
                       "<span class='bluish_text'>Open</span>";
                    } else {
                       "<span class='bluish_text'>Closed</span>";
                    }
                  }
                ?>
        <?php   } ?>
          <h5 class="size_18 font_bolder">ADDRESS</h5>
          <span class="dev_text">
            <?php echo $center->centeraddress; ?> <br>
            <?php echo $center->centercity . ", " . $center->centerstate . " $center->centerzip"; ?>
          </span>
          <br>
          <?php
            if($center->spnl == 1) {
              echo "<br><span class='bl bluish_text'>¡Se Habla Español!</span>";
            }
          ?>
          <br>
          <img src="/img/frontend/mail.gif">
          <a href="#" class="bluish_text"><?php echo $email;?></a>
          <br>
        </div>
      </div> <!-- END OF 2ND HALF -->
    </div> <!-- END OF SIDE 4 FLEX WRAPPER -->

    <?php if($centernews != '') { ?>
		<h5>News</h5>
		<span class="float_right margin_top-25px text_85"><a href="/<?php echo $centerslugs; ?>/blog/1" class="bluish_text"> View all posts &#x0203A;&#x0203A;</a></span>
		<div class="panel">
			<div class="panel_heading bg_red">
				<span class="size_18 font-bolder"><?php if($centernews != ''){echo $centernews->title;}?></span>
				<span class="text_85 float_right margin_top_5px"><?php if($centernews != ''){echo $centernews->date;}?></span>
			</div>
			<div class="panel_body overflow_hidden">
        <?php if($centernews->banner != ''){ ?>
        <img src="<?php echo $this->config->application->amazonlink .'/uploads/newsimage/'.$centernews->banner; ?>" class="full_width">
        <?php } ?>
        <?php echo $centernews->body; ?>

        {% if centerpage.centernews.author %}
          <span class="float_right">&mdash; {{centerpage.centernews.author}}</span>
        {% endif %}

			</div>
		</div>
		<br>
    <?php } ?>

    <?php $getginfo = $eventsdata;
      if(count($getginfo) != 0) { ?>
  		<h5>Calendar</h5>
  		 <a href="/<?php echo $centerslugs; ?>/calendar/1" class="bluish_text"><span class="float_right margin_top-25px bluish_text text_85">
       See all &#x0203A;&#x0203A;
      </span></a>
  		<!-- EVENT LISTING BEGINNING -->
  		<ul id="calendar_list" class="calendar_list">
           <?php
              foreach ($getginfo as $key => $value) {
                $date = date_create($getginfo[$key]->edate);
                $dayofweek = date_format($date, "l");
                $day = date_format($date, "j");
                $month = date_format($date, "n");

                if($getginfo[$key]->type == 'act'){
            ?>
            <li>
              <div class="date_wrap">
                <p class="week no-margin" style="font-size:16px"><?php echo  $dayofweek; ?></p>
                <p class="day no-margin"><?php echo  $month; ?>/<?php echo  $day; ?></p>
                <p class="time"><?php echo $getginfo[$key]->estarthour;  ?> <?php echo $getginfo[$key]->estarttimeformat;  ?> - <?php echo $getginfo[$key]->eendhour;  ?> <?php echo $getginfo[$key]->eendtimeformat;  ?></p>
              </div>
              <div class="text_wrap <?php echo $getginfo[$key]->eid;  ?>">
                <p class="title no-margin"><?php echo $getginfo[$key]->etitle;  ?></p>
                <p><?php echo $getginfo[$key]->edesc;  ?></p>
                <a href="" class="btncalendar expand" alt="<?php echo $getginfo[$key]->eid;?>" title="Expand">Expand</a>
              </div>
            </li>
            <?php } else{ ?>
            <li>
              <div class="date_wrap">
                <p class="week no-margin" style="font-size:16px"><?php echo  $dayofweek; ?></p>
                <p class="day no-margin"><?php echo  $month; ?>/<?php echo  $day; ?></p>
                <p class="time"><?php echo $getginfo[$key]->estarthour;  ?>:<?php echo $getginfo[$key]->estartminute;  ?> <?php echo $getginfo[$key]->estarttimeformat;  ?> - <?php echo $getginfo[$key]->eendhour;  ?>:<?php echo $getginfo[$key]->eendminute;  ?>  <?php echo $getginfo[$key]->eendtimeformat;  ?></p>
              </div>
              <div class="text_wrap <?php echo $getginfo[$key]->eid;  ?>">
                <p class="title no-margin"><?php echo $getginfo[$key]->etitle;  ?> <span><a id="rptEvent_ctl00_hlSignUp" href="/workshops/registration/<?php echo $getginfo[$key]->eid;  ?>"><img src="/img/frontend/search_signup.gif" alt="Sign UP"></a></span></p>
                <p class="no-margin" style="color:blue;">Venue : <a href="/<?php echo $getginfo[$key]->ecenterslugs; ?>">Body & Brain yoga <?php echo $getginfo[$key]->ecentertitle; ?> Center</a></p>
                <p><?php echo $getginfo[$key]->edesc;  ?></p>
                <a href="" class="btncalendar expand" alt="<?php echo $getginfo[$key]->eid;?>" title="Expand">Expand</a>
              </div>
            </li>


      <?php   }} ?>
      </ul> <!-- END OF EVENT LISTING -->
<?php } ?>
		<br>
	</div>
	<div class="side_4">
		<div class="side_4_flex_wrapper original_sidebar">
  		<div class="side_4_half">
        <a href="/getstarted/starterspackage/<?php echo $center->centerid; ?>">
  			<img src="/img/frontend/online-special-pricing.jpg" class="full_width responsive_margin">
        </a>
  			<br>
  			<center>
  			<div class="flexy responsive_margin">
  				<div class="trio">
            <a href="" data-target="modal-s" id="modal-schedule">
              <img src="/img/frontend/btn_schedule_on.gif" class="full_width">
            </a>
          </div>
  				<div class="trio"><a href="" data-target="modal-p" id="modal-pricing"><img src="/img/frontend/btn_pricing_on.gif" class="full_width"></a></div>
  				<div class="trio"><a href="" data-target="modal-m" id="modal-map" ng-click="openmap()"><img src="/img/frontend/btn_map_on.gif" class="full_width"></a></div>
  			</div>
  			<div class="bg_full_width valign-wrapper" id="centerpage_callus" style="background:url('/img/frontend/callus.gif');">
  				<h5 class="font_bold responsive_text"><?php echo $centerphonenumber; ?></h5>
  			</div>
  			</center>
  		</div> <!-- END OF HALF -->
  		<div class="side_4_half">
  			<div class="wrapper_default">
  				<span class="size_18 font_bolder">HOURS</span>
          <br>
          <!-- <label style="margin-left:70px"><b><u><?php  date('D h:i A')."";?></u></b></label> -->
          <br>
          <?php
            $days = array("mon","tue","wed","thu","fri");
            $daynow = date('D');

            //LOOPING MONDAY TO FRIDAY
            foreach($days as $day) {
              $timestart = $day."timestart";
              $formatstart = $day."formatstart";
              $timeend = $day."timeend";
              $formatend = $day."formatend";

              if(ucwords($day) === $daynow) { echo "<b>"; }
          ?>
  						<span class="red_text inbl" style="width:30px;"><?php echo ucwords($day); ?></span>
  							<span class="dev_text margin_left_s">
  								<?php echo $centerhours->$timestart." ".$centerhours->$formatstart; ?>  ~
  								<?php echo $centerhours->$timeend." ".$centerhours->$formatend; ?>
  							</span>
          <?php
                  if(ucwords($day) === $daynow) { echo "</b>";
                    $start = date('Y-m-d'). " ".$centerhours->$timestart.":00 ".$centerhours->$formatstart;
                    $end = date('Y-m-d'). " ".$centerhours->$timeend.":00 ".$centerhours->$formatend;
                    $newstart = date_create($start);
                    $newstart = date_format($newstart, 'Y-m-d H:i:s');
                    $newend = date_create($end);
                    $newend = date_format($newend, 'Y-m-d H:i:s');

                    $datenow = date('Y-m-d H:i:s');
                    if($datenow > $newstart && $datenow < $newend) {
                       "<span class='bluish_text'>Open</span>";
                    } else {
                       "<span class='bluish_text'>Closed</span>";
                    }
                  }
                ?>
                <br>
        <?php }
  					if($centerhours->satstatus != '0' ) {
              if('Sat' === $daynow) { echo "<b>"; }
        ?>
  						<span class="red_text inbl" style="width:30px;">Sat</span>
  							<span class="dev_text margin_left_s">
  								<?php echo $centerhours->sattimestart." ".$centerhours->satformatstart; ?>  ~
  								<?php echo $centerhours->sattimeend." ".$centerhours->satformatend;; ?>
  							</span>
                <?php
                  if('Sat' === $daynow) {
                    echo "</b>";
                    $start = date('Y-m-d'). " ".$centerhours->sattimestart.":00 ".$centerhours->satformatstart;
                    $end = date('Y-m-d'). " ".$centerhours->sattimeend.":00 ".$centerhours->satformatend;
                    $newstart = date_create($start);
                    $newstart = date_format($newstart, 'Y-m-d H:i:s');
                    $newend = date_create($end);
                    $newend = date_format($newend, 'Y-m-d H:i:s');

                    $datenow = date('Y-m-d H:i:s');
                    if($datenow > $newstart && $datenow < $newend) {
                       "<span class='bluish_text'>Open</span>";
                    } else {
                       "<span class='bluish_text'>Closed</span>";
                    }
                  }
                ?>
                <br>
  			<?php 	}
  					if($centerhours->sunstatus != '0' ) {
              if('Sun' === $daynow) { echo "<b>"; }
        ?>
  						<span class="red_text inbl" style="width:30px;">Sun</span>
  							<span class="dev_text margin_left_s">
  								<?php echo $centerhours->suntimestart." ".$centerhours->sunformatstart; ?> ~
  								<?php echo $centerhours->suntimeend." ".$centerhours->sunformatend; ?>
  							</span>
                <?php
                  if('Sun' === $daynow) {
                    echo "</b>";
                    $start = date('Y-m-d'). " ".$centerhours->suntimestart.":00 ".$centerhours->sunformatstart;
                    $end = date('Y-m-d'). " ".$centerhours->suntimeend.":00 ".$centerhours->sunformatend;
                    $newstart = date_create($start);
                    $newstart = date_format($newstart, 'Y-m-d H:i:s');
                    $newend = date_create($end);
                    $newend = date_format($newend, 'Y-m-d H:i:s');

                    $datenow = date('Y-m-d H:i:s');
                    if($datenow > $newstart && $datenow < $newend) {
                       "<span class='bluish_text'>Open</span>";
                    } else {
                       "<span class='bluish_text'>Closed</span>";
                    }
                  }
                ?>
  			<?php 	} ?>
  				<h5 class="size_18 font_bolder">ADDRESS</h5>
  				<span class="dev_text">
  					<?php echo $center->centeraddress; ?> <br>
  					<?php echo $center->centercity . ", " . $center->centerstate . " $center->centerzip"; ?>
  				</span>
          <br>
          <?php
            if($center->spnl == 1) {
              echo "<br><span class='bl bluish_text'>¡Se Habla Español!</span>";
            }
          ?>
  				<br>
  				<img src="/img/frontend/mail.gif">
  				<a href="#" class="bluish_text"><?php echo $email;?></a>
  				<br>
  			</div>
  		</div> <!-- END OF 2ND HALF -->
		</div> <!-- END OF SIDE 4 FLEX WRAPPER -->

      <div ng-controller="centerpageCtrl" ng-show="offersLen > 0">
        <br>
        <h5 class="no-margin float_left boldest">Special Offer<span ng-if="offersLen > 1">s</span>
        </h5>
        <br><br>
          <div class="couponBox" ng-repeat="offer in offers" ng-click="windowopen(offer.offerid);" style="background:url('<?php echo $this->config->application->amazonlink .'/uploads/offerimages/{[{offer.image}]}';?>') 50% 0 repeat-y;">
           <div class="msgBox">
              <div class="title">{[{offer.title}]}</div>
              <div class="des">{[{offer.description}]}</div>
            </div>
          </div>


                <!-- <div class="couponBox" onclick="window.open('/studio/coupon_pop.aspx?code=10476150831183425','Coupon','986','710','no','center');" style="background:url(/images/thumbnails/couponDefault2.jpg) 70% 0 repeat-y;">
                  <div class="msgBox">
                    <div class="title">MONTH of AUGUST - $25 UNLIMITED CLASSES FOR 1 WEEK WITH THIS COUPON</div>
                    <div class="des">Do something special for YOURSELF, try stress relief and clear mind through Deep Breathing, Meridian Stretching, Relaxation and Meditation for one week for $25 ONLY!!
                      *New Members ONLY
                    </div>
                  </div>

                </div> -->
      </div>

      <hr class="hr_dual">

      <div class="successstories-title-container">
  			<h5 class="no-margin float_left boldest">Success Stories</h5>
        <label class="inbl margin_top_7px margin_left_s">
          <a href="/success-stories/success-write/<?php echo $center->centerstate.'/'.$center->centerslugs; ?>" class="bluish_text">Share your experience &#x0203A;&#x0203A;</a>
        </label>
      </div>
	<?php 	foreach($centerpage->success as $success) {
          $story_slugs = preg_replace("![^a-z0-9]+!i", "-", strtolower($success->subject));
  ?>
			<div class="flex-container_center margin_bottom_s">
				<div class="flex-left_center">
					<div class="circleimages_centerpage">
            <a href="/<?php echo $center->centerslugs.'/story/'.$success->ssid.'/'.$story_slugs; ?>">
  						<div class="circle_centerpage bg_centered"
                    style="background:url('<?php

                                     $varheaders = get_headers($this->config->application->amazonlink . '/uploads/testimonialimages/'.$success->photo);
                                     if($varheaders[0] == 'HTTP/1.1 200 OK'){
                                      echo $this->config->application->amazonlink . '/uploads/testimonialimages/'.$success->photo;
                                    }
                                    else{
                                      echo '/img/blue-rose.jpg';
                                    }

                                    ?>')">
  						</div>
            </a>
					</div>
				</div>
				<div class="flex-right_center">
					<div class="padding_sides_2 bg_whitish">
						<h6 class="text_85">
            <a href="/<?php echo $center->centerslugs.'/story/'.$success->ssid.'/'.$story_slugs; ?>" class="dev_text">
              <?php echo $success->metadesc; ?>
            </a>
            </h6>
						<a href="/<?php echo $center->centerslugs.'/story/'.$success->ssid.'/'.$story_slugs; ?>" class="bluish_text font_90">read more</a>
					</div>
				</div>
			</div>
  <?php 	} ?>
		<hr class="hr_dual">
    <div ng-controller="centerpageCtrl" ng-show="socialexist">
  		<h5 class="text_center ">Like Us? Spread the Word!</h5>
      <div class="text_center">
        <a ng-href="{[{facebookurl}]}" target="_blank" ng-if="facebookurl">
          <img class="cursor_pointer" src="/img/frontend/ico_facebook.gif">
        </a>
        <a ng-href="{[{twitterurl}]}" target="_blank" ng-if="twitterurl">
          <img class="cursor_pointer" src="/img/frontend/ico_twitter.gif">
        </a>
        <a ng-href="{[{googleurl}]}" target="_blank" ng-if="googleurl">
          <img class="cursor_pointer" src="/img/frontend/ico_google.gif">
        </a>
        <a ng-href="{[{yelpurl}]}" target="_blank" ng-if="yelpurl">
          <img class="cursor_pointer" src="/img/frontend/sns_yepl.gif">
        </a>
      </div>
    </div>
	</div>
</div>
<!-- ========================= MODALS HERE ========================= -->

<!-- ========================= MAP ================================= -->
<div id="modal-m" class="modal overflow_hidden">
    <div class="modal-content no-margin">
      <div class="row no-margin">
      	<div class="col s12 m8 no-padding">
      		<div id="map_canvas">
      			<ui-gmap-google-map center="map.center" zoom="map.zoom" draggable="true" options="options">
	      			<ui-gmap-marker coords="marker.coords" options="marker.options" events="marker.events" idkey="marker.id">
	      			</ui-gmap-marker>
      			</ui-gmap-google-map>
      		</div>
      	</div>
      	<div class="col s12 m4 padding_s">
      	<img href="#!" src="/img/frontend/popup_close.png" class="float_right modal-close waves-effect cursor_pointer">
      	<span class="font_24_px">CENTER </span> <span class="font_24_px font_boldest teal_text"><?php echo $center->centertitle;?></span>
      	<br>
      	<h5 class="size_18 font_bolder">ADDRESS</h5>
			<span class="dev_text">
				<?php echo $center->centeraddress; ?> <br>
				<?php echo $center->centercity . ", " . $center->centerstate . " $center->centerzip"; ?>
			</span>

		<h5 class="size_18 font_bolder">PHONE</h5>
			<span class="dev_text">
				<?php echo $centerphonenumber; ?> <br>
			</span>

		<span class="size_18 font_bolder">HOURS</span>
          <br>
          <label style="margin-left:70px"><b><u><?php echo date('D h:i A')."";?></u></b></label>
          <br>
          <?php
            $days = array("mon","tue","wed","thu","fri");
            $daynow = date('D');

            //LOOPING MONDAY TO FRIDAY
            foreach($days as $day) {
              $timestart = $day."timestart";
              $formatstart = $day."formatstart";
              $timeend = $day."timeend";
              $formatend = $day."formatend";

              if(ucwords($day) === $daynow) { echo "<b>"; }
          ?>
              <span class="red_text inbl" style="width:30px;"><?php echo ucwords($day); ?></span>
                <span class="dev_text margin_left_s">
                  <?php echo $centerhours->$timestart." ".$centerhours->$formatstart; ?>  ~
                  <?php echo $centerhours->$timeend." ".$centerhours->$formatend; ?>
                </span>
          <?php
                  if(ucwords($day) === $daynow) { echo "</b>";
                    $start = date('Y-m-d'). " ".$centerhours->$timestart.":00 ".$centerhours->$formatstart;
                    $end = date('Y-m-d'). " ".$centerhours->$timeend.":00 ".$centerhours->$formatend;
                    $newstart = date_create($start);
                    $newstart = date_format($newstart, 'Y-m-d H:i:s');
                    $newend = date_create($end);
                    $newend = date_format($newend, 'Y-m-d H:i:s');

                    $datenow = date('Y-m-d H:i:s');
                    if($datenow > $newstart && $datenow < $newend) {
                       "<span class='bluish_text'>Open</span>";
                    } else {
                       "<span class='bluish_text'>Closed</span>";
                    }
                  }
                ?>
                <br>
        <?php }
            if($centerhours->satstatus != '0' ) {
              if('Sat' === $daynow) { echo "<b>"; }
        ?>
              <span class="red_text inbl" style="width:30px;">Sat</span>
                <span class="dev_text margin_left_s">
                  <?php echo $centerhours->sattimestart." ".$centerhours->satformatstart; ?>  ~
                  <?php echo $centerhours->sattimeend." ".$centerhours->satformatend;; ?>
                </span>
                <?php
                  if('Sat' === $daynow) {
                    echo "</b>";
                    $start = date('Y-m-d'). " ".$centerhours->sattimestart.":00 ".$centerhours->satformatstart;
                    $end = date('Y-m-d'). " ".$centerhours->sattimeend.":00 ".$centerhours->satformatend;
                    $newstart = date_create($start);
                    $newstart = date_format($newstart, 'Y-m-d H:i:s');
                    $newend = date_create($end);
                    $newend = date_format($newend, 'Y-m-d H:i:s');

                    $datenow = date('Y-m-d H:i:s');
                    if($datenow > $newstart && $datenow < $newend) {
                       "<span class='bluish_text'>Open</span>";
                    } else {
                       "<span class='bluish_text'>Closed</span>";
                    }
                  }
                ?>
                <br>
        <?php   }
            if($centerhours->sunstatus != '0' ) {
              if('Sun' === $daynow) { echo "<b>"; }
        ?>
              <span class="red_text inbl" style="width:30px;">Sun</span>
                <span class="dev_text margin_left_s">
                  <?php echo $centerhours->suntimestart." ".$centerhours->sunformatstart; ?> ~
                  <?php echo $centerhours->suntimeend." ".$centerhours->sunformatend; ?>
                </span>
                <?php
                  if('Sun' === $daynow) {
                    echo "</b>";
                    $start = date('Y-m-d'). " ".$centerhours->suntimestart.":00 ".$centerhours->sunformatstart;
                    $end = date('Y-m-d'). " ".$centerhours->suntimeend.":00 ".$centerhours->sunformatend;
                    $newstart = date_create($start);
                    $newstart = date_format($newstart, 'Y-m-d H:i:s');
                    $newend = date_create($end);
                    $newend = date_format($newend, 'Y-m-d H:i:s');

                    $datenow = date('Y-m-d H:i:s');
                    if($datenow > $newstart && $datenow < $newend) {
                       "<span class='bluish_text'>Open</span>";
                    } else {
                       "<span class='bluish_text'>Closed</span>";
                    }
                  }
                ?>
        <?php   } ?>
      	</div>
      </div>
    </div>
</div>

<!-- ========================= PRICING MODAL ================================= -->
<div id="modal-p" class="modal mh_5h">
    <div class="modal-content">
      <div class="row">
      	<div class="col s12">
      	<img href="#!" src="/img/frontend/popup_close.png" class="float_right modal-close waves-effect cursor_pointer">
      	<h5 class="main_title">Our Prices</h5>
        <table class="bordered">
          <thead>
            <tr class="dev_text border_bottom_s_gray">
              <th colspan="2" class="text_center border_bottom_s_gray">Membership</th>
              <th class="text_center border_bottom_s_gray">Price</th>
              <th class="text_center border_bottom_s_gray">Details</th>
            </tr>
          </thead>
          <tbody>
          <?php if($regfee != false) { ?>
            <tr>
              <td colspan="2" class="text_center font_90">Registration Fee</td>
              <td class="text_center font_90">$<?php echo $regfee->fee; ?></td>
              <td></td>
            </tr>
          <?php } ?>
          <?php if($regclass != false) { ?>
            <tr>
              <td colspan="2" class="text_center font_90">One Regular Class</td>
              <td class="text_center font_90">$<?php echo $regclass->fee; ?></td>
              <td></td>
            </tr>
          <?php } ?>
          <?php if($reg1on1intro != false) { ?>
            <tr><th colspan="4" class="bg_bluish">Intro</th></tr>
            <tr>
              <td colspan="2" class="text_center font_90">1-on-1 Introductory Session</td>
              <td class="text_center font_90">$<?php echo $reg1on1intro->fee; ?></td>
              <td></td>
            </tr>
          <?php } ?>
          <?php if($centermembership != false) { ?>
            <tr><th colspan="4" class="bg_bluish">Membership</th></tr>
            <?php foreach($centermembership as $membership) {
              switch($membership->period) {
                case 1: $period = "1 Month"; break;
                case 3: $period = "3 Months"; break;
                case 6: $period = "6 Months"; break;
                case 12: $period = "1 Year"; break;
                case 24: $period = "2 Years"; break;
                case 36: $period = "3 Years"; break;

                default: $period = " N/A ";
              }
              switch($membership->frequent) {
                case 7: $frequent = "Unlimited"; break;
                case 2: $frequent = "2 times a week"; break;
                case 3: $frequent = "3 times a week"; break;
                default: $frequent = " ";
              }
            ?>
            <tr>
              <td class="text_center teal_text font_90"><?php echo $period; ?></td>
              <td class="text_center font_90"><?php echo $frequent; ?></td>
              <td class="text_center bluish_text font_90">$<?php echo $membership->price; ?></td>
              <td class="mw_3h"><?php echo $membership->description; ?></td>
            </tr>
            <?php }
                }

               if($classpackages != false) { ?>
            <tr><th colspan="4" class="bg_bluish">Class Packages</th></tr>
            <?php foreach($classpackages as $membership) {
              switch($membership->period) {
                case 110: $period = "10 Classes"; break;
                case 120: $period = "20 Classes"; break;
                default: $period = " N/A ";
              }
              switch($membership->frequent) {
                case 7: $frequent = "Unlimited"; break;
                case 2: $frequent = "2 times a week"; break;
                case 3: $frequent = "3 times a week"; break;
                default: $frequent = " ";
              }
            ?>
            <tr>
              <td class="text_center teal_text font_90"><?php echo $period; ?></td>
              <td class="text_center font_90"><?php echo $frequent; ?></td>
              <td class="text_center bluish_text font_90">$<?php echo $membership->price; ?></td>
              <td class="mw_3h"><?php echo $membership->description; ?></td>
            </tr>
            <?php }
                }

                if($privatesession != false) { ?>
            <tr><th colspan="4" class="bg_bluish">Private Session</th></tr>
            <tr>
              <td colspan="4" class="font_90"><?php echo $privatesession->details; ?></td>
            </tr>
            <?php
                } ?>
          </tbody>
        </table>
      	</div>
      </div>
    </div>
</div>

<!-- ========================= CLASS SCHEDULE MODAL ================================= -->
<div id="modal-s" class="modal mh_5h">
    <div class="modal-content">
      <div class="row no-margin">
      	<div class="col s12  no-padding">
      	<img href="#!" src="/img/frontend/popup_close.png" class="float_right modal-close waves-effect cursor_pointer">
      	<span class="main_title no-margin">Our Class Schedule</span>
      	<table class="border_top_radius_m class_schedule">
          <thead>
            <tr class="no-margin bg_blue border_top_radius_m size_24 font_boldest">
                <th class="text_center padding_s_px class_th border_left_radius_s">Mon</th>
                <th class="text_center padding_s_px class_th">Tue</th>
                <th class="text_center padding_s_px class_th">Wed</th>
                <th class="text_center padding_s_px class_th">Thu</th>
                <th class="text_center padding_s_px class_th">Fri</th>
                <th class="text_center padding_s_px class_th">Sat</th>
                <th class="text_center padding_s_px class_th border_right_radius_s">Sun</th>
            </tr>
          </thead>
          <tbody class="border_light_gray">
            <tr>
              <td colspan="7" class="border:none;">
                <span class="teal_text font_90 font_bolder">MORNING SESSION:</span>
              </td>
            </tr>
            <tr> <!-- MORNING -->
                <td class="text_90"> <!-- monday -->
            <?php foreach($schedules as $class) {
                    if(substr($class->starttime,-2) == 'AM' && ($class->mo != null || $class->mo != '' ) ) {
                      echo "<b>".$class->mo . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime . "-" . $class->endtime . "<br></span>";
                    }
                  } ?>

                </td>
                <td> <!-- tuesday -->
            <?php foreach($schedules as $class) {
                    if(substr($class->starttime,-2) == 'AM' && ($class->tu != null || $class->tu != '' ) ) {
                      echo "<b>".$class->tu . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- wednesday -->
            <?php foreach($schedules as $class) {
                    if(substr($class->starttime,-2) == 'AM' && ($class->we != null || $class->we != '' ) ) {
                      echo "<b>".$class->we . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- thursday -->
            <?php foreach($schedules as $class) {
                    if(substr($class->starttime,-2) == 'AM' && ($class->th != null || $class->th != '' ) ) {
                      echo "<b>".$class->th . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- friday -->
            <?php foreach($schedules as $class) {
                    if(substr($class->starttime,-2) == 'AM' && ($class->fr != null || $class->fr != '' ) ) {
                      echo "<b>".$class->fr . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- saturday -->
            <?php foreach($schedules as $class) {
                    if(substr($class->starttime,-2) == 'AM' && ($class->sa != null || $class->sa != '' ) ) {
                      echo "<b>".$class->sa . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- sunday -->
            <?php foreach($schedules as $class) {
                    if(substr($class->starttime,-2) == 'AM' && ($class->su != null || $class->su != '' ) ) {
                      echo "<b>".$class->su . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
            </tr>
            <tr>
              <td colspan="7" class="border:none;">
                <span class="teal_text font_90 font_bolder">AFTERNOON SESSION:</span>
              </td>
            </tr>
            <tr> <!-- AFTERNOON -->
              <td> <!-- monday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time < 6 || $pm_time == 12) && ($class->mo != null || $class->mo != '' ) ) {
                      echo "<b>".$class->mo . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- tuesday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time < 6 || $pm_time == 12) && ($class->tu != null || $class->tu != '' ) ) {
                      echo "<b>".$class->tu . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- wednesday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time < 6 || $pm_time == 12) && ($class->we != null || $class->we != '' ) ) {
                      echo "<b>".$class->we."</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- thursday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time < 6 || $pm_time == 12) && ($class->th != null || $class->th != '' ) ) {
                      echo "<b>".$class->th . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- friday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time < 6 || $pm_time == 12) && ($class->fr != null || $class->fr != '' ) ) {
                      echo "<b>".$class->fr . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- saturday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time < 6 || $pm_time == 12) && ($class->sa != null || $class->sa != '' ) ) {
                      echo "<b>".$class->sa . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- sunday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time < 6 || $pm_time == 12) && ($class->su != null || $class->su != '' ) ) {
                      echo "<b>".$class->su . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
            </tr>
            <tr>
              <td colspan="7" class="border:none;">
                <span class="teal_text font_90 font_bolder">EVENING SESSION:</span>
              </td>
            </tr>
            <tr> <!-- EVENING -->
              <td> <!-- monday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time > 5 && $pm_time != 12) && ($class->mo != null || $class->mo != '' ) ) {
                      echo "<b>".$class->mo . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- tuesday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time > 5 && $pm_time != 12) && ($class->tu != null || $class->tu != '' ) ) {
                      echo "<b>".$class->tu . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- wednesday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time > 5 && $pm_time != 12) && ($class->we != null || $class->we != '' ) ) {
                      echo "<b>".$class->we . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- thursday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time > 5 && $pm_time != 12) && ($class->th != null || $class->th != '' ) ) {
                      echo "<b>".$class->th . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- friday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time > 5 && $pm_time != 12) && ($class->fr != null || $class->fr != '' ) ) {
                      echo "<b>".$class->fr . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- saturday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time > 5 && $pm_time != 12) && ($class->sa != null || $class->sa != '' ) ) {
                      echo "<b>".$class->sa . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- sunday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time > 5 && $pm_time != 12) && ($class->su != null || $class->su != '' ) ) {
                      echo "<b>".$class->su . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
            </tr>
          </tbody>
        </table>
      	</div>
      </div>
    </div>
</div>

<!-- ========================== HIDDEN MODAL ============================ -->
<!-- ========================= MAP ================================= -->
<div id="modal-m_h" class="modal overflow_hidden">
    <div class="modal-content no-margin">
      <div class="row no-margin">
        <div class="col s12 m8 no-padding">
          <div id="map_canvas">
            <ui-gmap-google-map center="map.center" zoom="map.zoom" draggable="true" options="options">
              <ui-gmap-marker coords="marker.coords" options="marker.options" events="marker.events" idkey="marker.id">
              </ui-gmap-marker>
            </ui-gmap-google-map>
          </div>
        </div>
        <div class="col s12 m4 padding_s">
        <img href="#!" src="/img/frontend/popup_close.png" class="float_right modal-close waves-effect cursor_pointer">
        <span class="font_24_px">CENTER </span> <span class="font_24_px font_boldest teal_text"><?php echo $center->centertitle;?></span>
        <br>
        <h5 class="size_18 font_bolder">ADDRESS</h5>
      <span class="dev_text">
        <?php echo $center->centeraddress; ?> <br>
        <?php echo $center->centercity . ", " . $center->centerstate . " $center->centerzip"; ?>
      </span>

    <h5 class="size_18 font_bolder">PHONE</h5>
      <span class="dev_text">
        <?php echo $centerphonenumber; ?> <br>
      </span>

              <span class="size_18 font_bolder">HOURS</span>
          <br>
          <label style="margin-left:70px"><b><u><?php echo date('D h:i A')."";?></u></b></label>
          <br>
          <?php
            $days = array("mon","tue","wed","thu","fri");
            $daynow = date('D');

            //LOOPING MONDAY TO FRIDAY
            foreach($days as $day) {
              $timestart = $day."timestart";
              $formatstart = $day."formatstart";
              $timeend = $day."timeend";
              $formatend = $day."formatend";

              if(ucwords($day) === $daynow) { echo "<b>"; }
          ?>
              <span class="red_text inbl" style="width:30px;"><?php echo ucwords($day); ?></span>
                <span class="dev_text margin_left_s">
                  <?php echo $centerhours->$timestart." ".$centerhours->$formatstart; ?>  ~
                  <?php echo $centerhours->$timeend." ".$centerhours->$formatend; ?>
                </span>
          <?php
                  if(ucwords($day) === $daynow) { echo "</b>";
                    $start = date('Y-m-d'). " ".$centerhours->$timestart.":00 ".$centerhours->$formatstart;
                    $end = date('Y-m-d'). " ".$centerhours->$timeend.":00 ".$centerhours->$formatend;
                    $newstart = date_create($start);
                    $newstart = date_format($newstart, 'Y-m-d H:i:s');
                    $newend = date_create($end);
                    $newend = date_format($newend, 'Y-m-d H:i:s');

                    $datenow = date('Y-m-d H:i:s');
                    if($datenow > $newstart && $datenow < $newend) {
                       "<span class='bluish_text'>Open</span>";
                    } else {
                       "<span class='bluish_text'>Closed</span>";
                    }
                  }
                ?>
                <br>
        <?php }
            if($centerhours->satstatus != '0' ) {
              if('Sat' === $daynow) { echo "<b>"; }
        ?>
              <span class="red_text inbl" style="width:30px;">Sat</span>
                <span class="dev_text margin_left_s">
                  <?php echo $centerhours->sattimestart." ".$centerhours->satformatstart; ?>  ~
                  <?php echo $centerhours->sattimeend." ".$centerhours->satformatend;; ?>
                </span>
                <?php
                  if('Sat' === $daynow) {
                    echo "</b>";
                    $start = date('Y-m-d'). " ".$centerhours->sattimestart.":00 ".$centerhours->satformatstart;
                    $end = date('Y-m-d'). " ".$centerhours->sattimeend.":00 ".$centerhours->satformatend;
                    $newstart = date_create($start);
                    $newstart = date_format($newstart, 'Y-m-d H:i:s');
                    $newend = date_create($end);
                    $newend = date_format($newend, 'Y-m-d H:i:s');

                    $datenow = date('Y-m-d H:i:s');
                    if($datenow > $newstart && $datenow < $newend) {
                       "<span class='bluish_text'>Open</span>";
                    } else {
                       "<span class='bluish_text'>Closed</span>";
                    }
                  }
                ?>
                <br>
        <?php   }
            if($centerhours->sunstatus != '0' ) {
              if('Sun' === $daynow) { echo "<b>"; }
        ?>
              <span class="red_text inbl" style="width:30px;">Sun</span>
                <span class="dev_text margin_left_s">
                  <?php echo $centerhours->suntimestart." ".$centerhours->sunformatstart; ?> ~
                  <?php echo $centerhours->suntimeend." ".$centerhours->sunformatend; ?>
                </span>
                <?php
                  if('Sun' === $daynow) {
                    echo "</b>";
                    $start = date('Y-m-d'). " ".$centerhours->suntimestart.":00 ".$centerhours->sunformatstart;
                    $end = date('Y-m-d'). " ".$centerhours->suntimeend.":00 ".$centerhours->sunformatend;
                    $newstart = date_create($start);
                    $newstart = date_format($newstart, 'Y-m-d H:i:s');
                    $newend = date_create($end);
                    $newend = date_format($newend, 'Y-m-d H:i:s');

                    $datenow = date('Y-m-d H:i:s');
                    if($datenow > $newstart && $datenow < $newend) {
                       "<span class='bluish_text'>Open</span>";
                    } else {
                       "<span class='bluish_text'>Closed</span>";
                    }
                  }
                ?>
        <?php   } ?>
        </div>
      </div>
    </div>
</div>

<!-- ========================= PRICING MODAL ================================= -->
<div id="modal-p_h" class="modal mh_5h">
    <div class="modal-content">
      <div class="row">
        <div class="col s12">
        <img href="#!" src="/img/frontend/popup_close.png" class="float_right modal-close waves-effect cursor_pointer">
        <h5 class="main_title">Our Prices</h5>
        <table class="bordered">
          <thead>
            <tr class="dev_text border_bottom_s_gray">
              <th colspan="2" class="text_center border_bottom_s_gray">Membership</th>
              <th class="text_center border_bottom_s_gray">Price</th>
              <th class="text_center border_bottom_s_gray">Details</th>
            </tr>
          </thead>
          <tbody>
          <?php if($regfee != false) { ?>
            <tr>
              <td colspan="2" class="text_center font_90">Registration Fee</td>
              <td class="text_center font_90">$<?php echo $regfee->fee; ?></td>
              <td></td>
            </tr>
          <?php } ?>
          <?php if($regclass != false) { ?>
            <tr>
              <td colspan="2" class="text_center font_90">One Regular Class</td>
              <td class="text_center font_90">$<?php echo $regclass->fee; ?></td>
              <td></td>
            </tr>
          <?php } ?>
          <?php if($reg1on1intro != false) { ?>
            <tr><th colspan="4" class="bg_bluish">Intro</th></tr>
            <tr>
              <td colspan="2" class="text_center font_90">1-on-1 Introductory Session</td>
              <td class="text_center font_90">$<?php echo $reg1on1intro->fee; ?></td>
              <td></td>
            </tr>
          <?php } ?>
          <?php if($centermembership != false) { ?>
            <tr><th colspan="4" class="bg_bluish">Membership</th></tr>
            <?php foreach($centermembership as $membership) {
              switch($membership->period) {
                case 1: $period = "1 Month"; break;
                case 3: $period = "3 Months"; break;
                case 6: $period = "6 Months"; break;
                case 12: $period = "1 Year"; break;
                case 24: $period = "2 Years"; break;
                case 36: $period = "3 Years"; break;
                case 110: $period = "10 Classes"; break;
                case 120: $period = "20 Classes"; break;
                default: $period = " N/A ";
              }
              switch($membership->frequent) {
                case 7: $frequent = "Unlimited"; break;
                case 2: $frequent = "2 times a week"; break;
                case 3: $frequent = "3 times a week"; break;
                default: $frequent = " ";
              }
            ?>
            <tr>
              <td class="text_center teal_text font_90"><?php echo $period; ?></td>
              <td class="text_center font_90"><?php echo $frequent; ?></td>
              <td class="text_center bluish_text font_90">$<?php echo $membership->price; ?></td>
              <td class="mw_3h"><?php echo $membership->description; ?></td>
            </tr>
            <?php }
                }

               if($classpackages != false) { ?>
            <tr><th colspan="4" class="bg_bluish">Class Packages</th></tr>
            <?php foreach($classpackages as $membership) {
              switch($membership->period) {
                case 110: $period = "10 Classes"; break;
                case 120: $period = "20 Classes"; break;
                default: $period = " N/A ";
              }
              switch($membership->frequent) {
                case 7: $frequent = "Unlimited"; break;
                case 2: $frequent = "2 times a week"; break;
                case 3: $frequent = "3 times a week"; break;
                default: $frequent = " ";
              }
            ?>
            <tr>
              <td class="text_center teal_text font_90"><?php echo $period; ?></td>
              <td class="text_center font_90"><?php echo $frequent; ?></td>
              <td class="text_center bluish_text font_90">$<?php echo $membership->price; ?></td>
              <td class="mw_3h"><?php echo $membership->description; ?></td>
            </tr>
            <?php }
                }

              if($privatesession != false) { ?>
            <tr><th colspan="4" class="bg_bluish">Private Session</th></tr>
            <tr>
              <td colspan="4" class="font_90"><?php echo $privatesession->details; ?></td>
            </tr>
            <?php
                } ?>
          </tbody>
        </table>
        </div>
      </div>
    </div>
</div>

<!-- ========================= CLASS SCHEDULE MODAL ================================= -->
<div id="modal-s_h" class="modal mh_5h">
    <div class="modal-content">
      <div class="row no-margin">
        <div class="col s12  no-padding">
        <img href="#!" src="/img/frontend/popup_close.png" class="float_right modal-close waves-effect cursor_pointer">
        <span class="main_title no-margin">Our Class Schedule</span>
        <table class="border_top_radius_m class_schedule">
          <thead>
            <tr>
              <td colspan="7" class="border:none;">
                <span class="teal_text font_90 font_bolder">MORNING SESSION:</span>
              </td>
            </tr>
            <tr> <!-- MORNING -->
                <td class="text_90"> <!-- monday -->
            <?php foreach($schedules as $class) {
                    if(substr($class->starttime,-2) == 'AM' && ($class->mo != null || $class->mo != '' ) ) {
                      echo "<b>".$class->mo . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime . "-" . $class->endtime . "<br></span>";
                    }
                  } ?>

                </td>
                <td> <!-- tuesday -->
            <?php foreach($schedules as $class) {
                    if(substr($class->starttime,-2) == 'AM' && ($class->tu != null || $class->tu != '' ) ) {
                      echo "<b>".$class->tu . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- wednesday -->
            <?php foreach($schedules as $class) {
                    if(substr($class->starttime,-2) == 'AM' && ($class->we != null || $class->we != '' ) ) {
                      echo "<b>".$class->we . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- thursday -->
            <?php foreach($schedules as $class) {
                    if(substr($class->starttime,-2) == 'AM' && ($class->th != null || $class->th != '' ) ) {
                      echo "<b>".$class->th . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- friday -->
            <?php foreach($schedules as $class) {
                    if(substr($class->starttime,-2) == 'AM' && ($class->fr != null || $class->fr != '' ) ) {
                      echo "<b>".$class->fr . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- saturday -->
            <?php foreach($schedules as $class) {
                    if(substr($class->starttime,-2) == 'AM' && ($class->sa != null || $class->sa != '' ) ) {
                      echo "<b>".$class->sa . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- sunday -->
            <?php foreach($schedules as $class) {
                    if(substr($class->starttime,-2) == 'AM' && ($class->su != null || $class->su != '' ) ) {
                      echo "<b>".$class->su . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
            </tr>
            <tr>
              <td colspan="7" class="border:none;">
                <span class="teal_text font_90 font_bolder">AFTERNOON SESSION:</span>
              </td>
            </tr>
            <tr> <!-- AFTERNOON -->
              <td> <!-- monday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time < 6 || $pm_time == 12) && ($class->mo != null || $class->mo != '' ) ) {
                      echo "<b>".$class->mo . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- tuesday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time < 6 || $pm_time == 12) && ($class->tu != null || $class->tu != '' ) ) {
                      echo "<b>".$class->tu . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- wednesday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time < 6 || $pm_time == 12) && ($class->we != null || $class->we != '' ) ) {
                      echo "<b>".$class->we."</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- thursday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time < 6 || $pm_time == 12) && ($class->th != null || $class->th != '' ) ) {
                      echo "<b>".$class->th . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- friday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time < 6 || $pm_time == 12) && ($class->fr != null || $class->fr != '' ) ) {
                      echo "<b>".$class->fr . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- saturday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time < 6 || $pm_time == 12) && ($class->sa != null || $class->sa != '' ) ) {
                      echo "<b>".$class->sa . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- sunday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time < 6 || $pm_time == 12) && ($class->su != null || $class->su != '' ) ) {
                      echo "<b>".$class->su . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
            </tr>
            <tr>
              <td colspan="7" class="border:none;">
                <span class="teal_text font_90 font_bolder">EVENING SESSION:</span>
              </td>
            </tr>
            <tr> <!-- EVENING -->
              <td> <!-- monday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time > 5 && $pm_time != 12) && ($class->mo != null || $class->mo != '' ) ) {
                      echo "<b>".$class->mo . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- tuesday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time > 5 && $pm_time != 12) && ($class->tu != null || $class->tu != '' ) ) {
                      echo "<b>".$class->tu . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- wednesday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time > 5 && $pm_time != 12) && ($class->we != null || $class->we != '' ) ) {
                      echo "<b>".$class->we . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- thursday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time > 5 && $pm_time != 12) && ($class->th != null || $class->th != '' ) ) {
                      echo "<b>".$class->th . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- friday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time > 5 && $pm_time != 12) && ($class->fr != null || $class->fr != '' ) ) {
                      echo "<b>".$class->fr . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- saturday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time > 5 && $pm_time != 12) && ($class->sa != null || $class->sa != '' ) ) {
                      echo "<b>".$class->sa . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
                <td> <!-- sunday -->
            <?php foreach($schedules as $class) {
                    $pm_time = substr($class->starttime, 0, 2);
                    if(substr($class->starttime,-2) == 'PM' && ($pm_time > 5 && $pm_time != 12) && ($class->su != null || $class->su != '' ) ) {
                      echo "<b>".$class->su . "</b><br><span class='dev_text font_85 inbl margin_bottom_s'>" . $class->starttime .  "-" . $class->endtime . "<br></span>";
                    }
                  } ?>
                </td>
            </tr>
          </tbody>
        </table>
        </div>
      </div>
    </div>
</div>

<?php
} else {
  echo "<br><br><center><span class='warning_message'>Error 404: Page not found!</span></center><br><br>";
} ?>

<div class="chrono hidden modalSlider">
  <center>
    <div class="modalImg-div">
      <img href="#!" src="/img/frontend/popup_close.png" class="float_right chrono-close cursor_pointer" style="position:absolute;clear:both">
      <img src="" id="modalImg" class="img_inherit">
    </div>
  </center>
</div>
