<div ng-controller="prevnewsCtrl">
<?php 

echo $this->getContent();

if($logoimage->value1 == 1){
  header('Location: /../../maintenance/');
}


?>
<div id="crumbs_cont">
	<ul id="crumbs">
		<li class="crumbsli"><a class="crumbs red_text" href="/">Home</a></li>
		<li class="crumbsli"><a class="crumbs red_text" href="">B&B Buzz</a></li>
		<li>
		<a class="crumbscategory" href="">{[{pcategoryname}]}</a>
		</li>
	</ul>
</div>

<div class="row">
    <div class="col s12">
      	<div class="newsindexcontent" >

	        <div class="newsindexnewsdata">

	        	<h2 class="view_title">{[{ptitle}]}</h2>
	        	<span class="view_date">Released on {[{pdate}]}</span>
	        	<div class="sns_wrap-view">
	        		<ul class="ul-social-view">
	        			<li>
	        				<div class="fb-like fb-view" data-href="<?php echo  $this->config->application->BaseURL;?><?php echo $articles; ?>/view/<?php echo $newsslugs;  ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
	        			</li>
	        			<li>
	        				<div class="fb-share-button fb-view" data-href="<?php echo  $this->config->application->BaseURL;?><?php echo $articles; ?>/view/<?php echo $newsslugs;  ?>" data-layout="button_count"></div>
	        			</li>
	        			<li>
	        				<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
	        			</li>
	        			<li>
	        				<!-- Place this tag where you want the +1 button to render. -->
	        				<div class="Gplus"><div class="g-plusone"></div></div>
	        			</li>
	        			<li>
	        				<a href="//www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark"  data-pin-color="white"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_white_20.png" /></a>
	        			</li>
					</ul>
	        	</div>

	            <div class="view_content">
	            	
	              <!-- <img src="<?php  $this->config->application->amazonlink .'/uploads/newsimage/'; ?>{[{pbanner}]}" class="full_width"> -->
	              <div ng-bind-html="pbody"></div>
	              <span class="float_right">&mdash; {[{pauthor}]}</span>
	              <br>
	              <hr class="hr-custom">
	              <h6 class="h6-tiny">Category:  
				    <a href="" class="a-custom-default">
			          {[{pcategoryname}]}
					</a>
				  </h6>

				

	            </div> <!-- End of View content DIV-->
	            <!-- DISCUSSION PLUGIN -->
	            <div class="view_discussion">
	            	<div id="disqus_thread"></div>
					<script type="text/javascript">
					    /* * * CONFIGURATION VARIABLES * * */
					    var disqus_shortname = 'bodynbrain';
					    
					    /* * * DON'T EDIT BELOW THIS LINE * * */
					    (function() {
					        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
					        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
					        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
					    })();
					</script>
					<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
	            </div> <!-- END OF DISCUSSION PLUGIN -->
	        </div>

	        <div class="newsindexsidebar">
	          <a href="/getstarted/starterspackage"><img src="/img/frontend/banner_getstarted_small.jpg" style="width:100%; height:auto"></a>

			   	<div class="row" ng-controller="yogalifeviewCtrl">

			   	<div class="col s6 mostpop bg_whitish padding_s" style="font-size:120%; text-align:center">
			   		<b class=" cursor_pointer" ng-click="tab_mostpop()">MOST POPULAR</b>
			   	</div>
			   	<div class="col s6 latest dev_text padding_s" style="font-size:120%; text-align:center">
			   		<b class="cursor_pointer " ng-click="tab_latest()">LATEST</b>
			   	</div>
			
			   	<div class="col s12 mostpopular bg_whitish" ng-show="mostpopular">
			   		<ul class="sidebar_list" style="padding:0">
			    		<li><br></li>
			    		<!-- LOOPING MOST VIEWED NEWS -->
			    		<?php 
			    			foreach($sidebarmostviewednews as $news) {
			    			?>
			    				<li>
			    					<a href="<?php echo $articles; ?>/view/<?php echo $news->newsslugs;?>" class="a_list">
						    		<div class="li_list">
						    			<div class="thumbnail">
											<img src="<?php echo  $this->config->application->amazonlink.'/uploads/newsimage/'.$news->banner;?>" class="img_thumbnail">
						    			</div>
						    			<div class="span">
						    				<?php echo substr($news->description, 0, 50) . "..";?>
						    			</div>
						    		</div>
					    			</a>
					    		</li>
			    			<?php
			    			}
			    		?>
			    	</ul>
			   	</div>
			   	<br>
			   	<div class="col s12 latest bg_whitish" ng-show="latest">
			   		<ul class="sidebar_list">
			    		<li><br></li>
			    		<!-- LOOPING LATEST NEWS -->
			    		<?php 
			    			foreach($sidebarlatestnews as $news) {
			    			?>
			    				<li><a href="<?php echo $articles; ?>/view/<?php echo $news->newsslugs;?>" class="a_list">
						    		<div class="li_list">
						    			<div class="thumbnail">
										  <img src="<?php echo  $this->config->application->amazonlink.'/uploads/newsimage/'.$news->banner;?>" class="img_thumbnail">
						    			</div>
						    			<div class="span">
						    				<?php echo substr($news->description, 0, 50) . ".."; ?>
						    			</div>
						    		</div>
					    			</a>
					    		</li>
			    			<?php
			    			}
			    		?>
			    	</ul>
			   	</div>
			    </div>
			    <br>
			   <div class="row sidebar_newsletter padding_m">
			      <div class="row">
			      	<div class="col s10">
			      		<h5>e-Newsletter</h5>
			      	</div>
			      	<form 	action="//bodynbrain.us3.list-manage.com/subscribe/post?u=cfc22757143336e0cfcf90eef&amp;id=eef0458b00" 
			      			method="post" 
			      			id="mc-embedded-subscribe-form" 
			      			name="mc-embedded-subscribe-form" 
			      			class="validate" 
			      			target="_blank" 
			      			novalidate >
				        <div class="input-field col s12">
				          <input 	type="email" 
				          			value="" 
				          			name="EMAIL" 
				          			class="required email validate custom-txtbox" 
				          			id="mce-EMAIL" 
				          			placeholder="E-mail Address">
				        </div>
				        <div class="col s6">
				        	<input 	type="submit" 
				        			value="Send" 
				        			name="subscribe" 
				        			id="mc-embedded-subscribe" 
				        			class="btn-custom btn-block">
				    	</div>
			    	</form>      
          
			      </div>                            
			  </div>
			  <br>
			  <div class="row success_stories padding_m">
			  	<div class="col s12">
			  		<h4 class="success_stories_title">Success Stories</h4>
			  		<ul class="sidebar_list">
			    		<li><br></li>
			    <?php 	foreach($view->stories as $story)  { ?>
			    		<li><a href="/successstories/view/<?php echo $story->id; ?>" class="a_list">
				    			<div class="li_list">
				    				<div class="thumbnail">
				    					<img src="<?php echo  $this->config->application->amazonlink.'/uploads/testimonialimages/'.$story->photo;?>" class="img_thumbnail">
				    				</div>
				    				<div class="span">
				    				  <?php echo $story->subject; ?>
				    				  <br>
				    				  <label><?php echo $story->author; ?></label>
				    				</div>
				    			</div>
			    			</a>
			    		</li>
			    <?php 	} ?>
			    	</ul>
			  	</div>
			  </div>

	        </div> <!-- END of newsindexsidebar -->

      	</div>
    </div>
</div>

</div>