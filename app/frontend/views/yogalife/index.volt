<?php echo $this->getContent()?>
<?php
if($logoimage->value1 == 1){
  header('Location: /../../maintenance/');
}
?>
<div id="crumbs_cont">
	<ul id="crumbs">
		<li class="crumbsli"><a class="crumbs red_text" href="/">Home</a></li>
		<li class="crumbsli"><a class="crumbs red_text" href="<?php echo $articles; ?>">B&B Buzz</a></li>
		<?php 
			$getginfo = $newscategory;
			foreach ($getginfo as $key => $value) {
		?>
		<li>
		<a class="crumbscategory" href="<?php echo $articles; ?>/category/<?php echo $getginfo[$key]->categoryslugs;  ?>/1"><?php echo $getginfo[$key]->categoryname . " &nbsp; &#8226;";  ?></a>
		</li>
		<?php } ?>  
	</ul>
</div>

<div class="divseparetor"></div>

<div class="row">
    <div class="col s12">
      	<div class="newsindexcontent">
	        <div class="newsindexnewsdata">
	        	<div class="featurednewscontainer">
	        		<div class="featurednews">
	        			<?php 
	        			$getginfo = $featurednews;
	        			foreach ($getginfo as $key => $value) {
	        				$date = date_create($getginfo[$key]->date);
	           				$date = date_format($date, "F jS Y");
	        			?>
	        			<h5>Featured</h5>
	        			<a href="<?php echo  $this->config->application->BaseURL.$articles."/view/".$getginfo[$key]->newsslugs; ?>">
		        			<div class="thumbnailimg" style="background-image: url('<?php echo  $this->config->application->amazonlink."/uploads/newsimage/".$getginfo[$key]->banner; ?>');">
	                      	</div>
                      	</a>

	        			<div class="thumbnailtitlecontainier">
	        				<div class="thumbseparator"></div>
		        			<a href="<?php echo  $this->config->application->BaseURL.$articles."/view/".$getginfo[$key]->newsslugs; ?>"><span class="thumbnailtitle"><?php echo $getginfo[$key]->title; ?></span></a>
		        			<div class="cmt">

		        				<a href="<?php echo  $this->config->application->BaseURL.$articles."/view/".$getginfo[$key]->newsslugs; ?>#disqus_thread" class="comment">Comment (0)</a><span> | </span><?php echo $date ?>
		        				

		        			</div>
	        			</div>
	        			<?php } ?>
	        		</div>

	        		<div class="founderswisdom">
	        			
	        			<h5><a href="<?php echo $articles; ?>/category/<?php echo $randomnewscategory[0]->categoryslugs; ?>/1"><?php echo $randomnewscategory[0]->categoryname; ?></a></h5>
	        			<a href="<?php echo  $this->config->application->BaseURL.$articles."/view/".$randomnewscategory[0]->newsslugs; ?>">
		        			<div class="thumbnailimg" style="background-image: url('<?php echo  $this->config->application->amazonlink."/uploads/newsimage/".$randomnewscategory[0]->banner; ?>');">
	                      	</div>
                      	</a>

	        			<div class="thumbnailtitlecontainier">
	        				<div class="thumbseparator"></div>
		        			<a href="<?php echo  $this->config->application->BaseURL.$articles."/view/".$randomnewscategory[0]->newsslugs; ?>"><span class="thumbnailtitle"><?php echo $randomnewscategory[0]->title; ?></span></a>
		        			<div class="cmt">

		        				<a href="<?php echo  $this->config->application->BaseURL.$articles."/view/".$randomnewscategory[0]->newsslugs; ?>#disqus_thread" class="comment">Comment (0)</a><span> | </span><?php $date = date_create($randomnewscategory[0]->date);$date = date_format($date, "F jS Y"); echo $date; ?>

		        			</div>
	        			</div>
	        		</div>
	           </div>

	           <div class="latestnewscontainer">
	           		<h5>Latest</h5>
	           		<?php 
	           			$getginfo = $latestnews;
	           			$viewcount = 0;
	           			foreach ($getginfo as $key => $value) {
	           				$date = date_create($getginfo[$key]->date);
	           				$date = date_format($date, "F jS Y");
	           				if($viewcount == 0)
	           				{

	           		?>
	           		<div class="latestnewsleft">
	           		  <a href="<?php echo  $this->config->application->BaseURL.$articles."/view/".$getginfo[$key]->newsslugs; ?>">
	        			<div class="thumbnailimg" style="background-image: url('<?php echo  $this->config->application->amazonlink."/uploads/newsimage/".$getginfo[$key]->banner; ?>');">
                      	</div>
                      </a>
		        	  <div class="thumbnailtitlecontainier">
		        				<div class="thumbseparator"></div>
			        			<a href="<?php echo  $this->config->application->BaseURL.$articles."/view/".$getginfo[$key]->newsslugs; ?>"><span class="thumbnailtitle"><?php echo $getginfo[$key]->title; ?></span></a>
			        			<div class="cmt">

			        				<a href="<?php echo  $this->config->application->BaseURL.$articles."/view/".$getginfo[$key]->newsslugs; ?>#disqus_thread" class="comment">Comment (0)</a><span> | </span><?php echo $date; ?>

			        			</div>
		        	  </div>
	        		</div>
	        		<?php
		        			$viewcount = 1;
		        			}
		           			else
		           			{
	           		?>
	           			<div class="latestnewsright">
	           			  <a href="<?php echo  $this->config->application->BaseURL.$articles."/view/".$getginfo[$key]->newsslugs; ?>">
		        			<div class="thumbnailimg" style="background-image: url('<?php echo  $this->config->application->amazonlink."/uploads/newsimage/".$getginfo[$key]->banner; ?>');">
	                      	</div>
	                      </a>

	        			<div class="thumbnailtitlecontainier">
	        				<div class="thumbseparator"></div>
		        			<a href="<?php echo  $this->config->application->BaseURL.$articles."/view/".$getginfo[$key]->newsslugs; ?>"><span class="thumbnailtitle"><?php echo $getginfo[$key]->title; ?></span></a>
		        			<div class="cmt">

		        				<a href="<?php echo  $this->config->application->BaseURL.$articles."/view/".$getginfo[$key]->newsslugs; ?>#disqus_thread" class="comment">Comment (0)</a><span> | </span><?php echo  $date; ?>

		        			</div>
	        			</div>
	        		</div>


	           		<?php
	           				$viewcount = 0;
	           				}
	           			} 
	           		?>
	        		

	           </div>

	        </div>

	        <div class="newsindexsidebar">
	          <a href="/getstarted/starterspackage"><img src="/img/frontend/banner_getstarted.jpg" style="width:100%;height:auto;"></a>
	          <div class="twitter_wrap">
	          	<h4 class="title">Community Conversations</h4>
	          	<p class="sub_title">Here you can post and read <br>messages on topics that interest, <br>intrigue, and relate to you.<br><a href="/conversations"><img src="/img/frontend/banner_btn.gif"></a></p>
	          	<div class="twitter_list" data-twttr-id="twttr-sandbox-0">
	          		<a class="twitter-timeline" href="https://twitter.com/bodynbrainyoga" data-widget-id="284465771520860161">Tweets by @bodynbrainyoga</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

	          	</div>
	          </div>
	        </div>

      	</div>
    </div>
</div>

